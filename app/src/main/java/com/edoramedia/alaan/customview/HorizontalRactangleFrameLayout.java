package com.edoramedia.alaan.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * The Custom ractangle view .
 */
public class HorizontalRactangleFrameLayout extends FrameLayout {
    public HorizontalRactangleFrameLayout(Context context) {
        super(context);
    }

    public HorizontalRactangleFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HorizontalRactangleFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //noinspection SuspiciousNameCombination
        super.onMeasure(heightMeasureSpec + 256, heightMeasureSpec);
    }
}
