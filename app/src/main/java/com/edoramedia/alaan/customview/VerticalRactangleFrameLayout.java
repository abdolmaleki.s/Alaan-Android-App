package com.edoramedia.alaan.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class VerticalRactangleFrameLayout extends FrameLayout {
    public VerticalRactangleFrameLayout(Context context) {
        super(context);
    }

    public VerticalRactangleFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VerticalRactangleFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //noinspection SuspiciousNameCombination
        super.onMeasure(widthMeasureSpec, widthMeasureSpec+128);
    }
}
