package com.edoramedia.alaan.customview;

import android.content.Context;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.infrastructure.DontObfuscate;
import com.github.ybq.android.spinkit.style.MultiplePulse;

/**
 * Custom Progress Dialog
 */
@DontObfuscate
public class MyProgressDialog extends android.app.ProgressDialog {

    private ProgressBar progressBar;

    /**
     * Instantiates a new My progress dialog.
     *
     * @param context the context
     */
    public MyProgressDialog(final Context context) {
        super(context, R.style.D1NoTitleDim);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress);
        progressBar = findViewById(R.id.dialog_progress_loading_progress);
        MultiplePulse multiplePulse = new MultiplePulse();
        progressBar.setIndeterminateDrawable(multiplePulse);
        setCancelable(false);

    }


}
