package com.edoramedia.alaan.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

import com.edoramedia.alaan.infrastructure.IScrollListener;

/**
 * The type Custom horizontal scroll view.
 */
public class CustomHorizontalScrollView extends HorizontalScrollView {
    private IScrollListener listener = null;


    public CustomHorizontalScrollView(Context context) {
        super(context);
    }

    public CustomHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setScrollViewListener(IScrollListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        if (listener != null) {
            listener.onScrollChanged(this, x, y, oldx, oldy);
        }
    }

}
