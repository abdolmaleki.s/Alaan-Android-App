package com.edoramedia.alaan.viewmodel;

/**
 * model for show's episods
 */
public class ShowEpisodeModel {
    public String id;
    public String title;
    public int number;
    public String imageUrl;

}
