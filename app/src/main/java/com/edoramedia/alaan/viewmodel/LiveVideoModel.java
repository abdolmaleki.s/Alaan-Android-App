package com.edoramedia.alaan.viewmodel;

/**
 * model for aish lives
 */
public class LiveVideoModel {
    public String id;
    public String imageUrl;
    public String videoUrl;
    public String description;
    public boolean isDescriptionVisible;
}
