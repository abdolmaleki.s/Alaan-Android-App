package com.edoramedia.alaan.viewmodel;

/**
 * model for schedule
 */
public class ScheduleProgramModel {
    public String id;
    public String title;
    public String startDate;
    public String endDate;
    public String imageUrl;
    public boolean isPlayingNow;
}
