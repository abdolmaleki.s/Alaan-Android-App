package com.edoramedia.alaan.viewmodel;

/**
 * model for articles category
 */
public class ArticleCategoryModel {

    public int id;
    public String title;
}
