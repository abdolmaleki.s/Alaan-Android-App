package com.edoramedia.alaan.viewmodel;

import java.util.List;

/**
 * model for articles
 */
public class ArticleModel {

    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_IMAGE_CUSTOM = 2;
    public static final int TYPE_GALLERY = 3;
    public static final int TYPE_VIDEO = 4;
    public static final int TYPE_highlight = 5;

    public static final int TYPE_AISH_IMAGE = 6;
    public static final int TYPE_AISH_IMAGE_CUSTOM = 7;
    public static final int TYPE_AISH_GALLERY = 8;
    public static final int TYPE_AISH_VIDEO = 9;
    public static final int TYPE_AISH_highlight = 10;

    public String id;
    public int type;
    public String title;
    public String backgroundImageUrl;
    public String publishDate;
    public String category;
    public List<String> galleryImageUrls;
    public List<HighlightArticleModel> highlightArticleModels;
}
