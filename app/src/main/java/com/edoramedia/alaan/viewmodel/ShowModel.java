package com.edoramedia.alaan.viewmodel;

/**
 * model for shows
 */
public class ShowModel {
    public String id;
    public String title;
    public String imageUrl;
    public String logoUrl;
    public String time;
    public String weekDays;
}
