package com.edoramedia.alaan.viewmodel;

/**
 * model for featured articles
 */

public class HighlightArticleModel {
    public String title;
    public String id;

    public HighlightArticleModel(String title, String id) {
        this.title = title;
        this.id = id;
    }
}
