package com.edoramedia.alaan.viewmodel;

import java.util.ArrayList;
import java.util.List;

/**
 * Model for Slide menu.
 */
public class SlideMenuModel {

    public String icon;
    public String title;
    public List<String> subMenus;

    public SlideMenuModel(String icon, String title, List<String> subMenus) {
        this.icon = icon;
        this.title = title;
        if (subMenus == null) {
            this.subMenus = new ArrayList<>();
        } else {
            this.subMenus = subMenus;
        }
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets icon.
     *
     * @return the icon
     */
    public String getIcon() {
        return icon;
    }
}
