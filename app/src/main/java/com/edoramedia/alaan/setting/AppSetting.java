package com.edoramedia.alaan.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.viewmodel.ArticleCategoryModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * caching app setting
 */
public class AppSetting {

    private static SharedPreferences prefs;
    private static AppSetting session;

    /**
     * Config cache.
     *
     * @param cntx the cntx
     */
    public static void configAppSetting(Context cntx) {
        if (prefs == null) {
            prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static AppSetting getInstance() {
        if (session == null) {
            session = new AppSetting();
        }
        return session;
    }

    /**
     * save lunch status
     *
     * @param isFirstLunch the is first lunch
     */
    public void setIsFirstLunch(boolean isFirstLunch) {
        prefs.edit().putBoolean(Constant.Setting.IS_FIRST_LUNCH, isFirstLunch).apply();
    }

    /**
     * Sets section (site).
     *
     * @param appSection the app section
     */
    public void setAppSection(int appSection) {
        prefs.edit().putInt(Constant.KEY_APP_SECTION, appSection).apply();
    }

    /**
     * Is first lunch boolean.
     *
     * @return the boolean
     */
    public boolean isFirstLunch() {
        boolean isFirstLunch = prefs.getBoolean(Constant.Setting.IS_FIRST_LUNCH, true);
        return isFirstLunch;
    }

    /**
     * save authentication status
     *
     * @param isAuthenticated the is authenticated
     */
    public void setAuthenticated(boolean isAuthenticated) {
        prefs.edit().putBoolean(Constant.Setting.IS_AUTHENTICATED, isAuthenticated).apply();
    }


    /**
     * save article categorize
     *
     * @param categoryModels the category models
     */
    public void setArticleCategoryList(List<ArticleCategoryModel> categoryModels) {
        Gson gson = new Gson();
        String jsonList = gson.toJson(categoryModels);
        prefs.edit().putString(Constant.Setting.CATEGORY_LIST, jsonList).apply();
    }

    /**
     * Gets article categories.
     *
     * @return the article categories
     */
    public List<ArticleCategoryModel> getArticleCategories() {
        Gson gson = new Gson();
        String articleCategories = prefs.getString(Constant.Setting.CATEGORY_LIST, null);
        Type type = new TypeToken<List<ArticleCategoryModel>>() {
        }.getType();
        List<ArticleCategoryModel> categoryModels = gson.fromJson(articleCategories, type);
        return categoryModels;
    }


    /**
     * get authentication status
     *
     * @return the boolean
     */
    public boolean isAuthenticated() {
        boolean isAuthenticated = prefs.getBoolean(Constant.Setting.IS_AUTHENTICATED, false);
        return isAuthenticated;
    }

    /**
     * Gets app section.
     *
     * @return the app section
     */
    public int getAppSection() {
        int appSection = prefs.getInt(Constant.KEY_APP_SECTION, Constant.Section.TV);
        return appSection;
    }

    /**
     * Clear cache.
     */
    public void clear() {
        prefs.edit().clear();
        prefs.edit().apply();
    }

}
