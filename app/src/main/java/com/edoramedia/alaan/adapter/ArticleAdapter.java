package com.edoramedia.alaan.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.Interface.IPageController;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.fragment.ArticleFragment;
import com.edoramedia.alaan.fragment.SpecificArticleFragment;
import com.edoramedia.alaan.infrastructure.Convertor;
import com.edoramedia.alaan.setting.AppSetting;
import com.edoramedia.alaan.viewmodel.ArticleModel;
import com.edoramedia.alaan.viewmodel.HighlightArticleModel;

import java.util.List;
import java.util.Objects;

import static com.edoramedia.alaan.infrastructure.Convertor.fromHtml;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyViewHolder> {

    public static final int STYLE_DASHBOARD = 1;
    public static final int STYLE_ARTICLE = 2;

    private List<ArticleModel> data;
    public Context context;
    private RecyclerViewClickListener itemListener;
    private int mSection;
    private int mStyle;

    public ArticleAdapter(List<ArticleModel> data, int style, Context ctx, RecyclerViewClickListener itemListener) {
        this.data = data;
        this.context = ctx;
        this.itemListener = itemListener;
        this.mSection = AppSetting.getInstance().getAppSection();
        this.mStyle = style;
    }

    @NonNull
    @Override
    public ArticleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(viewType, parent, false);

        MyViewHolder holder = null;
        switch (viewType) {
            case R.layout.item_list_article_image:
                holder = new ImageViewHolder(view);
                break;

            case R.layout.item_list_article_video:
                holder = new VideoViewHolder(view);
                break;

            case R.layout.item_list_article_custom_image:
                holder = new CustomImageViewHolder(view);
                break;

            case R.layout.item_list_article_gallery:
                holder = new GalleryViewHolder(view);
                break;


            case R.layout.item_list_article_highlight:
                holder = new HighlightViewHolder(view);
                break;

            case R.layout.item_list_article_aish_image:
                holder = new ImageViewHolder(view);
                break;

            case R.layout.item_list_article_aish_video:
                holder = new VideoViewHolder(view);
                break;

            case R.layout.item_list_article_aish_custom_image:
                holder = new CustomImageViewHolder(view);
                break;

            case R.layout.item_list_article_aish_highlight:
                holder = new HighlightViewHolder(view);
                break;

            case R.layout.item_list_article_aish_gallery:
                holder = new GalleryViewHolder(view);
                break;

            default:
                holder = new ImageViewHolder(view);

        }
        return holder;
    }

    @Override
    public int getItemViewType(int position) {

        switch (data.get(position).type) {
            case ArticleModel.TYPE_IMAGE:
                return R.layout.item_list_article_image;
            case ArticleModel.TYPE_VIDEO:
                return R.layout.item_list_article_video;
            case ArticleModel.TYPE_IMAGE_CUSTOM:
                return R.layout.item_list_article_custom_image;
            case ArticleModel.TYPE_GALLERY:
                return R.layout.item_list_article_gallery;
            case ArticleModel.TYPE_highlight:
                return R.layout.item_list_article_highlight;
            case ArticleModel.TYPE_AISH_IMAGE:
                return R.layout.item_list_article_aish_image;
            case ArticleModel.TYPE_AISH_VIDEO:
                return R.layout.item_list_article_aish_video;
            case ArticleModel.TYPE_AISH_IMAGE_CUSTOM:
                return R.layout.item_list_article_aish_custom_image;
            case ArticleModel.TYPE_AISH_GALLERY:
                return R.layout.item_list_article_aish_gallery;
            case ArticleModel.TYPE_AISH_highlight:
                return R.layout.item_list_article_aish_highlight;

            default:
                return R.layout.item_list_article_image;
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ArticleModel currentData = data.get(holder.getAdapterPosition());
        holder.bind(currentData);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public abstract class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public MyViewHolder(View itemView) {
            super(itemView);
        }

        abstract void bind(ArticleModel item);

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(data.get(getLayoutPosition()));
        }
    }

    public class ImageViewHolder extends MyViewHolder implements View.OnClickListener {

        private TextView date;
        private TextView title;
        private TextView category;
        private ImageView backgroundImage;
        private CardView containerPanel;
        private View bottomLine;
        private View bottomInnerLine;

        public ImageViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.item_list_article_image_tv_date);
            title = itemView.findViewById(R.id.item_list_article_image_tv_title);
            category = itemView.findViewById(R.id.item_list_article_image_tv_category);
            backgroundImage = itemView.findViewById(R.id.item_list_article_image_img_background);
            if (mSection == Constant.Section.AISH) {
                containerPanel = itemView.findViewById(R.id.item_list_article_aish_panel_cardview);
                bottomLine = itemView.findViewById(R.id.item_list_article_aish_bottom_line);
                bottomInnerLine = itemView.findViewById(R.id.item_list_article_aish_bottom_inner_line);
            }
            itemView.setOnClickListener(this);
        }

        @Override
        void bind(ArticleModel item) {
            date.setText(item.publishDate);
            title.setText(item.title);
            category.setText(item.category);
            Glide.with(context).load(item.backgroundImageUrl).apply(new RequestOptions()).into(backgroundImage);

            switch (mSection) {
                case Constant.Section.TV:
                    break;

                case Constant.Section.NEWS:
                    category.setTextColor(ContextCompat.getColor(context, R.color.alaan_akhbar_primary));
                    break;

                case Constant.Section.FM:
                    break;

                case Constant.Section.AISH:
                    if (mStyle == ArticleAdapter.STYLE_DASHBOARD) {
                        bottomLine.setVisibility(View.GONE);
                        bottomInnerLine.setVisibility(View.VISIBLE);
                        containerPanel.setRadius(Convertor.convertDpToPixel(8, context));
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(containerMargin, containerMargin, containerMargin, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    } else {
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(0, containerMargin, 0, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    }
                    break;
            }

            category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((IPageController) context).loadFragment(ArticleFragment.newInstance(0));
                }
            });

        }
    }

    public class VideoViewHolder extends MyViewHolder implements View.OnClickListener {

        private TextView date;
        private TextView title;
        private TextView category;
        private ImageView backgroundImage;
        private CardView containerPanel;
        private View bottomLine;
        private View bottomInnerLine;


        public VideoViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.item_list_article_video_tv_date);
            title = itemView.findViewById(R.id.item_list_article_video_tv_title);
            category = itemView.findViewById(R.id.item_list_article_video_tv_category);
            backgroundImage = itemView.findViewById(R.id.item_list_article_video_img_background);
            if (mSection == Constant.Section.AISH) {
                containerPanel = itemView.findViewById(R.id.item_list_article_aish_panel_cardview);
                bottomLine = itemView.findViewById(R.id.item_list_article_aish_bottom_line);
                bottomInnerLine = itemView.findViewById(R.id.item_list_article_aish_bottom_inner_line);

            }
            itemView.setOnClickListener(this);
        }

        @Override
        void bind(ArticleModel item) {
            date.setText(item.publishDate);
            title.setText(item.title);
            category.setText(item.category);
            Glide.with(context).load(item.backgroundImageUrl).apply(new RequestOptions()).into(backgroundImage);

            switch (mSection) {
                case Constant.Section.TV:
                    break;

                case Constant.Section.NEWS:
                    category.setTextColor(ContextCompat.getColor(context, R.color.alaan_akhbar_primary));
                    break;

                case Constant.Section.FM:
                    break;

                case Constant.Section.AISH:

                    if (mStyle == ArticleAdapter.STYLE_DASHBOARD) {
                        bottomLine.setVisibility(View.GONE);
                        bottomInnerLine.setVisibility(View.VISIBLE);
                        containerPanel.setRadius(Convertor.convertDpToPixel(8, context));
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(containerMargin, containerMargin, containerMargin, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    } else {
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(0, containerMargin, 0, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    }
                    break;
            }

            category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((IPageController) context).loadFragment(ArticleFragment.newInstance(0));
                }
            });

        }
    }

    public class CustomImageViewHolder extends MyViewHolder implements View.OnClickListener {

        private TextView date;
        private TextView title;
        private TextView category;
        private ImageView backgroundImage;
        private LinearLayout panelTitle;
        private CardView containerPanel;
        private View bottomLine;
        private View bottomInnerLine;

        public CustomImageViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.item_list_article_custom_image_tv_date);
            title = itemView.findViewById(R.id.item_list_article_custom_image_tv_title);
            category = itemView.findViewById(R.id.item_list_article_custom_image_tv_category);
            backgroundImage = itemView.findViewById(R.id.item_list_article_custom_image_img_background);
            panelTitle = itemView.findViewById(R.id.item_list_article_custom_image_panel_title);
            if (mSection == Constant.Section.AISH) {
                containerPanel = itemView.findViewById(R.id.item_list_article_aish_panel_cardview);
                bottomLine = itemView.findViewById(R.id.item_list_article_aish_bottom_line);
                bottomInnerLine = itemView.findViewById(R.id.item_list_article_aish_bottom_inner_line);

            }
            itemView.setOnClickListener(this);
        }

        @Override
        void bind(ArticleModel item) {
            date.setText(item.publishDate);
            title.setText(item.title);
            category.setText(item.category);
            Glide.with(context).load(item.backgroundImageUrl).apply(new RequestOptions()).into(backgroundImage);
            switch (mSection) {
                case Constant.Section.TV:
                    break;

                case Constant.Section.NEWS:
                    category.setTextColor(ContextCompat.getColor(context, R.color.alaan_akhbar_primary));
                    panelTitle.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_bg_custom_image_akhbar));
                    break;

                case Constant.Section.FM:
                    break;

                case Constant.Section.AISH:
                    if (mStyle == ArticleAdapter.STYLE_DASHBOARD) {
                        bottomLine.setVisibility(View.GONE);
                        bottomInnerLine.setVisibility(View.VISIBLE);
                        containerPanel.setRadius(Convertor.convertDpToPixel(8, context));
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(containerMargin, containerMargin, containerMargin, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    } else {
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(0, containerMargin, 0, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    }
                    break;
            }

            category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((IPageController) context).loadFragment(ArticleFragment.newInstance(0));
                }
            });
        }
    }

    public class GalleryViewHolder extends MyViewHolder implements View.OnClickListener {

        private TextView date;
        private TextView title;
        private TextView category;
        private ViewPager viewPager;
        private LinearLayout dotsLayout;
        private CardView containerPanel;
        private View bottomLine;
        private View bottomInnerLine;

        public GalleryViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.item_list_article_gallery_tv_date);
            title = itemView.findViewById(R.id.item_list_article_gallery_tv_title);
            category = itemView.findViewById(R.id.item_list_article_gallery_tv_category);
            viewPager = itemView.findViewById(R.id.item_list_article_gallery_pager_imagelist);
            dotsLayout = itemView.findViewById(R.id.item_list_article_gallery_panel_dots);
            if (mSection == Constant.Section.AISH) {
                containerPanel = itemView.findViewById(R.id.item_list_article_aish_panel_cardview);
                bottomLine = itemView.findViewById(R.id.item_list_article_aish_bottom_line);
                bottomInnerLine = itemView.findViewById(R.id.item_list_article_aish_bottom_inner_line);
            }
            itemView.setOnClickListener(this);
        }

        @Override
        void bind(ArticleModel item) {
            date.setText(item.publishDate);
            title.setText(item.title);
            category.setText(item.category);
            GalleryPageAdapter adapter = new GalleryPageAdapter(context, item.galleryImageUrls);
            viewPager.setAdapter(adapter);
            addBottomDots(0, dotsLayout);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    addBottomDots(position, dotsLayout);
                }

                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }

            });

            switch (mSection) {
                case Constant.Section.TV:
                    break;

                case Constant.Section.NEWS:
                    category.setTextColor(ContextCompat.getColor(context, R.color.alaan_akhbar_primary));
                    break;

                case Constant.Section.FM:
                    break;

                case Constant.Section.AISH:
                    if (mStyle == ArticleAdapter.STYLE_DASHBOARD) {
                        bottomLine.setVisibility(View.GONE);
                        bottomInnerLine.setVisibility(View.VISIBLE);
                        containerPanel.setRadius(Convertor.convertDpToPixel(8, context));
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(containerMargin, containerMargin, containerMargin, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    } else {
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(0, containerMargin, 0, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    }
                    break;
            }

            category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((IPageController) context).loadFragment(ArticleFragment.newInstance(0));
                }
            });
        }

        private void addBottomDots(int currentPage, LinearLayout dotsLayout) {

            TextView[] dots = new TextView[Objects.requireNonNull(viewPager.getAdapter()).getCount()];

            dotsLayout.removeAllViews();
            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(context);
                dots[i].setText(fromHtml("&#9675;"));
                dots[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.gallery_dots_size));
                dots[i].setTextColor(context.getResources().getColor(R.color.white));
                dotsLayout.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[currentPage].setText(fromHtml("&#9679;"));

        }
    }

    public class HighlightViewHolder extends MyViewHolder implements View.OnClickListener {

        private RecyclerView list;
        private CardView containerPanel;

        public HighlightViewHolder(View itemView) {
            super(itemView);
            list = itemView.findViewById(R.id.item_list_article_highlight_list_articles);
            containerPanel = itemView.findViewById(R.id.item_list_article_aish_panel_cardview);
        }

        @Override
        void bind(ArticleModel item) {

            HighlightArticleAdapter adapter = new HighlightArticleAdapter(item.highlightArticleModels, new HighlightArticleAdapter.onItemClickListener() {
                @Override
                public void onItemClickListener(HighlightArticleModel model) {
                    ((IPageController) context).loadFragment(SpecificArticleFragment.newInstance());
                }
            });
            list.setLayoutManager(new LinearLayoutManager(context));
            list.setAdapter(adapter);

            switch (mSection) {
                case Constant.Section.TV:
                    break;

                case Constant.Section.NEWS:
                    break;

                case Constant.Section.FM:
                    break;

                case Constant.Section.AISH:
                    if (mStyle == ArticleAdapter.STYLE_DASHBOARD) {
                        containerPanel.setRadius(Convertor.convertDpToPixel(8, context));
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(containerMargin, containerMargin, containerMargin, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    } else {
                        int containerMargin = (int) Convertor.convertDpToPixel(8, context);
                        ConstraintLayout.LayoutParams layoutParams =
                                (ConstraintLayout.LayoutParams) containerPanel.getLayoutParams();
                        layoutParams.setMargins(0, containerMargin, 0, containerMargin);
                        containerPanel.setLayoutParams(layoutParams);
                        containerPanel.requestLayout();
                    }
                    break;
            }

        }
    }

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(ArticleModel model);
    }

    public void addArticles(List<ArticleModel> newData) {
        data.addAll(newData);
        notifyDataSetChanged();
    }
}