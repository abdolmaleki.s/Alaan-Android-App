package com.edoramedia.alaan.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.viewmodel.ArticleModel;

import java.util.List;

public class SpecificShowNewsAdapter extends RecyclerView.Adapter<SpecificShowNewsAdapter.ViewHolder> {

    private List<ArticleModel> data;
    private Context context;
    private OnItemClick mOnItemClick;

    public SpecificShowNewsAdapter(List<ArticleModel> data, Context ctx, OnItemClick onItemClick) {
        this.data = data;
        this.context = ctx;
        this.mOnItemClick = onItemClick;
    }

    @NonNull
    @Override
    public SpecificShowNewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_list_article_image, parent, false);
        return new SpecificShowNewsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SpecificShowNewsAdapter.ViewHolder holder, int position) {
        ArticleModel item = data.get(position);
        holder.date.setText(item.publishDate);
        holder.title.setText(item.title);
        holder.category.setText(item.category);
        Glide.with(context).load(item.backgroundImageUrl).apply(new RequestOptions()).into(holder.backgroundImage);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView date;
        private TextView title;
        private TextView category;
        private ImageView backgroundImage;

        ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.item_list_article_image_tv_date);
            title = itemView.findViewById(R.id.item_list_article_image_tv_title);
            category = itemView.findViewById(R.id.item_list_article_image_tv_category);
            backgroundImage = itemView.findViewById(R.id.item_list_article_image_img_background);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClick.onItemClick(getLayoutPosition(), data.get(getLayoutPosition()));
        }
    }

    public interface OnItemClick {
        void onItemClick(int position, ArticleModel model);
    }

}
