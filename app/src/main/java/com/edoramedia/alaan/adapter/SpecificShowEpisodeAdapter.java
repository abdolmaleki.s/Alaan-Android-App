package com.edoramedia.alaan.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.viewmodel.ShowEpisodeModel;
import com.joooonho.SelectableRoundedImageView;

import java.util.List;

public class SpecificShowEpisodeAdapter extends RecyclerView.Adapter<SpecificShowEpisodeAdapter.ViewHolder> {

    private List<ShowEpisodeModel> data;
    private Context context;
    private OnItemClick mOnItemClick;

    public SpecificShowEpisodeAdapter(List<ShowEpisodeModel> data, Context ctx, OnItemClick onItemClick) {
        this.data = data;
        this.context = ctx;
        this.mOnItemClick = onItemClick;
    }

    @NonNull
    @Override
    public SpecificShowEpisodeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_list_specific_show_episod, parent, false);
        return new SpecificShowEpisodeAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SpecificShowEpisodeAdapter.ViewHolder holder, int position) {
        ShowEpisodeModel currentDate = data.get(position);
        holder.title.setText(currentDate.title);
        holder.number.setText("الحلقه " + currentDate.number
        );
        Glide.with(context).load(currentDate.imageUrl).apply(new RequestOptions().circleCrop()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView title;
        public TextView number;
        public SelectableRoundedImageView image;

        ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_list_specific_episode_tv_title);
            number = itemView.findViewById(R.id.item_list_specific_episode_tv_number);
            image = itemView.findViewById(R.id.item_list_specific_episode_img_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClick.onItemClick(getLayoutPosition(), data.get(getLayoutPosition()));
        }
    }

    public interface OnItemClick {
        void onItemClick(int position, ShowEpisodeModel model);
    }

}
