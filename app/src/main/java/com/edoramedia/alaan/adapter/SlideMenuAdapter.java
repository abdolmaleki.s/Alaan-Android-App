package com.edoramedia.alaan.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.viewmodel.SlideMenuModel;
import java.util.List;

public class SlideMenuAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<SlideMenuModel> slideMenuModels;

    public SlideMenuAdapter(Context context, List<SlideMenuModel> slideMenuModels) {
        this.context = context;
        this.slideMenuModels = slideMenuModels;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.slideMenuModels.get(listPosition).subMenus.get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = slideMenuModels.get(listPosition).subMenus.get(expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_list_slide_menu_submenu, null);
        }
        TextView detailTitle = convertView
                .findViewById(R.id.item_list_slide_menu_submenu_title);
        detailTitle.setText(expandedListText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.slideMenuModels.get(listPosition).subMenus.size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.slideMenuModels.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.slideMenuModels.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        final SlideMenuModel currentModel = ((SlideMenuModel) getGroup(listPosition));

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_list_slide_menu, null);
        }
        TextView title = convertView.findViewById(R.id.item_list_slide_menu_tv_title);
        TextView menuIcon = convertView.findViewById(R.id.item_list_slide_menu_tv_icon);
        TextView plusIcon = convertView.findViewById(R.id.item_list_slide_menu_tv_plus);

        if (currentModel.subMenus != null && currentModel.subMenus.size() > 0) {
            plusIcon.setVisibility(View.VISIBLE);
            title.setTextColor(ContextCompat.getColor(context, R.color.alan_tv_primary));
            menuIcon.setTextColor(ContextCompat.getColor(context, R.color.alan_tv_primary));
        } else {
            plusIcon.setVisibility(View.GONE);
            title.setTextColor(ContextCompat.getColor(context, R.color.gray_icon_drawer));
            menuIcon.setTextColor(ContextCompat.getColor(context, R.color.gray_icon_drawer));
        }

        if (isExpanded) {
            plusIcon.setText(context.getString(R.string.faw_minus));
        } else {
            plusIcon.setText(context.getString(R.string.faw_plus));
        }

        title.setText(currentModel.getTitle());
        menuIcon.setText(currentModel.getIcon());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }
}

