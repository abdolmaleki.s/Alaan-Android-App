package com.edoramedia.alaan.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.infrastructure.Convertor;
import com.edoramedia.alaan.viewmodel.HighlightArticleModel;

import java.util.List;

public class HighlightArticleAdapter extends RecyclerView.Adapter<HighlightArticleAdapter.ViewHolder> {

    private List<HighlightArticleModel> data;
    private onItemClickListener onItemClickListener;

    public HighlightArticleAdapter(List<HighlightArticleModel> data, onItemClickListener onItemClickListener) {
        this.data = data;
        this.onItemClickListener = onItemClickListener;

    }

    @Override
    public HighlightArticleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_list_highlight_article, parent, false);
        return new HighlightArticleAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(HighlightArticleAdapter.ViewHolder holder, int position) {
        holder.title.setText(data.get(position).title);
        holder.dot.setText(Convertor.fromHtml("&#9679;"));

        if (position == data.size() - 1) {
            holder.hrLine.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView title;
        private TextView dot;
        private View hrLine;

        ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_list_highlight_article_tv_title);
            dot = itemView.findViewById(R.id.item_list_highlight_article_tv_dot);
            hrLine = itemView.findViewById(R.id.hr_line);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClickListener(data.get(getLayoutPosition()));
        }
    }

    public interface onItemClickListener {
        void onItemClickListener(HighlightArticleModel model);
    }
}
