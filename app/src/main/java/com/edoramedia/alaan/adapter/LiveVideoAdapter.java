package com.edoramedia.alaan.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.customview.TextViewEx;
import com.edoramedia.alaan.viewmodel.LiveVideoModel;

import java.util.List;

public class LiveVideoAdapter extends RecyclerView.Adapter<LiveVideoAdapter.ViewHolder> {

    private List<LiveVideoModel> data;
    private Context context;
    private OnItemClick mOnItemClick;

    public LiveVideoAdapter(List<LiveVideoModel> data, Context ctx, OnItemClick onItemClick) {
        this.data = data;
        this.context = ctx;
        this.mOnItemClick = onItemClick;
    }

    @NonNull
    @Override
    public LiveVideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_list_live_video, parent, false);
        return new LiveVideoAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final LiveVideoAdapter.ViewHolder holder, int position) {
        final LiveVideoModel currentDate = data.get(position);
        holder.description.setText(currentDate.description);
        Glide.with(context).load(currentDate.imageUrl).apply(new RequestOptions()).into(holder.imageUrl);
        holder.infoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!currentDate.isDescriptionVisible) {
                    YoYo.with(Techniques.FlipInX)
                            .duration(700)
                            .playOn(holder.description);

                    holder.description.setVisibility(View.VISIBLE);
                    holder.imageUrl.setVisibility(View.GONE);
                } else {
                    YoYo.with(Techniques.FlipInX)
                            .duration(700)
                            .playOn(holder.imageUrl);

                    holder.description.setVisibility(View.GONE);
                    holder.imageUrl.setVisibility(View.VISIBLE);
                }
                currentDate.isDescriptionVisible = !currentDate.isDescriptionVisible;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextViewEx description;
        private ImageView imageUrl;
        private TextView infoIcon;

        ViewHolder(View itemView) {
            super(itemView);
            description = itemView.findViewById(R.id.item_list_live_video_tv_description);
            imageUrl = itemView.findViewById(R.id.item_list_live_video_iv_background);
            infoIcon = itemView.findViewById(R.id.item_list_live_video_btn_info);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClick.onItemClick(getLayoutPosition(), data.get(getLayoutPosition()));
        }
    }

    public interface OnItemClick {
        void onItemClick(int position, LiveVideoModel model);
    }

}
