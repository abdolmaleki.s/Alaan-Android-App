package com.edoramedia.alaan.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.viewmodel.ShowModel;
import com.joooonho.SelectableRoundedImageView;

import java.util.List;

public class ShowAdapter extends RecyclerView.Adapter<ShowAdapter.ViewHolder> {

    private List<ShowModel> data;
    private Context context;
    private OnItemClick mOnItemClick;

    public ShowAdapter(List<ShowModel> data, Context ctx, OnItemClick onItemClick) {
        this.data = data;
        this.context = ctx;
        this.mOnItemClick = onItemClick;
    }

    @NonNull
    @Override
    public ShowAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_list_show, parent, false);
        return new ShowAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ShowAdapter.ViewHolder holder, int position) {
        ShowModel currentDate = data.get(position);
        holder.title.setText(currentDate.title);
        holder.weekday.setText(currentDate.weekDays);
        holder.time.setText(currentDate.time);
        Glide.with(context).load(currentDate.imageUrl).apply(new RequestOptions()).into(holder.image);
        Glide.with(context).load(currentDate.logoUrl).apply(new RequestOptions().circleCrop()).into(holder.logo);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView title;
        private TextView time;
        private TextView weekday;
        private SelectableRoundedImageView image;
        private ImageView logo;

        ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_list_show_tv_title);
            time = itemView.findViewById(R.id.item_list_show_tv_time);
            weekday = itemView.findViewById(R.id.item_list_show_tv_weekday);
            logo = itemView.findViewById(R.id.item_list_show_img_logo);
            image = itemView.findViewById(R.id.item_list_specific_episode_img_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnItemClick.onItemClick(getLayoutPosition(), data.get(getLayoutPosition()));
        }
    }

    public interface OnItemClick {
        void onItemClick(int position, ShowModel model);
    }

}
