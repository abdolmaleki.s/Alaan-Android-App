package com.edoramedia.alaan.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.infrastructure.Convertor;
import com.edoramedia.alaan.viewmodel.ScheduleProgramModel;

import java.util.List;

public class ScheduleProgramAdapter extends RecyclerView.Adapter<ScheduleProgramAdapter.MyViewHolder> {

    private List<ScheduleProgramModel> mProgramModels;
    private Context mContext;
    private int mFixTimeWidthPiexl;
    private OnItemClickListener onItemClickListener;

    public ScheduleProgramAdapter(Context context, List<ScheduleProgramModel> locations, OnItemClickListener onItemClickListener) {
        this.mProgramModels = locations;
        this.mContext = context;
        this.onItemClickListener = onItemClickListener;
        mFixTimeWidthPiexl = context.getResources().getDimensionPixelSize(R.dimen.schedule_programm_time_item_width);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_schedule_program, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, final int position) {
        final ScheduleProgramModel currentData = mProgramModels.get(position);
        viewHolder.title.setText(currentData.title);
        Glide.with(mContext).load(currentData.imageUrl).apply(new RequestOptions().circleCrop()
        ).into(viewHolder.image);
        int itemWidth = computeItemWidth(currentData.startDate, currentData.endDate);
        int itemMarginRight;
        if (position != 0) {
            itemMarginRight = computeItemRightMargin(currentData.startDate, mProgramModels.get(position - 1).endDate);
        } else {
            itemMarginRight = computeItemRightMargin(currentData.startDate, "00:00");
        }

        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) viewHolder.contentPanel.getLayoutParams();
        layoutParams.width = itemWidth;
        layoutParams.setMargins(0, 0, itemMarginRight, 0);
        viewHolder.contentPanel.setLayoutParams(layoutParams);

        if (currentData.isPlayingNow) {
            viewHolder.playIcon.setVisibility(View.VISIBLE);
            viewHolder.contentPanel.setBackground(ContextCompat.getDrawable(mContext, R.drawable.shape_bg_item_schedule_white));
        }

    }

    @Override
    public int getItemCount() {
        return mProgramModels.size();
    }

    public int computeItemWidth(String startTime, String endTime) {
        int startMinutes = Convertor.convertTimeToMinutes(startTime);
        int EndMinuts = Convertor.convertTimeToMinutes(endTime);
        int widthMinuts = EndMinuts - startMinutes;
        int widthPixel = (widthMinuts * mFixTimeWidthPiexl) / 60;
        return widthPixel;
    }

    public int computeItemRightMargin(String startTime, String previousItemEndTime) {
        int startMinutes = Convertor.convertTimeToMinutes(startTime);
        int previousItemEndMinuts = Convertor.convertTimeToMinutes(previousItemEndTime);
        int marginMinuts = startMinutes - previousItemEndMinuts;
        int marginPixel = (marginMinuts * mFixTimeWidthPiexl) / 60;
        return marginPixel;

    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView ueaTime;
        TextView ksaTime;
        TextView title;
        ImageView image;
        TextView playIcon;
        ConstraintLayout contentPanel;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ueaTime = itemView.findViewById(R.id.item_list_schedule_program_tv_time_uea);
            ksaTime = itemView.findViewById(R.id.item_list_schedule_program_tv_time_ksa);
            title = itemView.findViewById(R.id.item_list_schedule_program_tv_title);
            image = itemView.findViewById(R.id.item_list_schedule_program_img_image);
            playIcon = itemView.findViewById(R.id.item_list_schedule_program_tv_play_icon);
            contentPanel = itemView.findViewById(R.id.item_list_schedule_panel_content);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onClick(mProgramModels.get(getLayoutPosition()));
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onClick(ScheduleProgramModel model);
    }
}
