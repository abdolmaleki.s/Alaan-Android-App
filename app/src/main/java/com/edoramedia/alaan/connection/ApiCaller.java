package com.edoramedia.alaan.connection;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;

import com.edoramedia.alaan.Interface.IPageController;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.connection.dto.BaseDto;
import com.edoramedia.alaan.connection.som.BaseSom;
import com.edoramedia.alaan.fragment.NetProblemFragment;
import com.edoramedia.alaan.infrastructure.DontObfuscate;
import com.edoramedia.alaan.infrastructure.Helper;
import com.edoramedia.alaan.setting.AppSetting;
import com.google.gson.JsonObject;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Api utility class for calling webservices
 */
@DontObfuscate
public class ApiCaller {

    private int mApiType;
    private Call<JsonObject> mToken = null;
    private final static String OS = "android";
    private boolean mIsAutoFinishing = true;
    private boolean IsDisplayProgrssDialog = true;
    private AppSetting appSetting;

    /**
     * Instantiates a new Api caller.
     *
     * @param apiType the api type
     */
    public ApiCaller(int apiType) {
        this.mApiType = apiType;
    }

    /**
     * Instantiates a new Api caller.
     *
     * @param apiType       the api type
     * @param autoFinishing indicate this is last api calling or not
     */
    public ApiCaller(int apiType, boolean autoFinishing) {
        this.mApiType = apiType;
        mIsAutoFinishing = autoFinishing;
    }

    /**
     * Instantiates a new Api caller.
     *
     * @param apiType               the api type
     * @param autoFinishing         indicate this is last api calling or not
     * @param displayProgressDialog indicate needing show progressbar during calling webservice
     */
    public ApiCaller(int apiType, boolean autoFinishing, boolean displayProgressDialog) {
        this.mApiType = apiType;
        mIsAutoFinishing = autoFinishing;
        IsDisplayProgrssDialog = displayProgressDialog;
    }

    //
    //     _    ____ ___     ____             __ _
    //    / \  |  _ \_ _|   / ___|___  _ __  / _(_) __ _
    //   / _ \ | |_) | |   | |   / _ \| '_ \| |_| |/ _` |
    //  / ___ \|  __/| |   | |__| (_) | | | |  _| | (_| |
    // /_/   \_\_|  |___|   \____\___/|_| |_|_| |_|\__, |
    //                                             |___/

    /**
     * Call.
     *
     * @param ctx         the ctx
     * @param dto         the object that contains requset parameters
     * @param apiCallback the callback that returns api response/error
     */
    public void call(final Context ctx, BaseDto dto, final ApiCallback apiCallback) {

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////// show progressbar if needed ////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (IsDisplayProgrssDialog) {
            Helper.progressBar.showDialog(ctx);
        }

        try {

            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            ANApi apiService = retrofit.create(ANApi.class);

            //
            //     _    ____ ___   _____                   _   _                 _      _
            //    / \  |  _ \_ _| |_   _|   _ _ __   ___  | | | | __ _ _ __   __| | ___| |
            //   / _ \ | |_) | |    | || | | | '_ \ / _ \ | |_| |/ _` | '_ \ / _` |/ _ \ |
            //  / ___ \|  __/| |    | || |_| | |_) |  __/ |  _  | (_| | | | | (_| |  __/ |
            // /_/   \_\_|  |___|   |_| \__, | .__/ \___| |_| |_|\__,_|_| |_|\__,_|\___|_|
            //                          |___/|_|

            appSetting = AppSetting.getInstance();

            switch (mApiType) {
                case Constant.ApiTypes.LOGIN:
                    //LoginDto loginDto = (LoginDto) dto;
                    //mToken = apiService.login(Constant.API_FIX_TOKEN, loginDto.username, loginDto.password, appLanguage);
                    break;

            }

            mToken.enqueue(new retrofit2.Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    JsonObject jsonObject = response.body();
                    if (jsonObject == null) {
                        Helper.progressBar.hideDialog();
                        apiCallback.onFail(ctx.getString(R.string.server_connection_error));

                    } else {
                        try {
                            BaseSom baseSom = Helper.getGson().fromJson(jsonObject.toString(), BaseSom.class);
                            if (!baseSom.success) {
                                Helper.progressBar.hideDialog();
                            }

                            if (mIsAutoFinishing) {
                                Helper.progressBar.hideDialog();
                            }
                            apiCallback.onResponse(response.code(), jsonObject);


                        } catch (Exception e) {
                            Helper.progressBar.hideDialog();
                            apiCallback.onFail(ctx.getString(R.string.get_information_error));
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                    Helper.progressBar.hideDialog();
                    if (t instanceof java.net.ConnectException || t instanceof SocketTimeoutException || t instanceof UnknownHostException) {
                        showNetProblemPage(ctx);
                    }
                }
            });

        } catch (Exception e) {
            Helper.progressBar.hideDialog();
        }
    }

    private void showNetProblemPage(Context context) {

        if (context instanceof IPageController) {
            ((IPageController) context).loadFragment(NetProblemFragment.newInstance());

        } else {
            DialogFragment fragment = NetProblemFragment.newInstance();
            fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogFragment);
            fragment.show(((AppCompatActivity) context).getSupportFragmentManager(), "DialogFragment");
        }
    }

    /**
     * The interface Api callback.
     */
    public interface ApiCallback {

        /**
         * return api response.
         *
         * @param responseCode the response code
         * @param jsonObject   the json object
         */
        void onResponse(int responseCode, JsonObject jsonObject);

        /**
         * return failed message.
         *
         * @param message the message
         */
        void onFail(String message);
    }
}
