package com.edoramedia.alaan.connection;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ANApi {

    @FormUrlEncoded
    @POST("user/login")
    Call<JsonObject> login(@Header("token") String token, @Field("username") String userName, @Field("password") String password, @Field("locale") String lang);

}

