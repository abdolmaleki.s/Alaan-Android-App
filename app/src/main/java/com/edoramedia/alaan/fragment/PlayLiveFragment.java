package com.edoramedia.alaan.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.adapter.LiveVideoAdapter;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.infrastructure.Convertor;
import com.edoramedia.alaan.infrastructure.SpacesItemDecorationLiveVideo;
import com.edoramedia.alaan.viewmodel.LiveVideoModel;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Play Live stream in aish section.
 */

public class PlayLiveFragment extends BaseFragment implements View.OnClickListener, TabLayout.BaseOnTabSelectedListener {

    private FrameLayout mPanelPlayer;
    private RecyclerView mListOtherLives;
    private TabLayout mTabLayout_tabs;
    private ConstraintLayout mPanelDescription;
    private TextView mTV_refreshIcon;
    private TextView mIMG_fullScreen;
    private Dialog mFullScreenDialog;
    private boolean mIsPlayerFullscreen = false;
    private LinearLayout mPanelLiveList;
    private ImageView mIV_avatar;

    //////////// Amazon ////////////////////////////
    private SimpleExoPlayer mExoPlayer;
    private PlayerView mExoPlayerView;
    /**
     * The M stream url.
     */
    public String mStreamUrl = "http://www.streambox.fr/playlists/test_001/stream.m3u8";
    private LinearLayout mPanel_refresh;
    private TextView mTV_changeQuality;


    /**
     * New instance play live fragment.
     *
     * @return the play live fragment
     */
    public static PlayLiveFragment newInstance() {
        PlayLiveFragment fragment = new PlayLiveFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_play_live, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePlayer();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        resumePlayer();
    }

    @Override
    public void onDestroy() {
        releasePlayer();
        super.onDestroy();
    }

    @Override
    void initToolbar() {
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == R.id.toolbar_ic_back) {
            mPageController.goBack();
        } else if (id == mIMG_fullScreen.getId()) {

            if (!mIsPlayerFullscreen)
                openFullscreenDialog();
            else
                closeFullscreenDialog();
        } else if (id == mTV_refreshIcon.getId()) {
            tryToPlay();
        } else if (id == R.id.toolbar_label_share) {
            shareLink();
        }


    }

    private void initView(View rootView) {
        bindView(rootView);
        initTabLayout();
        initExoPlayer();

    }

    private void initAdapter() {
        List<LiveVideoModel> modelList = new ArrayList<>();
        LiveVideoModel model1 = new LiveVideoModel();
        LiveVideoModel model2 = new LiveVideoModel();
        LiveVideoModel model3 = new LiveVideoModel();
        LiveVideoModel model4 = new LiveVideoModel();
        LiveVideoModel model5 = new LiveVideoModel();
        LiveVideoModel model6 = new LiveVideoModel();
        LiveVideoModel model7 = new LiveVideoModel();
        LiveVideoModel model8 = new LiveVideoModel();

        model1.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model1.description = "هذة توضیحات لالفیدئو";

        model8.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model8.description = "هذة توضیحات لالفیدئو";

        model2.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model2.description = "هذة توضیحات لالفیدئو";

        model3.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model3.description = "هذة توضیحات لالفیدئو";

        model4.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model4.description = "هذة توضیحات لالفیدئو";

        model5.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model5.description = "هذة توضیحات لالفیدئو";

        model6.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model6.description = "هذة توضیحات لالفیدئو";

        model7.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model7.description = "هذة توضیحات لالفیدئو";

        modelList.add(model1);
        modelList.add(model2);
        modelList.add(model3);
        modelList.add(model4);
        modelList.add(model5);
        modelList.add(model6);
        modelList.add(model7);
        modelList.add(model8);

        LiveVideoAdapter liveVideoAdapter = new LiveVideoAdapter(modelList, mActivity, new LiveVideoAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position, LiveVideoModel model) {

            }
        });

        mListOtherLives.setLayoutManager(new GridLayoutManager(mActivity, 2));
        mListOtherLives.setAdapter(liveVideoAdapter);
    }

    private void bindView(View rootView) {

        mPageController.needBottomNavigation(true);
        mPanelDescription = rootView.findViewById(R.id.fragment_play_live_panel_description);
        mPanelPlayer = rootView.findViewById(R.id.fragment_play_live_panel_player);
        mTabLayout_tabs = rootView.findViewById(R.id.fragment_play_live_tabs);
        mListOtherLives = rootView.findViewById(R.id.fragment_play_live_list_other_lives);
        mExoPlayerView = rootView.findViewById(R.id.fragment_play_live_player_amazon);
        mPanel_refresh = rootView.findViewById(R.id.fragment_play_live_panel_refresh);
        mTV_refreshIcon = rootView.findViewById(R.id.fragment_play_live_ic_refresh);
        mPanelLiveList = rootView.findViewById(R.id.fragment_play_live_panel_live_list);
        mIV_avatar = rootView.findViewById(R.id.fragment_play_live_iv_avatar);
        mIMG_fullScreen = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mTV_changeQuality = mExoPlayerView.findViewById(R.id.frame_exo_control_view_live_btn_change_quality);
        mTV_changeQuality.setVisibility(View.GONE);
        mIMG_fullScreen.setOnClickListener(this);
        mTV_refreshIcon.setOnClickListener(this);
        mListOtherLives.addItemDecoration(new SpacesItemDecorationLiveVideo((int) Convertor.convertDpToPixel(2f, mActivity)));

        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_ic_back).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_label_share).setOnClickListener(this);

        initToolbar();
    }

    private void initExoPlayer() {
        mExoPlayerView.setVisibility(View.VISIBLE);
        mExoPlayer = ExoPlayerFactory.newSimpleInstance(mActivity);
        mExoPlayerView.setPlayer(mExoPlayer);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(mActivity,
                Util.getUserAgent(mActivity, "app"));

        HlsMediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(mStreamUrl));
        mExoPlayer.prepare(videoSource);
        mExoPlayer.setPlayWhenReady(true);

        mExoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                        break;
                    case Player.STATE_ENDED:

                        break;
                    case Player.STATE_IDLE:

                        break;
                    case Player.STATE_READY:
                        hideRefreshPanel();

                        break;
                    default:
                        break;
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                showRefreshPanel();

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

    private void hideRefreshPanel() {
        mPanel_refresh.setVisibility(View.GONE);

    }

    private void showRefreshPanel() {
        mPanel_refresh.setVisibility(View.VISIBLE);

    }

    private void tryToPlay() {
        Animation animRotateAclk = AnimationUtils.loadAnimation(mActivity.getApplicationContext(), R.anim.anim_rotate_clock);
        animRotateAclk.setRepeatCount(Animation.INFINITE);
        mTV_refreshIcon.startAnimation(animRotateAclk);
        initExoPlayer();
    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mActivity, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mIsPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        ((AppCompatActivity) mActivity).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        initFullscreenDialog();
        ((ViewGroup) mExoPlayerView.getParent()).removeView(mExoPlayerView);
        ((ViewGroup) mPanel_refresh.getParent()).removeView(mPanel_refresh);
        mFullScreenDialog.addContentView(mExoPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenDialog.addContentView(mPanel_refresh, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mIMG_fullScreen.setText(R.string.faw_minimize);
        mIsPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    private void closeFullscreenDialog() {
        ((AppCompatActivity) mActivity).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ((ViewGroup) mExoPlayerView.getParent()).removeView(mExoPlayerView);
        ((ViewGroup) mPanel_refresh.getParent()).removeView(mPanel_refresh);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mPanelPlayer.addView(mExoPlayerView, layoutParams);
        mPanelPlayer.addView(mPanel_refresh, layoutParams);
        mIMG_fullScreen.setText(R.string.faw_maximize);
        mIsPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
    }

    private void releasePlayer() {
        mExoPlayer.release();
        mExoPlayer = null;
    }

    private void resumePlayer() {
        mExoPlayer.setPlayWhenReady(true);
    }

    private void pausePlayer() {
        mExoPlayer.setPlayWhenReady(false);

    }

    private void initTabLayout() {
        mTabLayout_tabs.addTab(mTabLayout_tabs.newTab().setText("تفاصیل"));
        mTabLayout_tabs.addTab(mTabLayout_tabs.newTab().setText("البث المباشر"));

        mTabLayout_tabs.addOnTabSelectedListener(this);

        for (int i = 0; i < mTabLayout_tabs.getTabCount(); i++) {
            TextView tv = (TextView) LayoutInflater.from(mActivity).inflate(R.layout.item_tab_specific_show, null);
            if (mTabLayout_tabs.getTabAt(i).isSelected()) {
                tv.setTextColor(ContextCompat.getColor(mActivity, R.color.alan_tv_primary));

            }
            mTabLayout_tabs.getTabAt(i).setCustomView(tv);
        }

        onTabSelected(mTabLayout_tabs.getTabAt(0));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        ((TextView) tab.getCustomView()).setTextColor(ContextCompat.getColor(mActivity, R.color.alan_tv_primary));
        int position = tab.getPosition();
        switch (position) {
            case 0:
                mPanelLiveList.setVisibility(View.GONE);
                mPanelDescription.setVisibility(View.VISIBLE);
                Glide.with(this).load(Constant.FAKE_AUTHOR_IMAGE_URL).apply(new RequestOptions().circleCrop()).into(mIV_avatar);
                break;
            case 1:
                mPanelDescription.setVisibility(View.GONE);
                mPanelLiveList.setVisibility(View.VISIBLE);
                initAdapter();
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        ((TextView) tab.getCustomView()).setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void shareLink() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Arshanikan");
            String sAux = "\nبا اپلیکیشن آرشانیکان زمین تنیس رزرو کن حالشو ببر\n\n";
            sAux = sAux + "<https://play.google.com/store/apps/details?id=com.edorosoft.edorok12 \n\n>";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "اختر خيارا"));
        } catch (Exception e) {
            //e.toString();
        }
    }

}
