package com.edoramedia.alaan.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.activity.PublishLiveActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;


/**
 * this page is for request live stream in aish section
 */
public class LivePublishDetailsFragment extends BaseFragment implements View.OnClickListener {

    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_requestLive;
    private TextView mTV_back;

    /**
     * New instance live publish details fragment.
     *
     * @return the live publish details fragment
     */
    public static LivePublishDetailsFragment newInstance() {
        LivePublishDetailsFragment fragment = new LivePublishDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_live_publish_detail, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(false);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);
        mTV_requestLive = rootView.findViewById(R.id.fragment_live_publish_detail_btn_publish_request);
        mTV_back = rootView.findViewById(R.id.toolbar_left_icon);
        mTV_back.setOnClickListener(this);
        mTV_requestLive.setOnClickListener(this);
        initToolbar();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mTV_requestLive.getId()) {
            checkPermissions();

        } else if (id == mTV_back.getId()) {
            mPageController.goBack();
        }
    }


    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                mPageController.loadFragment(LivePublishEndFragment.newInstance());
            } else if (resultCode == Activity.RESULT_CANCELED) {
                mPageController.loadFragment(LivePublishDetailsFragment.newInstance());

            }
        }
    }

    private void GoLiveBroadcasting() {
        Intent intent = new Intent(mActivity, PublishLiveActivity.class);
        startActivityForResult(intent, 1);
    }

    private void checkPermissions() {

        Dexter.withActivity((Activity) mActivity)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    GoLiveBroadcasting();
                } else {

                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }
}
