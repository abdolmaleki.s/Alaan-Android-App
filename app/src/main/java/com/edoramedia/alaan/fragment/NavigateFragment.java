package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.setting.AppSetting;

/**
 * navigation page that have 4 button for navigation to 4 section (tv/akhbar/aish/fm)
 */
public class NavigateFragment extends BaseFragment implements View.OnClickListener {

    private ImageView mIMG_liveShow;
    private LinearLayout mBTN_tv;
    private LinearLayout mBTN_fm;
    private LinearLayout mBTN_akhbar;
    private LinearLayout mBTN_aish;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_humbergerIcon;


    /**
     * New instance navigate fragment.
     *
     * @return the navigate fragment
     */
    public static NavigateFragment newInstance() {
        NavigateFragment fragment = new NavigateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_navigate, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        mIMG_liveShow = rootView.findViewById(R.id.toolbar_img_live);
        mIMG_liveShow.setOnClickListener(this);

        mBTN_tv = rootView.findViewById(R.id.fragment_navigate_btn_tv);
        mBTN_fm = rootView.findViewById(R.id.fragment_navigate_btn_fm);
        mBTN_akhbar = rootView.findViewById(R.id.fragment_navigate_btn_akhbar);
        mBTN_aish = rootView.findViewById(R.id.fragment_navigate_btn_aish);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);

        mBTN_tv.setOnClickListener(this);
        mBTN_fm.setOnClickListener(this);
        mBTN_akhbar.setOnClickListener(this);
        mBTN_aish.setOnClickListener(this);

        setSectionTheme();

    }

    private void setSectionTheme() {
        int section = AppSetting.getInstance().getAppSection();
        switch (section) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                mIV_toolbarSetionImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_outlet_news));
                mPanelToolbar.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.alaan_akhbar_primary));
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_fm_icon_width);
                mIV_toolbarSetionImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_fm_toolbar));
                mPanelToolbar.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.alaan_FM_primary));
                mIMG_liveShow.setVisibility(View.GONE);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mIV_toolbarSetionImage.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_outlet_aish));
                mPanelToolbar.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.alaan_aish_primary));
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mIMG_liveShow.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_aish_video_camera));
                mIMG_liveShow.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_left_icon_width);
                mIMG_liveShow.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_left_icon_width);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mIMG_liveShow.getId()) {
            int section = AppSetting.getInstance().getAppSection();
            if (section == Constant.Section.AISH) {
                mPageController.loadFragment(LiveVideoListingFragment.newInstance());
            } else {
                mPageController.loadFragment(LiveTVFragment.newInstance());
            }
        } else if (id == mBTN_tv.getId()) {
            mPageController.setSection(Constant.Section.TV);
            mPageController.loadFragment(DashboardFragment.newInstance());
        } else if (id == mBTN_akhbar.getId()) {
            mPageController.setSection(Constant.Section.NEWS);
            mPageController.loadFragment(DashboardFragment.newInstance());
        } else if (id == mBTN_fm.getId()) {
            mPageController.setSection(Constant.Section.FM);
            mPageController.loadFragment(DashboardFragment.newInstance());
        } else if (id == mBTN_aish.getId()) {
            mPageController.setSection(Constant.Section.AISH);
            mPageController.loadFragment(DashboardFragment.newInstance());
        }
    }

    @Override
    void initToolbar() {

    }
}
