package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;

/**
 * The type Comming soon fragment.
 */
public class CommingSoonFragment extends BaseFragment implements View.OnClickListener {

    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private ConstraintLayout mPanelContent;
    private ImageView mIV_background;
    private Button mBTN_submit;
    private TextView mTV_humbergerIcon;
    private ImageView mIMG_liveShow;

    /**
     * New instance comming soon fragment.
     *
     * @return the comming soon fragment
     */
    public static CommingSoonFragment newInstance() {
        CommingSoonFragment fragment = new CommingSoonFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_comming_soon, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);
        mIMG_liveShow = rootView.findViewById(R.id.toolbar_iv_live);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_iv_live).setOnClickListener(this);
        mPanelContent = rootView.findViewById(R.id.fragment_coming_soon_panel_main);
        mIV_background = rootView.findViewById(R.id.fragment_coming_soon_iv_background);
        mBTN_submit = rootView.findViewById(R.id.fragment_coming_soon_btn_submit);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);
        initToolbar();
        setSectionThem();
    }

    private void setSectionThem() {
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mPanelContent.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.alaan_akhbar_primary));
                mBTN_submit.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.alaan_akhbar_primary));
                mIV_background.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_bg_comming_soon_blue
                ));
                break;

            case Constant.Section.FM:
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);

                break;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == R.id.toolbar_iv_live) {
            mPageController.loadFragment(LiveTVFragment.newInstance());
        }
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mIMG_liveShow.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_aish_video_camera));
                mIMG_liveShow.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_left_icon_width);
                mIMG_liveShow.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_left_icon_width);
                break;
        }
    }
}
