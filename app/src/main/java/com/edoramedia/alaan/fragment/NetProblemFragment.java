package com.edoramedia.alaan.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.edoramedia.alaan.Interface.IPageController;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.infrastructure.NetworkHelper;
import com.github.ybq.android.spinkit.style.Wave;

/**
 * this page showing when we have internet connection problem
 */
public class NetProblemFragment extends DialogFragment implements View.OnClickListener {

    private FrameLayout mBTN_retry;
    private TextView mTV_retry;
    private ImageView mImg_progress;
    private Handler mHandler;
    private IPageController mPageController = null;


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IPageController) {
            mPageController = (IPageController) context;
        }
    }

    /**
     * New instance net problem fragment.
     *
     * @return the net problem fragment
     */
    public static NetProblemFragment newInstance() {
        NetProblemFragment fragment = new NetProblemFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_net_problem, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mBTN_retry = rootView.findViewById(R.id.fragment_net_problem_btn_retry);
        mTV_retry = rootView.findViewById(R.id.fragment_net_problem_tv_retry);
        mImg_progress = rootView.findViewById(R.id.fragment_net_problem_img_progress);
        mHandler = new Handler(Looper.getMainLooper());
        mBTN_retry.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mBTN_retry.getId()) {
            Wave circle = new Wave();
            circle.setColor(Color.WHITE);
            circle.start();
            mImg_progress.setImageDrawable(circle);
            mTV_retry.setVisibility(View.INVISIBLE);
            mImg_progress.setVisibility(View.VISIBLE);


            NetworkHelper.isConnectingToInternet(getActivity(), new NetworkHelper.CheckNetworkStateListener() {
                @Override
                public void onNetworkChecked(boolean isSuccess) {
                    if (isSuccess) {

                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (mPageController != null) {
                                    mPageController.goBack();
                                } else {
                                    dismiss();
                                }
                            }
                        }, 2000);

                    } else {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mImg_progress.setVisibility(View.GONE);
                                mTV_retry.setVisibility(View.VISIBLE);
                            }
                        });

                    }
                }
            });
        }
    }
}
