package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.edoramedia.alaan.R;

/**
 * this is a sample fragment and not show in app . it will delete after releas
 */
public class SampleFragment extends BaseFragment implements View.OnClickListener {

    /**
     * New instance sample fragment.
     *
     * @return the sample fragment
     */
    public static SampleFragment newInstance() {
        SampleFragment fragment = new SampleFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sample, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        }
    }

    @Override
    void initToolbar() {

    }
}
