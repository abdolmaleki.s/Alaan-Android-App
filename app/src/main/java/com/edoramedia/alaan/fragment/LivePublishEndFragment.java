package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edoramedia.alaan.R;


/**
 * page that have successful message and duration after ending live stream
 */
public class LivePublishEndFragment extends BaseFragment implements View.OnClickListener {

    private TextView mTV_back;

    /**
     * New instance live publish end fragment.
     *
     * @return the live publish end fragment
     */
    public static LivePublishEndFragment newInstance() {
        LivePublishEndFragment fragment = new LivePublishEndFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_live_end, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(false);
        mTV_back = rootView.findViewById(R.id.fragment_live_end_btn_back);
        mTV_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mTV_back.getId()) {
            mPageController.goBack();
        }
    }

    @Override
    void initToolbar() {

    }
}
