package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.constraint.Guideline;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.adapter.ScheduleProgramAdapter;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.customview.CustomHorizontalScrollView;
import com.edoramedia.alaan.customview.CustomVerticalScrollView;
import com.edoramedia.alaan.infrastructure.DateHelper;
import com.edoramedia.alaan.infrastructure.IScrollListener;
import com.edoramedia.alaan.viewmodel.ScheduleProgramModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * TV/FM programs schedule
 */
public class ScheduleFragment extends BaseFragment implements View.OnClickListener, IScrollListener {

    private final String backimageUrl = "https://i.kinja-img.com/gawker-media/image/upload/s--GpcFH1UL--/c_scale,f_auto,fl_progressive,q_80,w_800/d6h1yrp2v7fvdsm5osem.jpg";

    private RecyclerView mListProgramsFirst;
    private RecyclerView mListProgramsSecond;
    private RecyclerView mListProgramsThird;
    private RecyclerView mListProgramsFourth;
    private RecyclerView mListProgramsFifth;
    private RecyclerView mListProgramsSixth;
    private RecyclerView mListProgramsSeventh;

    private CustomVerticalScrollView mScrollViewVerticalDays;
    private CustomVerticalScrollView mScrollViewVertiicalContent;
    private CustomHorizontalScrollView mScrollViewHorizontalTime;
    private CustomHorizontalScrollView mScrollViewHorizontalContent;

    private ImageView mIV_topBackground;
    private ImageView mIV_logo;
    private ImageView mIV_live;

    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;

    private LinearLayout mTimePanel;
    private LinearLayout mPanelTopic;
    private ConstraintLayout mPanelTop;
    private ConstraintLayout mPanelSchedule;
    private ConstraintLayout mPabelRoot;
    private Guideline mGuidline;
    private boolean mIsFullScreen;

    private TextView mTV_play;

    /**
     * New instance schedule fragment.
     *
     * @return the schedule fragment
     */
    public static ScheduleFragment newInstance() {
        ScheduleFragment fragment = new ScheduleFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_schedule, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        bindView(rootView);
        initTopSection();
        adjustListsSize();
        initAdapter();
        initToolbar();
        setSectionTheme();
        initDateColumns(rootView);
    }

    private void setSectionTheme() {
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                break;

            case Constant.Section.FM:
                mTimePanel.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.alaan_FM_primary));
                mPanelTopic.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.alaan_FM_primary));
                break;

            case Constant.Section.AISH:
                break;
        }
    }

    private void initTopSection() {
        Glide.with(this).load(backimageUrl).into(mIV_topBackground);
        Glide.with(this).load("http://www-hep.phys.cmu.edu/logos/NASA_logo.png").apply(new RequestOptions().circleCrop()).into(mIV_logo);
    }

    private void bindView(View rootView) {

        mListProgramsFirst = rootView.findViewById(R.id.fragment_schedule_list_program_first_day);
        mListProgramsSecond = rootView.findViewById(R.id.fragment_schedule_list_program_second_day);
        mListProgramsThird = rootView.findViewById(R.id.fragment_schedule_list_program_third_day);
        mListProgramsFourth = rootView.findViewById(R.id.fragment_schedule_list_program_fourth_day);
        mListProgramsFifth = rootView.findViewById(R.id.fragment_schedule_list_program_fifth_day);
        mListProgramsSixth = rootView.findViewById(R.id.fragment_schedule_list_program_sixth_day);
        mListProgramsSeventh = rootView.findViewById(R.id.fragment_schedule_list_program_seventh_day);

        mScrollViewVerticalDays = rootView.findViewById(R.id.fragment_schedule_scroll_vertical_Days);
        mScrollViewHorizontalTime = rootView.findViewById(R.id.fragment_schedule_scroll_horizontal_time);
        mScrollViewVertiicalContent = rootView.findViewById(R.id.fragment_schedule_scroll_vertical_content);
        mScrollViewHorizontalContent = rootView.findViewById(R.id.fragment_schedule_scroll_horizontal_content);
        mTV_play = rootView.findViewById(R.id.fragment_schedule_btn_play);

        mScrollViewVerticalDays.setScrollViewListener(this);
        mScrollViewVertiicalContent.setScrollViewListener(this);
        mScrollViewHorizontalTime.setScrollViewListener(this);
        mScrollViewHorizontalContent.setScrollViewListener(this);

        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);

        mIV_topBackground = rootView.findViewById(R.id.fragment_schedule_img_background);
        mIV_live = rootView.findViewById(R.id.toolbar_iv_live);
        mIV_logo = rootView.findViewById(R.id.fragment_schedule_img_logo);
        mTimePanel = rootView.findViewById(R.id.panel_time_content);
        mPanelTopic = rootView.findViewById(R.id.fragment_schedule_panel_schedule_topic);
        mPanelTop = rootView.findViewById(R.id.fragment_schedule_panel_top);
        mPanelSchedule = rootView.findViewById(R.id.fragment_schedule_panel_schedule_main);
        mPabelRoot = rootView.findViewById(R.id.fragment_schedule_panel_root);
        mGuidline = rootView.findViewById(R.id.guideline2);

        mIV_live.setOnClickListener(this);
        mPanelTopic.setOnClickListener(this);
        mTV_play.setOnClickListener(this);
    }

    private void adjustListsSize() {
        mListProgramsFirst.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        mListProgramsSecond.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        mListProgramsThird.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        mListProgramsFourth.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        mListProgramsFifth.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        mListProgramsSixth.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        mListProgramsSeventh.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));

        mListProgramsFirst.setNestedScrollingEnabled(false);
        mListProgramsSecond.setNestedScrollingEnabled(false);
        mListProgramsThird.setNestedScrollingEnabled(false);
        mListProgramsFourth.setNestedScrollingEnabled(false);
        mListProgramsFifth.setNestedScrollingEnabled(false);
        mListProgramsSixth.setNestedScrollingEnabled(false);
        mListProgramsSeventh.setNestedScrollingEnabled(false);

        int width = getResources().getDimensionPixelSize(R.dimen.schedule_programm_time_item_width) * 25;

        ViewGroup.LayoutParams params;
        params = mListProgramsFirst.getLayoutParams();
        params.width = width;
        mListProgramsFirst.setLayoutParams(params);

        params = mListProgramsSecond.getLayoutParams();
        params.width = width;
        mListProgramsSecond.setLayoutParams(params);

        params = mListProgramsThird.getLayoutParams();
        params.width = width;
        mListProgramsThird.setLayoutParams(params);

        params = mListProgramsFourth.getLayoutParams();
        params.width = width;
        mListProgramsFourth.setLayoutParams(params);

        params = mListProgramsFifth.getLayoutParams();
        params.width = width;
        mListProgramsFifth.setLayoutParams(params);

        params = mListProgramsSixth.getLayoutParams();
        params.width = width;
        mListProgramsSixth.setLayoutParams(params);

        params = mListProgramsSeventh.getLayoutParams();
        params.width = width;
        mListProgramsSeventh.setLayoutParams(params);
    }

    private void initAdapter() {
        int count = 1;
        List<ScheduleProgramModel> programModels = new ArrayList<>();

        ScheduleProgramModel model0 = new ScheduleProgramModel();
        ScheduleProgramModel model1 = new ScheduleProgramModel();
        ScheduleProgramModel model2 = new ScheduleProgramModel();
        ScheduleProgramModel model3 = new ScheduleProgramModel();
        ScheduleProgramModel model4 = new ScheduleProgramModel();
        ScheduleProgramModel model5 = new ScheduleProgramModel();
        ScheduleProgramModel model6 = new ScheduleProgramModel();
        ScheduleProgramModel model7 = new ScheduleProgramModel();
        ScheduleProgramModel model8 = new ScheduleProgramModel();
        ScheduleProgramModel model9 = new ScheduleProgramModel();
        ScheduleProgramModel model10 = new ScheduleProgramModel();

        model0.title = "الکومیدی شو " + count++;
        model0.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model0.startDate = "00:00";
        model0.endDate = "1:00";

        model1.title = "الکومیدی شو " + count++;
        model1.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model1.startDate = "01:00";
        model1.endDate = "02:00";

        model2.title = "الکومیدی شو " + count++;
        model2.isPlayingNow = true;
        model2.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model2.startDate = "02:00";
        model2.endDate = "03:00";

        model3.title = "الکومیدی شو " + count++;
        model3.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model3.startDate = "04:00";
        model3.endDate = "05:00";

        model4.title = "الکومیدی شو " + count++;
        model4.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model4.startDate = "05:30";
        model4.endDate = "06:00";

        model5.title = "الکومیدی شو " + count++;
        model5.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model5.startDate = "07:00";
        model5.endDate = "08:00";

        model6.title = "الکومیدی شو " + count++;
        model6.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model6.startDate = "09:00";
        model6.endDate = "11:00";

        model7.title = "الکومیدی شو " + count++;
        model7.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model7.startDate = "11:00";
        model7.endDate = "14:00";

        model8.title = "الکومیدی شو " + count++;
        model8.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model8.startDate = "14:00";
        model8.endDate = "16:00";

        model9.title = "الکومیدی شو " + count++;
        model9.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model9.startDate = "16:30";
        model9.endDate = "17:30";

        model10.title = "الکومیدی شو " + count++;
        model10.imageUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";
        model10.startDate = "18:00";
        model10.endDate = "18:30";

        programModels.add(model0);
        programModels.add(model1);
        programModels.add(model2);
        programModels.add(model3);
        programModels.add(model4);
        programModels.add(model5);
        programModels.add(model6);
        programModels.add(model7);
        programModels.add(model8);
        programModels.add(model9);
        programModels.add(model10);

        ScheduleProgramAdapter adapter = new ScheduleProgramAdapter(mActivity, programModels, new ScheduleProgramAdapter.OnItemClickListener() {
            @Override
            public void onClick(ScheduleProgramModel model) {
                if (mSecetion == Constant.Section.FM) {
                    mPageController.loadFragment(LiveRadioFragment.newInstance());

                } else if (mSecetion == Constant.Section.TV) {
                    if (model.isPlayingNow) {
                        mPageController.loadFragment(LiveTVFragment.newInstance());
                    } else {
                        mPageController.loadFragment(PlayShowFragment.newInstance());
                    }
                }

            }
        });

        mListProgramsFirst.setAdapter(adapter);
        mListProgramsSecond.setAdapter(adapter);
        mListProgramsThird.setAdapter(adapter);
        mListProgramsFourth.setAdapter(adapter);
        mListProgramsFifth.setAdapter(adapter);
        mListProgramsSixth.setAdapter(adapter);
        mListProgramsSeventh.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mIV_live.getId()) {
            mPageController.loadFragment(LiveTVFragment.newInstance());
        } else if (id == mPanelTopic.getId()) {
            handleFullScreen();
        } else if (id == mTV_play.getId()) {
            handlePlayButton();
        }
    }

    private void handlePlayButton() {
        if (mSecetion == Constant.Section.FM) {
            mPageController.loadFragment(LiveRadioFragment.newInstance());
        } else {
            mPageController.loadFragment(PlayShowFragment.newInstance());
        }
    }

    private void handleFullScreen() {
        ConstraintSet set = new ConstraintSet();
        set.clone(mPabelRoot);

        if (!mIsFullScreen) {
            set.connect(mPanelSchedule.getId(), ConstraintSet.TOP, mPanelToolbar.getId(), ConstraintSet.BOTTOM);
            set.connect(mPanelTop.getId(), ConstraintSet.BOTTOM, mPanelToolbar.getId(), ConstraintSet.BOTTOM);


        } else {
            set.connect(mPanelSchedule.getId(), ConstraintSet.TOP, mGuidline.getId(), ConstraintSet.BOTTOM);
            set.connect(mPanelTop.getId(), ConstraintSet.BOTTOM, mGuidline.getId(), ConstraintSet.TOP);
        }

        set.applyTo(mPabelRoot);
        mIsFullScreen = !mIsFullScreen;
    }

    @Override
    public void onScrollChanged(FrameLayout scrollView, int x, int y, int oldx, int oldy) {
        if (scrollView == mScrollViewVerticalDays) {
            mScrollViewVertiicalContent.scrollTo(x, y);
        }

        if (scrollView == mScrollViewVertiicalContent) {
            mScrollViewVerticalDays.scrollTo(x, y);
        }

        if (scrollView == mScrollViewHorizontalContent) {
            mScrollViewHorizontalTime.scrollTo(x, y);
        }

        if (scrollView == mScrollViewHorizontalTime) {
            mScrollViewHorizontalContent.scrollTo(x, y);
        }
    }

    @Override
    void initToolbar() {

        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                mIV_live.setVisibility(View.GONE);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);

                break;
        }
    }

    private void initDateColumns(View rootView) {

        Calendar calendar = Calendar.getInstance();
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_text_first)).setText(calendar.get(Calendar.DAY_OF_MONTH) + "");
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_text_second)).setText(calendar.get(Calendar.DAY_OF_MONTH) + "");
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_number_second)).setText(DateHelper.getDayOFWeekString(calendar.get(Calendar.DAY_OF_WEEK)));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_text_third)).setText(calendar.get(Calendar.DAY_OF_MONTH) + "");
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_number_third)).setText(DateHelper.getDayOFWeekString(calendar.get(Calendar.DAY_OF_WEEK)));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_text_fourth)).setText(calendar.get(Calendar.DAY_OF_MONTH) + "");
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_number_fourth)).setText(DateHelper.getDayOFWeekString(calendar.get(Calendar.DAY_OF_WEEK)));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_text_fifth)).setText(calendar.get(Calendar.DAY_OF_MONTH) + "");
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_number_fifth)).setText(DateHelper.getDayOFWeekString(calendar.get(Calendar.DAY_OF_WEEK)));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_text_sixth)).setText(calendar.get(Calendar.DAY_OF_MONTH) + "");
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_number_sixth)).setText(DateHelper.getDayOFWeekString(calendar.get(Calendar.DAY_OF_WEEK)));
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_text_seventh)).setText(calendar.get(Calendar.DAY_OF_MONTH) + "");
        ((TextView) rootView.findViewById(R.id.fragment_schedule_tv_day_number_seventh)).setText(DateHelper.getDayOFWeekString(calendar.get(Calendar.DAY_OF_WEEK)));

    }
}
