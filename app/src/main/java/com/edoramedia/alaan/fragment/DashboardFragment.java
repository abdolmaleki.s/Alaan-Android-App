package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.adapter.ArticleAdapter;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.setting.AppSetting;
import com.edoramedia.alaan.viewmodel.ArticleModel;
import com.edoramedia.alaan.viewmodel.HighlightArticleModel;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

/**
 * dashboard page.
 */
public class DashboardFragment extends BaseFragment implements View.OnClickListener {

    private int mSection;
    private RecyclerView mList_Articles;
    private ImageView mIMG_liveShow;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_humbergerIcon;
    private ArticleAdapter mAdapter;

    private SpinKitView mProgressBar;
    private int mSkipRows;
    private int mTakeRows = 5;
    private long mTotalRows = 0;
    private boolean mIsLoading = false;

    /**
     * New instance dashboard fragment.
     *
     * @return the dashboard fragment
     */
    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        loadData();
        initView(rootView);
        initAdapter();
        return rootView;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mIMG_liveShow.getId()) {
            if (mSection == Constant.Section.AISH) {
                mPageController.loadFragment(LiveVideoListingFragment.newInstance());
            } else {
                mPageController.loadFragment(LiveTVFragment.newInstance());
            }
        }
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                mIMG_liveShow.setVisibility(View.GONE);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mIMG_liveShow.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_aish_video_camera));
                mIMG_liveShow.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_left_icon_width);
                mIMG_liveShow.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_left_icon_width);
                break;
        }
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        bindView(rootView);
        initToolbar();
        initLazyLoading();
    }

    private void bindView(View rootView) {
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);
        mList_Articles = rootView.findViewById(R.id.fragment_article_rcv_articles);
        mIMG_liveShow = rootView.findViewById(R.id.fragment_article_img_toolbar_left_icon);
        mIMG_liveShow.setOnClickListener(this);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.fragment_article_img_logo);
        mProgressBar = rootView.findViewById(R.id.fragment_article_progress);
    }

    private void initAdapter() {
        List<ArticleModel> articleModels = new ArrayList<>();
        ArticleModel model1 = new ArticleModel();
        ArticleModel model2 = new ArticleModel();
        ArticleModel model3 = new ArticleModel();
        ArticleModel model4 = new ArticleModel();
        ArticleModel model5 = new ArticleModel();

        if (mSection == Constant.Section.AISH) {
            model1.type = ArticleModel.TYPE_AISH_IMAGE;
            model2.type = ArticleModel.TYPE_AISH_VIDEO;
            model5.type = ArticleModel.TYPE_AISH_IMAGE_CUSTOM;
            model4.type = ArticleModel.TYPE_AISH_GALLERY;
            model3.type = ArticleModel.TYPE_AISH_highlight;
        } else {
            model1.type = ArticleModel.TYPE_IMAGE;
            model2.type = ArticleModel.TYPE_VIDEO;
            model5.type = ArticleModel.TYPE_IMAGE_CUSTOM;
            model4.type = ArticleModel.TYPE_GALLERY;
            model3.type = ArticleModel.TYPE_highlight;
        }

        model1.title = "بصمتي : موسم جديد مع مشاهير العالم العربي الامازات متحدة!";
        model1.category = "ریاضیات";
        model1.backgroundImageUrl = Constant.FAKE_Article_IMAGE_URL;
        model1.publishDate = "منذ 10 ساعات";

        model2.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model2.category = "ریاضیات";
        model2.backgroundImageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model2.publishDate = "منذ 10 ساعات";

        model5.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model5.category = "ریاضیات";
        model5.backgroundImageUrl = "https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg";
        model5.publishDate = "منذ 10 ساعات";

        model4.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model4.category = "ریاضیات";
        model4.publishDate = "منذ 10 ساعات";
        model4.galleryImageUrls = new ArrayList<>();
        model4.galleryImageUrls.add(Constant.FAKE_Article_IMAGE_URL);
        model4.galleryImageUrls.add("https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg");
        model4.galleryImageUrls.add("https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg");


        ArticleModel hmodel1 = new ArticleModel();
        hmodel1.title = "رابط إخباري جديد للاختبار";
        ArticleModel hmodel2 = new ArticleModel();
        hmodel2.title = "رابط إخباري جديد للاختبار";
        ArticleModel hmodel3 = new ArticleModel();
        hmodel3.title = "رابط إخباري جديد للاختبار";
        ArticleModel hmodel4 = new ArticleModel();
        hmodel4.title = "رابط إخباري جديد للاختبار";
        ArticleModel hmodel5 = new ArticleModel();
        hmodel5.title = "رابط إخباري جديد للاختبار";
        model3.highlightArticleModels = new ArrayList<>();
        model3.highlightArticleModels.add(new HighlightArticleModel(hmodel1.title, "1"));
        model3.highlightArticleModels.add(new HighlightArticleModel(hmodel2.title, "1"));
        model3.highlightArticleModels.add(new HighlightArticleModel(hmodel3.title, "1"));
        model3.highlightArticleModels.add(new HighlightArticleModel(hmodel4.title, "1"));
        model3.highlightArticleModels.add(new HighlightArticleModel(hmodel5.title, "1"));


        articleModels.add(model1);
        articleModels.add(model2);
        articleModels.add(model3);
        articleModels.add(model4);
        articleModels.add(model5);

        mAdapter = new ArticleAdapter(articleModels, ArticleAdapter.STYLE_DASHBOARD, mActivity, new ArticleAdapter.RecyclerViewClickListener() {
            @Override
            public void recyclerViewListClicked(ArticleModel model) {
                mPageController.loadFragment(SpecificArticleFragment.newInstance(model.type));
            }
        });

        mList_Articles.setLayoutManager(new LinearLayoutManager(mActivity));
        mList_Articles.setAdapter(mAdapter);

    }

    private void fakeAddNewItems() {
        final List<ArticleModel> articleModels = new ArrayList<>();
        ArticleModel model1 = new ArticleModel();
        ArticleModel model2 = new ArticleModel();
        ArticleModel model4 = new ArticleModel();
        ArticleModel model5 = new ArticleModel();

        if (mSection == Constant.Section.AISH) {
            model1.type = ArticleModel.TYPE_AISH_IMAGE;
            model2.type = ArticleModel.TYPE_AISH_VIDEO;
            model5.type = ArticleModel.TYPE_AISH_IMAGE_CUSTOM;
            model4.type = ArticleModel.TYPE_AISH_GALLERY;
        } else {
            model1.type = ArticleModel.TYPE_IMAGE;
            model2.type = ArticleModel.TYPE_VIDEO;
            model5.type = ArticleModel.TYPE_IMAGE_CUSTOM;
            model4.type = ArticleModel.TYPE_GALLERY;
        }

        model1.title = "بصمتي : موسم جديد مع مشاهير العالم العربي الامازات متحدة!";
        model1.category = "مشاهیر";
        model1.backgroundImageUrl = Constant.FAKE_Article_IMAGE_URL;
        model1.publishDate = "منذ 10 ساعات";

        model2.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model2.category = "مشاهیر";
        model2.backgroundImageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model2.publishDate = "منذ 10 ساعات";

        model5.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model5.category = "مشاهیر";
        model5.backgroundImageUrl = "https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg";
        model5.publishDate = "منذ 10 ساعات";

        model4.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model4.category = "مشاهیر";
        model4.publishDate = "منذ 10 ساعات";
        model4.galleryImageUrls = new ArrayList<>();
        model4.galleryImageUrls.add(Constant.FAKE_Article_IMAGE_URL);
        model4.galleryImageUrls.add("https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg");
        model4.galleryImageUrls.add("https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg");

        articleModels.add(model1);
        articleModels.add(model2);
        articleModels.add(model4);
        articleModels.add(model5);

        mProgressBar.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mAdapter.addArticles(articleModels);
                mProgressBar.setVisibility(View.GONE);
                mIsLoading = false;
            }
        }, 4000);

    }

    private void initLazyLoading() {
        mList_Articles.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (recyclerView.getAdapter() != null) {
                    if (!mIsLoading) {
                        int existItemsCount = recyclerView.getAdapter().getItemCount();
                        if (existItemsCount > 0) {
                            LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
                            int currentPosition = llm.findLastVisibleItemPosition();
                            if (currentPosition >= existItemsCount - 1 && currentPosition != mTotalRows - 1) {
                                mIsLoading = true;
                                fakeAddNewItems();
                            }
                        }
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void loadData() {
        mSection = AppSetting.getInstance().getAppSection();
    }


}
