package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;

/**
 * dashboard page.
 */

public class AccountDashboardFragment extends BaseFragment implements View.OnClickListener {

    private ImageView mIMG_avatar;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_humbergerIcon;
    private TextView mTV_editText;
    private TextView mTV_editIcon;


    /**
     * New instance account dashboard fragment.
     *
     * @return the account dashboard fragment
     */
    public static AccountDashboardFragment newInstance() {
        AccountDashboardFragment fragment = new AccountDashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_account_dashboard, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        mIMG_avatar = rootView.findViewById(R.id.fragment_accoutn_dashboard_img_avatar);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.toolbar_section_icon);
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);
        mTV_editText = rootView.findViewById(R.id.fragment_account_dashboard_label_edit);
        mTV_editIcon = rootView.findViewById(R.id.fragment_account_dashboard_ic_edit);
        Glide.with(this).load(Constant.FAKE_AUTHOR_IMAGE_URL).apply(new RequestOptions().circleCrop().placeholder(R.drawable.ic_user_empty)).into(mIMG_avatar);
        rootView.findViewById(R.id.fragment_account_dashboard_ic_edit).setOnClickListener(this);
        rootView.findViewById(R.id.fragment_account_dashboard_label_edit).setOnClickListener(this);
        initToolbar();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == R.id.fragment_account_dashboard_label_edit || id == R.id.fragment_account_dashboard_ic_edit) {
            mPageController.loadFragment(AccountFragment.newInstance());
        }
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mTV_editIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mTV_editText.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                break;
        }
    }
}
