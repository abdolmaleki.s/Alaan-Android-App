package com.edoramedia.alaan.fragment;

import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dailymotion.android.player.sdk.PlayerWebView;
import com.dailymotion.android.player.sdk.events.PlayerEvent;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.adapter.SpecificShowEpisodeAdapter;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.viewmodel.ShowEpisodeModel;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * player for stream specific show
 */
public class PlayShowFragment extends BaseFragment implements View.OnClickListener, TabLayout.BaseOnTabSelectedListener {

    private FrameLayout mPanelPlayer;
    private RecyclerView mListEpisods;
    private TabLayout mTabLayout_tabs;
    private ConstraintLayout mPanelDescription;
    private LinearLayout mPanelEpisods;
    private TextView mTV_refreshIcon;
    private TextView mIMG_fullScreen;
    private Dialog mFullScreenDialog;
    private boolean mIsPlayerFullscreen = false;
    private long playBackPostion = 0;
    private int mVideoSourceType = Constant.VideoTypes.AMAZON;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;


    //////////// Amazon ////////////////////////////
    private SimpleExoPlayer mExoPlayer;
    private PlayerView mExoPlayerView;
    /**
     * The M stream url.
     */
    public String mStreamUrl = "http://www.streambox.fr/playlists/test_001/stream.m3u8";
    private LinearLayout mPanel_refresh;
    private TextView mTV_changeQuality;

    //////////// Dailymotion ////////////////////////////
    private PlayerWebView mDailymotionPlayer;

    //////////// Youtube ////////////////////////////
    private YouTubeFragment mYouTubeFragment;

    /**
     * New instance play show fragment.
     *
     * @return the play show fragment
     */
    public static PlayShowFragment newInstance() {
        PlayShowFragment fragment = new PlayShowFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_play_show, container, false);
        initView(rootView);
        initPlayer();
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePlayer();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        resumePlayer();
    }

    @Override
    public void onDestroy() {
        releasePlayer();
        super.onDestroy();
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);

                break;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == R.id.toolbar_ic_back || id == R.id.toolbar_label_back) {
            mPageController.goBack();
        } else if (id == mIMG_fullScreen.getId()) {

            if (!mIsPlayerFullscreen)
                openFullscreenDialog();
            else
                closeFullscreenDialog();
        } else if (id == mTV_refreshIcon.getId()) {
            tryToPlay();
        } else if (id == mTV_changeQuality.getId()) {
            showQualityList();
        }

    }

    private void showQualityList() {
        final DialogFragmentQuality dialogFragmentQuality = new DialogFragmentQuality();
        dialogFragmentQuality.displayChangeQuality((AppCompatActivity) mActivity, new DialogFragmentQuality.OnQualityChange() {
            @Override
            public void onSelectedQuality(int quality) {
                mTV_changeQuality.setText(quality + "p");
                dialogFragmentQuality.dismiss();
            }
        });
    }

    private void initView(View rootView) {
        bindView(rootView);
        initTabLayout();
        initAdapter();

    }

    private void initAdapter() {
        int number = 1;
        List<ShowEpisodeModel> models = new ArrayList<>();
        ShowEpisodeModel model = new ShowEpisodeModel();
        ShowEpisodeModel model2 = new ShowEpisodeModel();
        ShowEpisodeModel model3 = new ShowEpisodeModel();
        ShowEpisodeModel model4 = new ShowEpisodeModel();
        ShowEpisodeModel model5 = new ShowEpisodeModel();

        model.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model.title = "سوف یذهب هنا و علی خطین";
        model.number = number++;

        model2.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model2.title = "سوف یذهب هنا و علی خطین";
        model2.number = number++;

        model3.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model3.title = "سوف یذهب هنا و علی خطین";
        model3.number = number++;

        model4.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model4.title = "سوف یذهب هنا و علی خطین";
        model4.number = number++;

        model5.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model5.title = "سوف یذهب هنا و علی خطین";
        model5.number = number++;

        models.add(model);
        models.add(model2);
        models.add(model3);
        models.add(model4);
        models.add(model5);

        SpecificShowEpisodeAdapter adapter = new SpecificShowEpisodeAdapter(models, mActivity, new SpecificShowEpisodeAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position, ShowEpisodeModel model) {

            }
        });

        mListEpisods.setLayoutManager(new LinearLayoutManager(mActivity));
        mListEpisods.setAdapter(adapter);
    }

    private void bindView(View rootView) {
        mPageController.needBottomNavigation(true);
        mPanelDescription = rootView.findViewById(R.id.fragment_play_show_panel_description);
        mPanelEpisods = rootView.findViewById(R.id.fragment_play_show_panel_episods);
        mPanelPlayer = rootView.findViewById(R.id.fragment_play_show_panel_player);
        mTabLayout_tabs = rootView.findViewById(R.id.fragment_play_show_tabs);
        mListEpisods = rootView.findViewById(R.id.fragment_play_show_list_episods);
        mExoPlayerView = rootView.findViewById(R.id.fragment_play_show_player_amazon);
        mPanel_refresh = rootView.findViewById(R.id.fragment_play_show_panel_refresh);
        mTV_refreshIcon = rootView.findViewById(R.id.fragment_play_show_ic_refresh);
        mDailymotionPlayer = rootView.findViewById(R.id.fragment_play_show_player_dailymotion);
        mIMG_fullScreen = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mTV_changeQuality = mExoPlayerView.findViewById(R.id.frame_exo_control_view_archive_btn_change_quality);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.fragment_author_article_img_logo);

        mIMG_fullScreen.setOnClickListener(this);
        mTV_refreshIcon.setOnClickListener(this);
        mTV_changeQuality.setOnClickListener(this);

        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_ic_back).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_label_back).setOnClickListener(this);

        initToolbar();


    }

    private void initPlayer() {
        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                initExoPlayer();
                break;
            case Constant.VideoTypes.YOUTUBE:
                initYoutubePlayer();
                break;
            case Constant.VideoTypes.DAILYMOTION:
                initDailymotionPlayer();
                break;
        }
    }

    private void initExoPlayer() {
        mExoPlayerView.setVisibility(View.VISIBLE);
        mExoPlayer = ExoPlayerFactory.newSimpleInstance(mActivity);
        mExoPlayerView.setPlayer(mExoPlayer);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(mActivity,
                Util.getUserAgent(mActivity, "app"));

        HlsMediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(mStreamUrl));
        mExoPlayer.prepare(videoSource);
        mExoPlayer.setPlayWhenReady(true);

        mExoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                        break;
                    case Player.STATE_ENDED:

                        break;
                    case Player.STATE_IDLE:

                        break;
                    case Player.STATE_READY:
                        hideRefreshPanel();

                        break;
                    default:
                        break;
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                playBackPostion = mExoPlayer.getCurrentPosition();
                showRefreshPanel();

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

    private void initDailymotionPlayer() {

        mDailymotionPlayer.setVisibility(View.VISIBLE);
        mDailymotionPlayer.load("x70clkm");
        mDailymotionPlayer.setPlayerEventListener(new PlayerWebView.PlayerEventListener() {
            @Override
            public void onEvent(@NonNull PlayerEvent event) {
                Log.e(Constant.DEBUG_LOG_KEY, event.getName());
                if (event.getName().equals(PlayerWebView.EVENT_FULLSCREEN_TOGGLE_REQUESTED)) {

                    if (!mIsPlayerFullscreen) {
                        openFullscreenDialog();
                    } else {
                        closeFullscreenDialog();
                    }
                }
            }
        });
    }

    private void initYoutubePlayer() {
        mYouTubeFragment = YouTubeFragment.newInstance("E5T7Txo9sWk");
        ((AppCompatActivity) mActivity).getSupportFragmentManager().beginTransaction().replace(mPanelPlayer.getId(), mYouTubeFragment).commit();
    }

    private void hideRefreshPanel() {
        mPanel_refresh.setVisibility(View.GONE);

    }

    private void showRefreshPanel() {
        mPanel_refresh.setVisibility(View.VISIBLE);

    }

    private void tryToPlay() {

        Animation animRotateAclk = AnimationUtils.loadAnimation(mActivity.getApplicationContext(), R.anim.anim_rotate_clock);
        animRotateAclk.setRepeatCount(Animation.INFINITE);
        mTV_refreshIcon.startAnimation(animRotateAclk);

        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                initExoPlayer();
                break;
            case Constant.VideoTypes.DAILYMOTION:
                initDailymotionPlayer();
                break;
        }

    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mActivity, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mIsPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        ((AppCompatActivity) mActivity).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        initFullscreenDialog();


        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                ((ViewGroup) mExoPlayerView.getParent()).removeView(mExoPlayerView);
                ((ViewGroup) mPanel_refresh.getParent()).removeView(mPanel_refresh);
                mFullScreenDialog.addContentView(mExoPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                mFullScreenDialog.addContentView(mPanel_refresh, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                mIMG_fullScreen.setText(R.string.faw_minimize);
                break;

            case Constant.VideoTypes.DAILYMOTION:
                ((ViewGroup) mDailymotionPlayer.getParent()).removeView(mDailymotionPlayer);
                mFullScreenDialog.addContentView(mDailymotionPlayer, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                mDailymotionPlayer.setFullscreenButton(true);
                break;
        }


        mIsPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    private void closeFullscreenDialog() {
        ((AppCompatActivity) mActivity).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                ((ViewGroup) mExoPlayerView.getParent()).removeView(mExoPlayerView);
                ((ViewGroup) mPanel_refresh.getParent()).removeView(mPanel_refresh);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                mPanelPlayer.addView(mExoPlayerView, layoutParams);
                mPanelPlayer.addView(mPanel_refresh, layoutParams);
                mIMG_fullScreen.setText(R.string.faw_maximize);
                break;
            case Constant.VideoTypes.DAILYMOTION:
                ((ViewGroup) mDailymotionPlayer.getParent()).removeView(mDailymotionPlayer);
                FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                mPanelPlayer.addView(mDailymotionPlayer, layoutParams2);
                mDailymotionPlayer.setFullscreenButton(false);
                break;
        }


        mIsPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
    }

    private void releasePlayer() {
        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                mExoPlayer.release();
                mExoPlayer = null;
                break;
            case Constant.VideoTypes.YOUTUBE:
                mYouTubeFragment.onDestroy();
                break;
            case Constant.VideoTypes.DAILYMOTION:
                mDailymotionPlayer.release();
                mDailymotionPlayer = null;
                break;
        }

    }

    private void resumePlayer() {
        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                mExoPlayer.setPlayWhenReady(true);
                break;
            case Constant.VideoTypes.DAILYMOTION:
                mDailymotionPlayer.onResume();
                break;
        }
    }

    private void pausePlayer() {
        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                mExoPlayer.setPlayWhenReady(false);
                break;
            case Constant.VideoTypes.DAILYMOTION:
                mDailymotionPlayer.onPause();
                break;
        }

    }

    private void initTabLayout() {
        mTabLayout_tabs.addTab(mTabLayout_tabs.newTab().setText("تفاصیل"));
        mTabLayout_tabs.addTab(mTabLayout_tabs.newTab().setText("الحلقات"));

        mTabLayout_tabs.addOnTabSelectedListener(this);

        for (int i = 0; i < mTabLayout_tabs.getTabCount(); i++) {
            TextView tv = (TextView) LayoutInflater.from(mActivity).inflate(R.layout.item_tab_specific_show, null);
            if (mTabLayout_tabs.getTabAt(i).isSelected()) {
                tv.setTextColor(ContextCompat.getColor(mActivity, R.color.alan_tv_primary));

            }
            mTabLayout_tabs.getTabAt(i).setCustomView(tv);
        }

        onTabSelected(mTabLayout_tabs.getTabAt(0));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        ((TextView) tab.getCustomView()).setTextColor(ContextCompat.getColor(mActivity, R.color.alan_tv_primary));
        int position = tab.getPosition();
        switch (position) {
            case 0:
                mPanelEpisods.setVisibility(View.GONE);
                mPanelDescription.setVisibility(View.VISIBLE);
                break;
            case 1:
                mPanelDescription.setVisibility(View.GONE);
                mPanelEpisods.setVisibility(View.VISIBLE);
                initAdapter();
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        ((TextView) tab.getCustomView()).setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

}
