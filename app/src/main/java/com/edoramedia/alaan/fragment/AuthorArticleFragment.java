package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.adapter.ArticleAdapter;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.infrastructure.Helper;
import com.edoramedia.alaan.setting.AppSetting;
import com.edoramedia.alaan.viewmodel.ArticleModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Article listing of specific author page
 */
public class AuthorArticleFragment extends BaseFragment implements View.OnClickListener {

    private int mSection;
    private RecyclerView mList_Articles;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_humbergerIcon;
    private TextView mTV_backIcon;
    private TextView mTV_backLabel;
    private TextView mTV_autherTwitter;


    /**
     * New instance author article fragment.
     *
     * @return the author article fragment
     */
    public static AuthorArticleFragment newInstance() {
        AuthorArticleFragment fragment = new AuthorArticleFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_author_article, container, false);
        loadData();
        initView(rootView);
        initAdapter();
        return rootView;
    }

    private void loadData() {
        if (getArguments() != null) {
            mSection = AppSetting.getInstance().getAppSection();
        }
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);
        mTV_backIcon = rootView.findViewById(R.id.toolbar_ic_back);
        mTV_backLabel = rootView.findViewById(R.id.toolbar_label_back);
        mTV_autherTwitter = rootView.findViewById(R.id.fragment_author_article_tv_author_twitter);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_ic_back).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_label_back).setOnClickListener(this);
        mTV_autherTwitter.setOnClickListener(this);
        mList_Articles = rootView.findViewById(R.id.fragment_author_article_list_articles);
        mList_Articles.setNestedScrollingEnabled(false);
        ImageView authorImage = rootView.findViewById(R.id.fragment_author_article_img_author);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.fragment_author_article_img_logo);
        initToolbar();
        Glide.with(this).load(Constant.FAKE_AUTHOR_IMAGE_URL).apply(new RequestOptions().circleCrop().placeholder(R.drawable.ic_user_empty)).into(authorImage);
    }

    private void initAdapter() {

        List<ArticleModel> articleModels = new ArrayList<>();
        ArticleModel model1 = new ArticleModel();
        ArticleModel model2 = new ArticleModel();
        ArticleModel model3 = new ArticleModel();
        ArticleModel model4 = new ArticleModel();

        if (mSection == Constant.Section.AISH) {
            model1.type = ArticleModel.TYPE_AISH_IMAGE;
            model2.type = ArticleModel.TYPE_AISH_VIDEO;
            model4.type = ArticleModel.TYPE_AISH_GALLERY;
            model3.type = ArticleModel.TYPE_AISH_IMAGE_CUSTOM;
        } else {
            model1.type = ArticleModel.TYPE_IMAGE;
            model2.type = ArticleModel.TYPE_VIDEO;
            model4.type = ArticleModel.TYPE_GALLERY;
            model3.type = ArticleModel.TYPE_IMAGE_CUSTOM;
        }

        model1.title = "بصمتي : موسم جديد مع مشاهير العالم العربي الامازات متحدة!";
        model1.category = "مشاهیر";
        model1.backgroundImageUrl = Constant.FAKE_Article_IMAGE_URL;
        model1.publishDate = "منذ 10 ساعات";

        model2.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model2.category = "مشاهیر";
        model2.backgroundImageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model2.publishDate = "منذ 10 ساعات";

        model3.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model3.category = "مشاهیر";
        model3.backgroundImageUrl = "https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg";
        model3.publishDate = "منذ 10 ساعات";

        model4.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model4.category = "مشاهیر";
        model4.publishDate = "منذ 10 ساعات";
        model4.galleryImageUrls = new ArrayList<>();
        model4.galleryImageUrls.add(Constant.FAKE_Article_IMAGE_URL);
        model4.galleryImageUrls.add("https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg");
        model4.galleryImageUrls.add("https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg");


        articleModels.add(model1);
        articleModels.add(model2);
        articleModels.add(model3);
        articleModels.add(model4);

        ArticleAdapter adapter = new ArticleAdapter(articleModels, ArticleAdapter.STYLE_DASHBOARD, mActivity, new ArticleAdapter.RecyclerViewClickListener() {
            @Override
            public void recyclerViewListClicked(ArticleModel model) {
                mPageController.loadFragment(SpecificArticleFragment.newInstance(model.type));
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mList_Articles.setLayoutManager(linearLayoutManager);
        mList_Articles.setAdapter(adapter);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == R.id.toolbar_ic_back || id == R.id.toolbar_label_back) {
            mPageController.goBack();
        } else if (id == mTV_autherTwitter.getId()) {
            String twitter = mTV_autherTwitter.getText().subSequence(0, mTV_autherTwitter.getText().length() - 1).toString();
            Helper.openTwitter(mActivity, twitter);
        }
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mTV_backIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mTV_backLabel.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                break;
        }
    }


}
