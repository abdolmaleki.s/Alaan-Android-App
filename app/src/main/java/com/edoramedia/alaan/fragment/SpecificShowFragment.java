package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.adapter.SpecificShowEpisodeAdapter;
import com.edoramedia.alaan.adapter.SpecificShowNewsAdapter;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.infrastructure.Helper;
import com.edoramedia.alaan.setting.AppSetting;
import com.edoramedia.alaan.viewmodel.ArticleModel;
import com.edoramedia.alaan.viewmodel.ShowEpisodeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * specific show details page such as episods , description and etc.
 */
public class SpecificShowFragment extends BaseFragment implements View.OnClickListener, TabLayout.BaseOnTabSelectedListener {

    private final String backimageUrl = "https://i.kinja-img.com/gawker-media/image/upload/s--GpcFH1UL--/c_scale,f_auto,fl_progressive,q_80,w_800/d6h1yrp2v7fvdsm5osem.jpg";
    private final String friendsurl = "https://s26162.pcdn.co/wp-content/uploads/2018/10/588730354.jpg";
    private TextView mBTN_play;
    private ImageView mIMG_background;
    private ImageView mIMG_logo;
    private ImageView mIMG_about;
    private ImageView mIMG_live;
    private TabLayout mTabLayout_tabs;
    private RecyclerView mListEpisods;
    private RecyclerView mListNews;
    private LinearLayout mPanelEpisods;
    private ConstraintLayout mPanelAbout;
    private Spinner mSP_sessions;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private int mSection;
    private TextView mTwitterLink;

    /**
     * New instance specific show fragment.
     *
     * @return the specific show fragment
     */
    public static SpecificShowFragment newInstance() {
        SpecificShowFragment fragment = new SpecificShowFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_specific_show, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mBTN_play.getId()) {
            if (mSection == Constant.Section.TV) {
                mPageController.loadFragment(PlayShowFragment.newInstance());
            } else {
                mPageController.loadFragment(LiveRadioFragment.newInstance());
            }
        } else if (id == mIMG_live.getId()) {
            mPageController.loadFragment(LiveTVFragment.newInstance());
        } else if (id == mTwitterLink.getId()) {
            Helper.openTwitter(mActivity, mTwitterLink.getText().subSequence(0, mTwitterLink.length() - 1).toString());
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        ((TextView) tab.getCustomView()).setTextColor(ContextCompat.getColor(mActivity, R.color.alan_tv_primary));
        int position = tab.getPosition();
        switch (position) {
            case 0:
                mPanelAbout.setVisibility(View.GONE);
                mPanelEpisods.setVisibility(View.GONE);
                mListNews.setVisibility(View.VISIBLE);
                initNewsAdapter();
                break;
            case 1:
                mPanelAbout.setVisibility(View.VISIBLE);
                mPanelEpisods.setVisibility(View.GONE);
                mListNews.setVisibility(View.GONE);
                break;
            case 2:
                mPanelAbout.setVisibility(View.GONE);
                mPanelEpisods.setVisibility(View.VISIBLE);
                mListNews.setVisibility(View.GONE);
                initEpisodsAdapter();
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        ((TextView) tab.getCustomView()).setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void initView(View rootView) {
        bindView(rootView);
        initTabLayout();
        Glide.with(this).load(backimageUrl).into(mIMG_background);
        Glide.with(this).load("http://www-hep.phys.cmu.edu/logos/NASA_logo.png").apply(new RequestOptions().circleCrop()).into(mIMG_logo);
        Glide.with(this).load(friendsurl).apply(new RequestOptions().circleCrop()).into(mIMG_about);
        onTabSelected(mTabLayout_tabs.getTabAt(0));
        initSessionsSpinner();
        initToolbar();

    }

    private void bindView(View rootView) {
        mSection = AppSetting.getInstance().getAppSection();
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        mBTN_play = rootView.findViewById(R.id.fragment_specific_show_btn_play);
        mTwitterLink = rootView.findViewById(R.id.fragment_specific_show_tv_twitter);
        mIMG_background = rootView.findViewById(R.id.fragment_specific_show_img_background);
        mIMG_logo = rootView.findViewById(R.id.fragment_specific_show_img_logo);
        mIMG_live = rootView.findViewById(R.id.toolbar_iv_live);
        mIMG_about = rootView.findViewById(R.id.fragment_specific_show_about_image);
        mTabLayout_tabs = rootView.findViewById(R.id.fragment_specific_show_tabs);
        mListEpisods = rootView.findViewById(R.id.fragment_specific_show_list_episode);
        mListNews = rootView.findViewById(R.id.fragment_specific_show_list_news);
        mPanelEpisods = rootView.findViewById(R.id.fragment_specific_show_panel_episodes);
        mPanelAbout = rootView.findViewById(R.id.fragment_specific_show_panel_about);
        mSP_sessions = rootView.findViewById(R.id.fragment_specific_show_sp_sessions);
        mBTN_play.setOnClickListener(this);
        mIMG_live.setOnClickListener(this);
        mTwitterLink.setOnClickListener(this);

        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);

    }

    private void initTabLayout() {
        mTabLayout_tabs.addTab(mTabLayout_tabs.newTab().setText("أخبار"));
        mTabLayout_tabs.addTab(mTabLayout_tabs.newTab().setText("حول"));
        if (mSection == Constant.Section.TV) {
            mTabLayout_tabs.addTab(mTabLayout_tabs.newTab().setText("الحلقات"));
        }

        mTabLayout_tabs.addOnTabSelectedListener(this);

        for (int i = 0; i < mTabLayout_tabs.getTabCount(); i++) {
            TextView tv = (TextView) LayoutInflater.from(mActivity).inflate(R.layout.item_tab_specific_show, null);
            if (mTabLayout_tabs.getTabAt(i).isSelected()) {
                tv.setTextColor(ContextCompat.getColor(mActivity, R.color.alan_tv_primary));

            }
            mTabLayout_tabs.getTabAt(i).setCustomView(tv);
        }
    }

    private void initNewsAdapter() {
        List<ArticleModel> articleModels = new ArrayList<>();
        ArticleModel model1 = new ArticleModel();
        ArticleModel model2 = new ArticleModel();
        ArticleModel model4 = new ArticleModel();
        ArticleModel model3 = new ArticleModel();

        model1.type = ArticleModel.TYPE_IMAGE;
        model1.title = "بصمتي : موسم جديد مع مشاهير العالم العربي الامازات متحدة!";
        model1.category = "مشاهیر";
        model1.backgroundImageUrl = Constant.FAKE_Article_IMAGE_URL;
        model1.publishDate = "منذ 10 ساعات";

        model2.type = ArticleModel.TYPE_IMAGE;
        model2.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model2.category = "مشاهیر";
        model2.backgroundImageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model2.publishDate = "منذ 10 ساعات";

        model3.type = ArticleModel.TYPE_IMAGE;
        model3.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model3.category = "مشاهیر";
        model3.backgroundImageUrl = "https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg";
        model3.publishDate = "منذ 10 ساعات";

        model4.type = ArticleModel.TYPE_IMAGE;
        model4.title = "بصمتي : موسم جديد مع مشاهير العالم العربي !";
        model4.category = "مشاهیر";
        model4.backgroundImageUrl = "https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg";
        model4.publishDate = "منذ 10 ساعات";

        articleModels.add(model1);
        articleModels.add(model2);
        articleModels.add(model4);
        articleModels.add(model3);

        SpecificShowNewsAdapter adapter = new SpecificShowNewsAdapter(articleModels, mActivity, new SpecificShowNewsAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position, ArticleModel model) {
                mPageController.loadFragment(SpecificArticleFragment.newInstance(model.type));
            }
        });

        mListNews.setLayoutManager(new LinearLayoutManager(mActivity));
        mListNews.setAdapter(adapter);
    }

    private void initEpisodsAdapter() {
        int number = 1;
        List<ShowEpisodeModel> models = new ArrayList<>();
        ShowEpisodeModel model = new ShowEpisodeModel();
        ShowEpisodeModel model2 = new ShowEpisodeModel();
        ShowEpisodeModel model3 = new ShowEpisodeModel();
        ShowEpisodeModel model4 = new ShowEpisodeModel();
        ShowEpisodeModel model5 = new ShowEpisodeModel();

        model.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model.title = "سوف یذهب هنا و علی خطین";
        model.number = number++;

        model2.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model2.title = "سوف یذهب هنا و علی خطین";
        model2.number = number++;

        model3.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model3.title = "سوف یذهب هنا و علی خطین";
        model3.number = number++;

        model4.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model4.title = "سوف یذهب هنا و علی خطین";
        model4.number = number++;

        model5.imageUrl = "https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg";
        model5.title = "سوف یذهب هنا و علی خطین";
        model5.number = number++;

        models.add(model);
        models.add(model2);
        models.add(model3);
        models.add(model4);
        models.add(model5);

        SpecificShowEpisodeAdapter adapter = new SpecificShowEpisodeAdapter(models, mActivity, new SpecificShowEpisodeAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position, ShowEpisodeModel model) {
                mPageController.loadFragment(PlayShowFragment.newInstance());
            }
        });

        mListEpisods.setLayoutManager(new LinearLayoutManager(mActivity));
        mListEpisods.setAdapter(adapter);

    }

    private void initSessionsSpinner() {
        List<String> sessions = new ArrayList<>();
        sessions.add("فصل اول");
        sessions.add("فصل دوم");
        sessions.add("فصل سوم");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                R.layout.item_spinner, sessions);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        mSP_sessions.setAdapter(adapter);
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                mIMG_live.setVisibility(View.GONE);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                break;
        }
    }
}
