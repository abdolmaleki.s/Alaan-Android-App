package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.adapter.LiveVideoAdapter;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.infrastructure.Convertor;
import com.edoramedia.alaan.infrastructure.SpacesItemDecoration;
import com.edoramedia.alaan.infrastructure.SpacesItemDecorationLiveVideo;
import com.edoramedia.alaan.viewmodel.LiveVideoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * List of live streams in aish section.
 */
public class LiveVideoListingFragment extends BaseFragment implements View.OnClickListener {

    private RecyclerView mLiveList;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;

    /**
     * New instance live video listing fragment.
     *
     * @return the live video listing fragment
     */
    public static LiveVideoListingFragment newInstance() {
        LiveVideoListingFragment fragment = new LiveVideoListingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_live_listing, container, false);
        initView(rootView);
        initAdapter();
        return rootView;
    }

    private void initAdapter() {
        List<LiveVideoModel> modelList = new ArrayList<>();
        LiveVideoModel model1 = new LiveVideoModel();
        LiveVideoModel model2 = new LiveVideoModel();
        LiveVideoModel model3 = new LiveVideoModel();
        LiveVideoModel model4 = new LiveVideoModel();
        LiveVideoModel model5 = new LiveVideoModel();
        LiveVideoModel model6 = new LiveVideoModel();
        LiveVideoModel model7 = new LiveVideoModel();
        LiveVideoModel model8 = new LiveVideoModel();

        model1.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model1.description = "هذة توضیحات لالفیدئو";

        model8.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model8.description = "هذة توضیحات لالفیدئو";

        model2.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model2.description = "هذة توضیحات لالفیدئو";

        model3.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model3.description = "هذة توضیحات لالفیدئو";

        model4.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model4.description = "هذة توضیحات لالفیدئو";

        model5.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model5.description = "هذة توضیحات لالفیدئو";

        model6.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model6.description = "هذة توضیحات لالفیدئو";

        model7.imageUrl = Constant.FAKE_Article_IMAGE_URL;
        model7.description = "هذة توضیحات لالفیدئو";

        modelList.add(model1);
        modelList.add(model2);
        modelList.add(model3);
        modelList.add(model4);
        modelList.add(model5);
        modelList.add(model6);
        modelList.add(model7);
        modelList.add(model8);

        LiveVideoAdapter liveVideoAdapter = new LiveVideoAdapter(modelList, mActivity, new LiveVideoAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position, LiveVideoModel model) {
                mPageController.loadFragment(PlayLiveFragment.newInstance());
            }
        });

        mLiveList.setLayoutManager(new GridLayoutManager(mActivity, 2));
        mLiveList.setAdapter(liveVideoAdapter);
        mLiveList.addItemDecoration(new SpacesItemDecorationLiveVideo((int) Convertor.convertDpToPixel(2f, mActivity)));

    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        mLiveList = rootView.findViewById(R.id.fragment_live_listing_video_list);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);
        initToolbar();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        }
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
    }
}
