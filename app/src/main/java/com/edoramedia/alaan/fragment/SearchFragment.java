package com.edoramedia.alaan.fragment;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.infrastructure.DateHelper;
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog;


/**
 * Search Page
 */
public class SearchFragment extends BaseFragment implements View.OnClickListener, DatePickerFragmentDialog.OnDateSetListener, CompoundButton.OnCheckedChangeListener {

    private TextView mTV_settingIcon;
    private boolean mIsSettingVisible = false;
    private FrameLayout mPanel_Body;
    private TextView mTV_from;
    private TextView mTV_to;
    private int mDateCustomer;
    private AppCompatRadioButton mRB_Asending;
    private AppCompatRadioButton mRB_Desending;
    private Button mBTN_submit;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_humbergerIcon;

    /**
     * New instance search fragment.
     *
     * @return the search fragment
     */
    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        bindView(rootView);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        initToolbar();
        setSectionTheme();

    }

    private void setSectionTheme() {
        ColorStateList colorStateList;

        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                colorStateList = new ColorStateList(
                        new int[][]{
                                new int[]{R.color.gray_text_light}, //disabled
                                new int[]{ContextCompat.getColor(mActivity, R.color.alaan_akhbar_primary)} //enabled
                        },
                        new int[]{
                                R.color.gray_text_light, //disabled
                                ContextCompat.getColor(mActivity, R.color.alaan_akhbar_primary) //enabled
                        }
                );
                mRB_Asending.setButtonTintList(colorStateList);
                mRB_Desending.setButtonTintList(colorStateList);
                mBTN_submit.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.alaan_akhbar_primary));
                break;

            case Constant.Section.FM:
                break;

            case Constant.Section.AISH:
                break;
        }
    }

    private void bindView(View rootView) {
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);
        mTV_settingIcon = rootView.findViewById(R.id.fragment_search_ic_setting);
        mPanel_Body = rootView.findViewById(R.id.fragment_search_panel_body);
        mTV_from = rootView.findViewById(R.id.fragment_search_et_from);
        mTV_to = rootView.findViewById(R.id.fragment_search_et_to);
        mRB_Asending = rootView.findViewById(R.id.fragment_search_rb_asending);
        mRB_Desending = rootView.findViewById(R.id.fragment_search_rb_desending);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);
        mBTN_submit = rootView.findViewById(R.id.fragment_search_btn_submit);

        mTV_settingIcon.setOnClickListener(this);
        mTV_to.setOnClickListener(this);
        mTV_from.setOnClickListener(this);
        mBTN_submit.setOnClickListener(this);
        mRB_Asending.setOnCheckedChangeListener(this);
        mRB_Desending.setOnCheckedChangeListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mTV_settingIcon.getId()) {
            handleSettingPanel();
        } else if (id == mTV_from.getId()) {
            mDateCustomer = DateHelper.showDatePicker(mActivity, this, mTV_from);
        } else if (id == mTV_to.getId()) {
            mDateCustomer = DateHelper.showDatePicker(mActivity, this, mTV_to);
        } else if (id == mBTN_submit.getId()) {
            mIsSettingVisible = false;
            mPageController.needBottomNavigation(true);
            mTV_settingIcon.setText(getString(R.string.faw_adjust));
            mPanel_Body.setVisibility(View.GONE);
        }

    }

    private void handleSettingPanel() {
        if (mIsSettingVisible) {
            mIsSettingVisible = false;
            mPageController.needBottomNavigation(true);
            mTV_settingIcon.setText(getString(R.string.faw_adjust));
            mPanel_Body.setVisibility(View.GONE);

        } else {

            mIsSettingVisible = true;
            mPageController.needBottomNavigation(false);
            mTV_settingIcon.setText(getString(R.string.faw_exit));
            mPanel_Body.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDateSet(DatePickerFragmentDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (mDateCustomer == mTV_from.getId()) {
            mTV_from.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);

        } else {
            mTV_to.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (isChecked) {
            if (compoundButton == mRB_Asending) {

                mRB_Desending.setChecked(false);
                mRB_Asending.setChecked(true);

            } else {

                mRB_Desending.setChecked(true);
                mRB_Asending.setChecked(false);
            }
        }
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mTV_settingIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                break;
        }
    }
}
