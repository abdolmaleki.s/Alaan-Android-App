package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;

/**
 * Account Page.
 */
public class AccountFragment extends BaseFragment implements View.OnClickListener {

    private TextView mTV_setting;
    private TextView mTV_resetPass;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_humbergerIcon;
    private TextView mBackIcon;
    private TextView mBackLabel;


    /**
     * New instance account fragment.
     *
     * @return the account fragment
     */
    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_account, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        mTV_setting = rootView.findViewById(R.id.fragment_account_tv_setting);
        mTV_resetPass = rootView.findViewById(R.id.fragment_account_tv_reset_pass);
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);
        mBackLabel = rootView.findViewById(R.id.fragment_account_tv_back_label);
        mBackIcon = rootView.findViewById(R.id.fragment_account_tv_back_icon);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        rootView.findViewById(R.id.fragment_account_tv_back_label).setOnClickListener(this);
        rootView.findViewById(R.id.fragment_account_tv_back_icon).setOnClickListener(this);
        rootView.findViewById(R.id.fragment_account_tv_edit_profile).setOnClickListener(this);
        mTV_setting.setOnClickListener(this);
        mTV_resetPass.setOnClickListener(this);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);
        initToolbar();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == R.id.fragment_account_tv_back_label || id == R.id.fragment_account_tv_back_icon) {
            mPageController.goBack();
        } else if (id == mTV_setting.getId()) {
            mPageController.loadFragment(SettingFragment.newInstance());
        } else if (id == mTV_resetPass.getId()) {
            mPageController.loadFragment(ResetPasswordFragment.newInstance());
        } else if (id == R.id.fragment_account_tv_edit_profile) {
            mPageController.loadFragment(EditProfileFragment.newInstance());
        }
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mBackIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mBackLabel.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));

                break;
        }
    }
}
