package com.edoramedia.alaan.fragment;

import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

/**
 * TV live stream page.
 */
public class LiveTVFragment extends BaseFragment implements View.OnClickListener {

    private PlayerView mPlayerView;
    /**
     * Fake stream url.
     */
    public String mStreamUrl = "http://www.streambox.fr/playlists/test_001/stream.m3u8";
    private SimpleExoPlayer mPlayer;
    private long playBackPostion = 0;
    private Dialog mFullScreenDialog;
    private boolean mIsPlayerFullscreen = false;
    private TextView mIMG_fullScreen;
    private View mRootView;
    private ImageView mIMG_liveIcon;
    private ConstraintLayout mPanelMain;
    private LinearLayout mPanel_refresh;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_changeQuality;
    private TextView mTV_humburger;

    /**
     * The M tv refresh icon.
     */
    TextView mTV_refreshIcon;


    /**
     * New instance live tv fragment.
     *
     * @return the live tv fragment
     */
    public static LiveTVFragment newInstance() {
        LiveTVFragment fragment = new LiveTVFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_live_tv, container, false);
        mRootView = rootView;
        initView(rootView);
        initPlayer();
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        mPlayer.setPlayWhenReady(false);

    }

    @Override
    public void onResume() {
        super.onResume();
        mPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPlayer.release();
        mPlayer = null;
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humburger.setTextColor(ContextCompat.getColor(mActivity,R.color.gray_text_dark));
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == R.id.toolbar_ic_back || id == R.id.toolbar_label_back) {
            mPageController.goBack();
        } else if (id == mIMG_fullScreen.getId()) {
            if (!mIsPlayerFullscreen)
                openFullscreenDialog();
            else
                closeFullscreenDialog();
        } else if (id == mTV_refreshIcon.getId()) {
            tryToPlay();
        } else if (id == mTV_changeQuality.getId()) {
            showQualityList();
        }

    }

    private void showQualityList() {
        final DialogFragmentQuality dialogFragmentQuality = new DialogFragmentQuality();
        dialogFragmentQuality.displayChangeQuality((AppCompatActivity) mActivity, new DialogFragmentQuality.OnQualityChange() {
            @Override
            public void onSelectedQuality(int quality) {
                mTV_changeQuality.setText(quality + "p");
                dialogFragmentQuality.dismiss();
            }
        });
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        mPlayerView = rootView.findViewById(R.id.fragment_play_show_player_amazon);
        mPanelMain = rootView.findViewById(R.id.panel_main);
        mPanel_refresh = rootView.findViewById(R.id.fragment_live_panel_refresh);
        mTV_refreshIcon = rootView.findViewById(R.id.fragment_live_ic_refresh);
        mTV_humburger = rootView.findViewById(R.id.toolbar_ic_hamburger);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_ic_back).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_label_back).setOnClickListener(this);
        mTV_refreshIcon.setOnClickListener(this);
        mIMG_fullScreen = mPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mIMG_liveIcon = mPlayerView.findViewById(R.id.frame_exo_control_view_live_img_live);
        mTV_changeQuality = mPlayerView.findViewById(R.id.frame_exo_control_view_live_btn_change_quality);
        mIMG_fullScreen.setOnClickListener(this);
        mTV_changeQuality.setOnClickListener(this);
        Glide.with(this).asGif().load(R.raw.gif_live_show).into(mIMG_liveIcon);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.fragment_author_article_img_logo);
        initToolbar();


    }


    private void initPlayer() {

        mPlayer = ExoPlayerFactory.newSimpleInstance(mActivity);
        mPlayerView.setPlayer(mPlayer);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(mActivity,
                Util.getUserAgent(mActivity, "app"));

        HlsMediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(mStreamUrl));
        mPlayer.prepare(videoSource);
        mPlayer.setPlayWhenReady(true);

        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                        Log.e(Constant.DEBUG_LOG_KEY, "STATE_BUFFERING");
                        break;
                    case Player.STATE_ENDED:
                        Log.e(Constant.DEBUG_LOG_KEY, "STATE_ENDED");

                        break;
                    case Player.STATE_IDLE:
                        Log.e(Constant.DEBUG_LOG_KEY, "STATE_IDLE");

                        break;
                    case Player.STATE_READY:
                        Log.e(Constant.DEBUG_LOG_KEY, "STATE_READY");
                        hideRefreshPanel();

                        break;
                    default:
                        Log.e(Constant.DEBUG_LOG_KEY, "STATE_IDLE");

                        break;
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                playBackPostion = mPlayer.getCurrentPosition();
                showRefreshPanel();

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

    private void tryToPlay() {
        Animation animRotateAclk = AnimationUtils.loadAnimation(mActivity.getApplicationContext(), R.anim.anim_rotate_clock);
        animRotateAclk.setRepeatCount(Animation.INFINITE);
        mTV_refreshIcon.startAnimation(animRotateAclk);
        initPlayer();
    }

    private void hideRefreshPanel() {
        mPanel_refresh.setVisibility(View.GONE);

    }

    private void showRefreshPanel() {
        mPanel_refresh.setVisibility(View.VISIBLE);

    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mActivity, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mIsPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        ((AppCompatActivity) mActivity).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        initFullscreenDialog();
        ((ViewGroup) mPlayerView.getParent()).removeView(mPlayerView);
        ((ViewGroup) mPanel_refresh.getParent()).removeView(mPanel_refresh);
        mFullScreenDialog.addContentView(mPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenDialog.addContentView(mPanel_refresh, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mIMG_fullScreen.setText(R.string.faw_minimize);
        mIsPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    private void closeFullscreenDialog() {
        ((AppCompatActivity) mActivity).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ((ViewGroup) mPlayerView.getParent()).removeView(mPlayerView);
        ((ViewGroup) mPanel_refresh.getParent()).removeView(mPanel_refresh);
        mPanelMain.addView(mPlayerView, 0, 0);
        mPanelMain.addView(mPanel_refresh, 0, 0);

        ConstraintSet set = new ConstraintSet();
        set.clone(mPanelMain);
        set.connect(mPlayerView.getId(), ConstraintSet.TOP, R.id.constraintLayout, ConstraintSet.BOTTOM);
        set.connect(mPlayerView.getId(), ConstraintSet.BOTTOM, R.id.guideline, ConstraintSet.TOP);
        set.connect(mPlayerView.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
        set.connect(mPlayerView.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START);

        set.connect(mPanel_refresh.getId(), ConstraintSet.TOP, R.id.constraintLayout, ConstraintSet.BOTTOM);
        set.connect(mPanel_refresh.getId(), ConstraintSet.BOTTOM, R.id.guideline, ConstraintSet.TOP);
        set.connect(mPanel_refresh.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END);
        set.connect(mPanel_refresh.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START);
        set.applyTo(mPanelMain);

        mIMG_fullScreen.setText(R.string.faw_maximize);
        mIsPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
    }
}
