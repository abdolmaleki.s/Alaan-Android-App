package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;


/**
 * this dialog contains video quality listing for choose by user
 */
public class DialogFragmentQuality extends DialogFragment implements View.OnClickListener {

    /**
     * The M on quality change.
     */
    OnQualityChange mOnQualityChange;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_change_quality, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(1000,ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
    }

    private void initView(View rootView) {
        rootView.findViewById(R.id.fragment_change_quality_tv_720).setOnClickListener(this);
        rootView.findViewById(R.id.fragment_change_quality_tv_480).setOnClickListener(this);
        rootView.findViewById(R.id.fragment_change_quality_tv_360).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.fragment_change_quality_tv_720) {
            mOnQualityChange.onSelectedQuality(Constant.VideoQuality.P720);
        } else if (id == R.id.fragment_change_quality_tv_480) {
            mOnQualityChange.onSelectedQuality(Constant.VideoQuality.P480);
        } else if (id == R.id.fragment_change_quality_tv_360) {
            mOnQualityChange.onSelectedQuality(Constant.VideoQuality.P360);
        }
    }

    /**
     * Display change quality.
     *
     * @param activity        the activity
     * @param onQualityChange the on quality change
     */
    public void displayChangeQuality(AppCompatActivity activity, OnQualityChange onQualityChange) {
        this.show(activity.getSupportFragmentManager(), DialogFragmentQuality.class.getName());
        mOnQualityChange = onQualityChange;
    }

    /**
     * The interface On quality change.
     */
    public interface OnQualityChange {
        /**
         * On selected quality.
         *
         * @param quality the quality
         */
        void onSelectedQuality(int quality);
    }
}
