package com.edoramedia.alaan.fragment;

import android.os.Bundle;

import com.edoramedia.alaan.application.Constant;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

/**
 * Youtube player view
 */
public class YouTubeFragment extends YouTubePlayerSupportFragment {

    /**
     * Instantiates a new You tube fragment.
     */
    public YouTubeFragment() {
    }

    /**
     * New instance you tube fragment.
     *
     * @param url the youtube stream url
     * @return the you tube fragment
     */
    public static YouTubeFragment newInstance(String url) {

        YouTubeFragment f = new YouTubeFragment();
        Bundle b = new Bundle();
        b.putString("url", url);
        f.setArguments(b);
        f.init();

        return f;
    }

    private void init() {

        initialize(Constant.YOUTUBE_PLAYER_API_KEY, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
            }

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                if (!wasRestored) {
                    player.cueVideo(getArguments().getString("url"));
                }
            }
        });
    }

}
