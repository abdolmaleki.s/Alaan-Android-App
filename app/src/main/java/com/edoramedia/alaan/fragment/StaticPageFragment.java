package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Static Page contains About us , Terms and Policy.
 */
public class StaticPageFragment extends BaseFragment implements View.OnClickListener {

    /**
     * The constant TYPE_ABOUT.
     */
    public static final int TYPE_ABOUT = 1;
    /**
     * The constant TYPE_CONDITIONS.
     */
    public static final int TYPE_CONDITIONS = 2;
    /**
     * The constant TYPE_POLICY.
     */
    public static final int TYPE_POLICY = 3;
    /**
     * The constant STATIC_PAGE_TYPE_KEY.
     */
    public static final String STATIC_PAGE_TYPE_KEY = "STATIC_PAGE_TYPE_KEY";

    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;

    private Spinner mSP_type;
    private int mPageType;
    private ImageView mIMG_image;
    private TextView mTV_humbergerIcon;
    private ImageView mIMG_liveShow;

    /**
     * New instance static page fragment.
     *
     * @param type the type
     * @return the static page fragment
     */
    public static StaticPageFragment newInstance(int type) {
        StaticPageFragment fragment = new StaticPageFragment();
        Bundle args = new Bundle();
        args.putInt(STATIC_PAGE_TYPE_KEY, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_static_page, container, false);
        loadDate();
        initView(rootView);
        initSpinner();
        return rootView;
    }

    private void loadDate() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            mPageType = bundle.getInt(STATIC_PAGE_TYPE_KEY);
        }
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        rootView.findViewById(R.id.toolbar_iv_live).setOnClickListener(this);
        mSP_type = rootView.findViewById(R.id.fragment_static_page_sp_type);
        mIMG_image = rootView.findViewById(R.id.fragment_static_page_img_image);
        mIMG_liveShow = rootView.findViewById(R.id.toolbar_iv_live);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);

        initToolbar();
        Glide.with(this).load("https://www.ebi.ac.uk/sites/ebi.ac.uk/files/styles/wide_9_6/public/groups/external_relations/images/EBI_south_exterior.jpg").into(mIMG_image);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == R.id.toolbar_iv_live) {
            mPageController.loadFragment(LiveTVFragment.newInstance());
        }
    }

    private void initSpinner() {
        List<String> sessions = new ArrayList<>();
        sessions.add("معلومات عنا");
        sessions.add("الاحکام و الشروط");
        sessions.add("السیاسة و الخصوصیة");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                R.layout.item_spinner, sessions);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        mSP_type.setAdapter(adapter);

        switch (mPageType) {
            case TYPE_ABOUT:
                mSP_type.setSelection(0);
                break;
            case TYPE_CONDITIONS:
                mSP_type.setSelection(1);
                break;
            case TYPE_POLICY:
                mSP_type.setSelection(2);
                break;
        }
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mIMG_liveShow.setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_aish_video_camera));
                mIMG_liveShow.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_left_icon_width);
                mIMG_liveShow.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_left_icon_width);
                break;
        }
    }
}
