package com.edoramedia.alaan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.adapter.ShowAdapter;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.infrastructure.Convertor;
import com.edoramedia.alaan.infrastructure.SpacesItemDecoration;
import com.edoramedia.alaan.viewmodel.ShowModel;

import java.util.ArrayList;
import java.util.List;

/**
 * List of Show (TV/FM)
 */
public class ShowFragment extends BaseFragment implements View.OnClickListener {

    private ImageView mIMG_liveShow;
    private RecyclerView mList_shows;
    private final String backimageUrl = "https://i.kinja-img.com/gawker-media/image/upload/s--GpcFH1UL--/c_scale,f_auto,fl_progressive,q_80,w_800/d6h1yrp2v7fvdsm5osem.jpg";
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;

    /**
     * New instance show fragment.
     *
     * @return the show fragment
     */
    public static ShowFragment newInstance() {
        ShowFragment fragment = new ShowFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_show, container, false);
        initView(rootView);
        initAdapter();
        return rootView;
    }

    private void initAdapter() {

        List<ShowModel> modelList = new ArrayList<>();
        ShowModel model1 = new ShowModel();
        ShowModel model2 = new ShowModel();
        ShowModel model3 = new ShowModel();
        ShowModel model4 = new ShowModel();

        model1.title = "فی الطریق الی الاوشیا";
        model1.weekDays = "کل خمیس";
        model1.time = "21:00UEA - 22:30KSA";
        model1.imageUrl = backimageUrl;
        model1.logoUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";

        model2.title = "فی الطریق الی الاوشیا";
        model2.weekDays = "کل خمیس";
        model2.time = "21:00UEA - 22:30KSA";
        model2.imageUrl = backimageUrl;
        model2.logoUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";

        model3.title = "فی الطریق الی الاوشیا";
        model3.weekDays = "کل خمیس";
        model3.time = "21:00UEA - 22:30KSA";
        model3.imageUrl = backimageUrl;
        model3.logoUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";

        model4.title = "فی الطریق الی الاوشیا";
        model4.weekDays = "کل خمیس";
        model4.time = "21:00UEA - 22:30KSA";
        model4.imageUrl = backimageUrl;
        model4.logoUrl = "http://www-hep.phys.cmu.edu/logos/NASA_logo.png";

        modelList.add(model1);
        modelList.add(model2);
        modelList.add(model3);
        modelList.add(model4);

        ShowAdapter adapter = new ShowAdapter(modelList, mActivity, new ShowAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position, ShowModel model) {
                mPageController.loadFragment(SpecificShowFragment.newInstance());
            }
        });

        mList_shows.setLayoutManager(new GridLayoutManager(mActivity, 2));
        mList_shows.setAdapter(adapter);
        mList_shows.addItemDecoration(new SpacesItemDecoration((int) Convertor.convertDpToPixel(8f, mActivity)));

    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        mIMG_liveShow = rootView.findViewById(R.id.toolbar_img_live);
        mIMG_liveShow.setOnClickListener(this);
        mList_shows = rootView.findViewById(R.id.fragment_show_list_shows);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);
        initToolbar();


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mIMG_liveShow.getId()) {
            mPageController.loadFragment(LiveTVFragment.newInstance());
        }
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                mIMG_liveShow.setVisibility(View.GONE);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);

                break;
        }
    }
}
