package com.edoramedia.alaan.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.edoramedia.alaan.Interface.IPageController;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.infrastructure.Helper;
import com.edoramedia.alaan.setting.AppSetting;

/**
 * base fragment that contains common features of all pages.
 */
public abstract class BaseFragment extends Fragment {

    /**
     * with this interface in every page can control contains such as go to previous step, load other page or exit from curren page
     */
    protected IPageController mPageController;
    /**
     * instance of page container for connecting hardware modules
     */
    protected Context mActivity;
    /**
     * variable indicates the site (akhbar/tv/fm/aish).
     */
    protected int mSecetion;

    protected int mSecetionImageId;
    /**
     * indicates the color of toolbar base of section.
     */
    protected int mSecetionColor;

    @Override
    public void onPause() {
        super.onPause();
        Helper.progressBar.hideDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity = null;
        mPageController = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = context;
        mPageController = (IPageController) context;
        mSecetion = AppSetting.getInstance().getAppSection();
        configToolbar();
    }

    private void configToolbar() {
        switch (mSecetion) {
            case 1: //tv
                mSecetionColor = ContextCompat.getColor(mActivity, R.color.alan_tv_primary);
                mSecetionImageId = R.drawable.ic_outlet_tv;
                break;

            case 2: //akhbar
                mSecetionColor = ContextCompat.getColor(mActivity, R.color.alaan_akhbar_primary);
                mSecetionImageId = R.drawable.ic_outlet_news;
                break;

            case 3: //fm
                mSecetionColor = ContextCompat.getColor(mActivity, R.color.alaan_FM_primary);
                mSecetionImageId = R.drawable.ic_fm_toolbar;
                break;

            case 4: //aish
                mSecetionColor = ContextCompat.getColor(mActivity, R.color.alaan_aish_primary);
                mSecetionImageId = R.drawable.ic_outlet_aish;
                break;
        }
    }

    /**
     * initialize toolbar such as color, icons, logo base on section
     */
    abstract void initToolbar();
}
