package com.edoramedia.alaan.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dailymotion.android.player.sdk.PlayerWebView;
import com.dailymotion.android.player.sdk.events.PlayerEvent;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.adapter.GalleryPageAdapter;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.infrastructure.Convertor;
import com.edoramedia.alaan.viewmodel.ArticleModel;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.warkiz.tickseekbar.OnSeekChangeListener;
import com.warkiz.tickseekbar.SeekParams;
import com.warkiz.tickseekbar.TickSeekBar;

import java.util.ArrayList;
import java.util.Objects;

import static com.edoramedia.alaan.infrastructure.Convertor.fromHtml;

/**
 * Specific Article page.
 */
public class SpecificArticleFragment extends BaseFragment implements View.OnClickListener, View.OnTouchListener {

    private RelativeLayout mPanelRoot;
    private ConstraintLayout mPanelFont;
    private FrameLayout mPanelContent;
    private FrameLayout mPanelMedia;
    private FrameLayout mPanelCustomImage;
    private FrameLayout mPanelGallery;
    private TextView mTV_fontIcon;
    private TextView mTV_backIcon;
    private TextView mTV_shareIcon;
    private ArticleModel articleModel;
    private ImageView mIMG_author;
    private TextView mTV_lampIcon;
    private TextView mTV_mainText;
    private TextView mTV_title;
    private TextView mTV_authorName;
    private TextView mTV_date;
    private TextView mTV_category;
    private boolean isNightMode = false;
    private TickSeekBar mSeek_fontSize;

    ///////////////////////////////////////////////////////////////////////
    //////////////////////// Image Media //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    private ImageView mIMG_mediaImage;

    ///////////////////////////////////////////////////////////////////////
    //////////////////////// Custom Image Media //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    private ImageView mIMG_customMediaImage;

    ///////////////////////////////////////////////////////////////////////
    //////////////////////// Gallery Media //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    private ViewPager mViewpager_Gallery;
    private LinearLayout dotsLayout;

    ///////////////////////////////////////////////////////////////////////
    //////////////////////// Video Media //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////

    private TextView mTV_refreshIcon;
    private TextView mIMG_fullScreen;
    private Dialog mFullScreenDialog;
    private boolean mIsPlayerFullscreen = false;
    private long playBackPostion = 0;
    private int mVideoSourceType = Constant.VideoTypes.AMAZON;

    //////////// Amazon ////////////////////////////
    private SimpleExoPlayer mExoPlayer;
    private PlayerView mExoPlayerView;
    /**
     * Fake Stream URl.
     */
    public String mStreamUrl = "http://www.streambox.fr/playlists/test_001/stream.m3u8";
    private LinearLayout mPanel_refresh;
    private TextView mTV_changeQuality;


    //////////// Dailymotion ////////////////////////////
    private PlayerWebView mDailymotionPlayer;

    //////////// Youtube ////////////////////////////
    private YouTubeFragment mYouTubeFragment;


    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_humbergerIcon;

    /**
     * New instance specific article fragment.
     *
     * @return the specific article fragment
     */
    public static SpecificArticleFragment newInstance() {
        SpecificArticleFragment fragment = new SpecificArticleFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * New instance specific article fragment.
     *
     * @param modelType the model type (Image/Video/Gallery or etc)
     * @return the specific article fragment
     */
    public static SpecificArticleFragment newInstance(int modelType) {
        SpecificArticleFragment fragment = new SpecificArticleFragment();
        Bundle args = new Bundle();
        args.putInt(Constant.Keys.ARTICLE_TYPE, modelType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_specific_article, container, false);
        loadData();
        initView(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (articleModel.type == ArticleModel.TYPE_VIDEO || articleModel.type == ArticleModel.TYPE_AISH_VIDEO) {
            resumePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (articleModel.type == ArticleModel.TYPE_VIDEO || articleModel.type == ArticleModel.TYPE_AISH_VIDEO) {
            pausePlayer();
        }

    }

    @Override
    public void onDestroy() {
        if (articleModel.type == ArticleModel.TYPE_VIDEO || articleModel.type == ArticleModel.TYPE_AISH_VIDEO) {
            releasePlayer();
        }
        super.onDestroy();
    }

    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mTV_fontIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.alan_tv_primary));
                mTV_backIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mTV_shareIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));

                break;
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mTV_fontIcon.getId()) {
            handleShowFontResizer();
        } else if (id == mTV_lampIcon.getId()) {
            handleNightMode();
        } else if (id == R.id.specific_article_ic_back) {
            mPageController.goBack();
        } else if (id == R.id.fragment_specific_article_ic_share) {
            shareLink();
        } else if (id == mIMG_fullScreen.getId()) {

            if (!mIsPlayerFullscreen)
                openFullscreenDialog();
            else
                closeFullscreenDialog();
        } else if (id == mTV_refreshIcon.getId()) {
            tryToPlay();
        } else if (id == mTV_authorName.getId() || id == mIMG_author.getId()) {
            mPageController.loadFragment(AuthorArticleFragment.newInstance());

        } else if (id == mTV_changeQuality.getId()) {
            showQualityList();
        } else if (id == mTV_category.getId()) {
            mPageController.loadFragment(ArticleFragment.newInstance(0));
        }
    }

    private void loadData() {
        articleModel = new ArticleModel();

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            articleModel.type = bundle.getInt(Constant.Keys.ARTICLE_TYPE, ArticleModel.TYPE_AISH_GALLERY);
        }
        articleModel.galleryImageUrls = new ArrayList<>();
        articleModel.galleryImageUrls.add(Constant.FAKE_Article_IMAGE_URL);
        articleModel.galleryImageUrls.add("https://cdn.isna.ir/d/2018/12/31/3/57805937.jpg");
        articleModel.galleryImageUrls.add("https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg");

    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(true);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        bindView(rootView);
        adjustMedia();
        fillHeader();
        initFontResizerSeek();
        initToolbar();
        setSectionTheme();

    }

    private void setSectionTheme() {
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mTV_category.setTextColor(ContextCompat.getColor(mActivity, R.color.alaan_akhbar_primary));
                break;

            case Constant.Section.FM:
                break;

            case Constant.Section.AISH:
                break;
        }
    }

    private void fillHeader() {
        Glide.with(this).load(Constant.FAKE_AUTHOR_IMAGE_URL).apply(new RequestOptions().circleCrop()).into(mIMG_author);
    }

    private void bindView(View rootView) {

        mPanelRoot = rootView.findViewById(R.id.fragment_specific_article_panel_root);
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);
        mPanelFont = rootView.findViewById(R.id.fragment_specific_article_panel_font_resizer);
        mPanelContent = rootView.findViewById(R.id.fragment_specific_article_panel_content);
        mPanelMedia = rootView.findViewById(R.id.fragment_specific_article_panel_media);
        mPanelCustomImage = rootView.findViewById(R.id.fragment_specific_article_panel_media_custom_image);
        mPanelGallery = rootView.findViewById(R.id.fragment_specific_article_panel_gallery);
        mTV_fontIcon = rootView.findViewById(R.id.fragment_specific_article_ic_font_size);
        mTV_shareIcon = rootView.findViewById(R.id.fragment_specific_article_ic_share);
        mTV_backIcon = rootView.findViewById(R.id.specific_article_ic_back);
        mIMG_mediaImage = rootView.findViewById(R.id.fragment_specific_article_img_media);
        mIMG_customMediaImage = rootView.findViewById(R.id.fragment_specific_article_custom_image_img_background);
        mIMG_author = rootView.findViewById(R.id.fragment_specific_article_img_author);
        mTV_lampIcon = rootView.findViewById(R.id.fragment_specific_Article_tv_lamp_icon);
        mTV_mainText = rootView.findViewById(R.id.fragment_specific_article_tv_main_text);
        mTV_title = rootView.findViewById(R.id.fragment_specific_article_tv_title);
        mTV_authorName = rootView.findViewById(R.id.fragment_specific_article_tv_author);
        mTV_category = rootView.findViewById(R.id.fragment_specific_article_tv_category);
        mTV_date = rootView.findViewById(R.id.fragment_specific_article_tv_date);
        mSeek_fontSize = rootView.findViewById(R.id.fragment_specific_article_seek_font_size);
        mViewpager_Gallery = rootView.findViewById(R.id.fragment_specific_article_gallery_pager_imagelist);
        dotsLayout = rootView.findViewById(R.id.fragment_specific_article_gallery_panel_dots);
        rootView.findViewById(R.id.specific_article_ic_back).setOnClickListener(this);
        rootView.findViewById(R.id.fragment_specific_article_ic_share).setOnClickListener(this);
        mTV_fontIcon.setOnClickListener(this);
        mTV_lampIcon.setOnClickListener(this);
        mTV_authorName.setOnClickListener(this);
        mIMG_author.setOnClickListener(this);
        mTV_category.setOnClickListener(this);

        mExoPlayerView = rootView.findViewById(R.id.fragment_specific_article_player_amazon);
        mPanel_refresh = rootView.findViewById(R.id.fragment_specific_article_panel_refresh);
        mTV_refreshIcon = rootView.findViewById(R.id.fragment_specific_article_ic_refresh);
        mDailymotionPlayer = rootView.findViewById(R.id.fragment_specific_article_player_dailymotion);
        mIMG_fullScreen = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mTV_changeQuality = mExoPlayerView.findViewById(R.id.frame_exo_control_view_archive_btn_change_quality);
        mIMG_fullScreen.setOnClickListener(this);
        mTV_refreshIcon.setOnClickListener(this);
        mTV_changeQuality.setOnClickListener(this);

        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);

        mPanelRoot.setOnTouchListener(this);
        mPanelMedia.setOnTouchListener(this);
        mPanelContent.setOnTouchListener(this);
    }

    private void initFontResizerSeek() {
        mSeek_fontSize.setProgress(Convertor.convertPixelsToSP(mTV_mainText.getTextSize(), mActivity));
        mSeek_fontSize.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                mTV_mainText.setTextSize(TypedValue.COMPLEX_UNIT_SP, seekParams.progressFloat);
            }

            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }

        });
    }

    private void handleNightMode() {

        int whiteColor = ContextCompat.getColor(mActivity, R.color.white);
        int grayColor = ContextCompat.getColor(mActivity, R.color.gray_text_dark);

        if (!isNightMode) {
            mTV_authorName.setTextColor(whiteColor);
            mTV_date.setTextColor(whiteColor);
            mTV_title.setTextColor(whiteColor);
            mTV_mainText.setTextColor(whiteColor);
            mPanelContent.setBackgroundColor(grayColor);
            mTV_lampIcon.setBackground(ContextCompat.getDrawable(mActivity, R.drawable.shape_bg_nightmode));
            mTV_lampIcon.setText(getString(R.string.faw_lamp_off));

        } else {
            mTV_authorName.setTextColor(grayColor);
            mTV_date.setTextColor(grayColor);
            mTV_title.setTextColor(grayColor);
            mTV_mainText.setTextColor(grayColor);
            mPanelContent.setBackgroundColor(whiteColor);
            mTV_lampIcon.setBackground(null);
            mTV_lampIcon.setText(getString(R.string.faw_lamp));
        }

        isNightMode = !isNightMode;
    }

    private void handleShowFontResizer() {
        if (mPanelFont.getVisibility() == View.VISIBLE) {
            mPanelFont.setVisibility(View.GONE);
        } else {
            mPanelFont.setVisibility(View.VISIBLE);
        }
    }

    private void adjustMedia() {

        switch (articleModel.type) {
            case ArticleModel.TYPE_IMAGE:
            case ArticleModel.TYPE_AISH_IMAGE:
                loadImage();
                break;

            case ArticleModel.TYPE_VIDEO:
            case ArticleModel.TYPE_AISH_VIDEO:
                initPlayer();
                break;

            case ArticleModel.TYPE_IMAGE_CUSTOM:
            case ArticleModel.TYPE_AISH_IMAGE_CUSTOM:
                loadCustomImage();
                break;

            case ArticleModel.TYPE_GALLERY:
            case ArticleModel.TYPE_AISH_GALLERY:
                initGallery();
                break;


        }

    }

    private void loadImage() {
        mIMG_mediaImage.setVisibility(View.VISIBLE);
        Glide.with(this).load(Constant.FAKE_Article_IMAGE_URL).into(mIMG_mediaImage);
    }

    private void loadCustomImage() {
        mPanelCustomImage.setVisibility(View.VISIBLE);
        Glide.with(this).load(Constant.FAKE_Article_CUSTOM_IMAGE_URL).into(mIMG_customMediaImage);
    }

    private void initGallery() {
        mPanelGallery.setVisibility(View.VISIBLE);
        GalleryPageAdapter galleryPageAdapter = new GalleryPageAdapter(mActivity, articleModel.galleryImageUrls);
        mViewpager_Gallery.setAdapter(galleryPageAdapter);
        addBottomDots(0, dotsLayout);
        mViewpager_Gallery.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position, dotsLayout);
            }

            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }

        });
    }

    private void addBottomDots(int currentPage, LinearLayout dotsLayout) {

        TextView[] dots = new TextView[Objects.requireNonNull(mViewpager_Gallery.getAdapter()).getCount()];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(mActivity);
            dots[i].setText(fromHtml("&#9675;"));
            dots[i].setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.gallery_dots_size));
            dots[i].setTextColor(mActivity.getResources().getColor(R.color.white));
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setText(fromHtml("&#9679;"));

    }

    private void shareLink() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Arshanikan");
            String sAux = "\nبا اپلیکیشن آرشانیکان زمین تنیس رزرو کن حالشو ببر\n\n";
            sAux = sAux + "<https://play.google.com/store/apps/details?id=com.edorosoft.edorok12 \n\n>";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "اختر خيارا"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    //
    //  __     ___     _              _   _                 _      _ _
    //  \ \   / (_) __| | ___  ___   | | | | __ _ _ __   __| | ___| (_)_ __   __ _
    //   \ \ / /| |/ _` |/ _ \/ _ \  | |_| |/ _` | '_ \ / _` |/ _ \ | | '_ \ / _` |
    //    \ V / | | (_| |  __/ (_) | |  _  | (_| | | | | (_| |  __/ | | | | | (_| |
    //     \_/  |_|\__,_|\___|\___/  |_| |_|\__,_|_| |_|\__,_|\___|_|_|_| |_|\__, |
    //                                                                       |___/

    private void initPlayer() {
        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                initExoPlayer();
                break;
            case Constant.VideoTypes.YOUTUBE:
                initYoutubePlayer();
                break;
            case Constant.VideoTypes.DAILYMOTION:
                initDailymotionPlayer();
                break;
        }
    }

    private void initYoutubePlayer() {
        mYouTubeFragment = YouTubeFragment.newInstance("E5T7Txo9sWk");
        ((AppCompatActivity) mActivity).getSupportFragmentManager().beginTransaction().replace(mPanelMedia.getId(), mYouTubeFragment).commit();
    }

    private void initExoPlayer() {
        mExoPlayerView.setVisibility(View.VISIBLE);
        mExoPlayer = ExoPlayerFactory.newSimpleInstance(mActivity);
        mExoPlayerView.setPlayer(mExoPlayer);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(mActivity,
                Util.getUserAgent(mActivity, "app"));

        HlsMediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(mStreamUrl));
        mExoPlayer.prepare(videoSource);
        mExoPlayer.setPlayWhenReady(true);

        mExoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                        break;
                    case Player.STATE_ENDED:

                        break;
                    case Player.STATE_IDLE:

                        break;
                    case Player.STATE_READY:
                        hideRefreshPanel();

                        break;
                    default:
                        break;
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                playBackPostion = mExoPlayer.getCurrentPosition();
                showRefreshPanel();

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

    private void initDailymotionPlayer() {

        mDailymotionPlayer.setVisibility(View.VISIBLE);
        mDailymotionPlayer.load("x70clkm");
        mDailymotionPlayer.setPlayerEventListener(new PlayerWebView.PlayerEventListener() {
            @Override
            public void onEvent(@NonNull PlayerEvent event) {
                Log.e(Constant.DEBUG_LOG_KEY, event.getName());
                if (event.getName().equals(PlayerWebView.EVENT_FULLSCREEN_TOGGLE_REQUESTED)) {

                    if (!mIsPlayerFullscreen) {
                        openFullscreenDialog();
                    } else {
                        closeFullscreenDialog();
                    }
                }
            }
        });
    }

    private void hideRefreshPanel() {
        mPanel_refresh.setVisibility(View.GONE);

    }

    private void showRefreshPanel() {
        mPanel_refresh.setVisibility(View.VISIBLE);

    }

    private void tryToPlay() {

        Animation animRotateAclk = AnimationUtils.loadAnimation(mActivity.getApplicationContext(), R.anim.anim_rotate_clock);
        animRotateAclk.setRepeatCount(Animation.INFINITE);
        mTV_refreshIcon.startAnimation(animRotateAclk);

        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                initExoPlayer();
                break;
            case Constant.VideoTypes.DAILYMOTION:
                initDailymotionPlayer();
                break;
        }

    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mActivity, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mIsPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        ((AppCompatActivity) mActivity).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        initFullscreenDialog();


        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                ((ViewGroup) mExoPlayerView.getParent()).removeView(mExoPlayerView);
                ((ViewGroup) mPanel_refresh.getParent()).removeView(mPanel_refresh);
                mFullScreenDialog.addContentView(mExoPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                mFullScreenDialog.addContentView(mPanel_refresh, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                mIMG_fullScreen.setText(R.string.faw_minimize);
                break;

            case Constant.VideoTypes.DAILYMOTION:
                ((ViewGroup) mDailymotionPlayer.getParent()).removeView(mDailymotionPlayer);
                mFullScreenDialog.addContentView(mDailymotionPlayer, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                mDailymotionPlayer.setFullscreenButton(true);
                break;
        }


        mIsPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    private void closeFullscreenDialog() {
        ((AppCompatActivity) mActivity).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                ((ViewGroup) mExoPlayerView.getParent()).removeView(mExoPlayerView);
                ((ViewGroup) mPanel_refresh.getParent()).removeView(mPanel_refresh);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.specific_article_video_height));
                mPanelMedia.addView(mExoPlayerView, layoutParams);
                mPanelMedia.addView(mPanel_refresh, layoutParams);
                mIMG_fullScreen.setText(R.string.faw_maximize);
                break;

            case Constant.VideoTypes.DAILYMOTION:
                ((ViewGroup) mDailymotionPlayer.getParent()).removeView(mDailymotionPlayer);
                FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.specific_article_video_height));
                mPanelMedia.addView(mDailymotionPlayer, layoutParams2);
                mDailymotionPlayer.setFullscreenButton(false);
                break;
        }


        mIsPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
    }

    private void releasePlayer() {
        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                mExoPlayer.release();
                mExoPlayer = null;
                break;
            case Constant.VideoTypes.YOUTUBE:
                mYouTubeFragment.onDestroy();
                break;
            case Constant.VideoTypes.DAILYMOTION:
                mDailymotionPlayer.release();
                mDailymotionPlayer = null;
                break;
        }

    }

    private void resumePlayer() {
        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                mExoPlayer.setPlayWhenReady(true);
                break;
            case Constant.VideoTypes.DAILYMOTION:
                mDailymotionPlayer.onResume();
                break;
        }
    }

    private void pausePlayer() {
        switch (mVideoSourceType) {
            case Constant.VideoTypes.AMAZON:
                mExoPlayer.setPlayWhenReady(false);
                break;
            case Constant.VideoTypes.DAILYMOTION:
                mDailymotionPlayer.onPause();
                break;
        }

    }

    private void showQualityList() {
        final DialogFragmentQuality dialogFragmentQuality = new DialogFragmentQuality();
        dialogFragmentQuality.displayChangeQuality((AppCompatActivity) mActivity, new DialogFragmentQuality.OnQualityChange() {
            @Override
            public void onSelectedQuality(int quality) {
                mTV_changeQuality.setText(quality + "p");
                dialogFragmentQuality.dismiss();
            }
        });
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if (mPanelFont.getVisibility() == View.VISIBLE) {
            mPanelFont.setVisibility(View.GONE);
        }
        return false;
    }
}

