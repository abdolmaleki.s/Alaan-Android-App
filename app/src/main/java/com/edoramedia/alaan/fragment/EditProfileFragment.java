package com.edoramedia.alaan.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;
import com.fxn.pix.Pix;

import java.util.ArrayList;

/**
 * Edit Profile page
 */
public class EditProfileFragment extends BaseFragment implements View.OnClickListener {

    private ImageView mIV_avatar;
    private static final int IMAGE_REQUEST_CODE = 2501;
    private ConstraintLayout mPanelToolbar;
    private ImageView mIV_toolbarSetionImage;
    private TextView mTV_humbergerIcon;
    private TextView mTV_backIcon;
    private TextView mTV_backLabel;


    /**
     * New instance edit profile fragment.
     *
     * @return the edit profile fragment
     */
    public static EditProfileFragment newInstance() {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_REQUEST_CODE) {
            ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
        }
    }

    private void initView(View rootView) {
        mPageController.needBottomNavigation(false);
        mIV_avatar = rootView.findViewById(R.id.fragment_edit_profile_img_pick_image);
        mTV_humbergerIcon = rootView.findViewById(R.id.toolbar_ic_hamburger);
        mTV_backIcon = rootView.findViewById(R.id.tv_back_icon);
        mTV_backLabel = rootView.findViewById(R.id.tv_back);
        rootView.findViewById(R.id.toolbar_ic_hamburger).setOnClickListener(this);
        mIV_avatar.setOnClickListener(this);
        rootView.findViewById(R.id.fragment_edit_profile_tv_pick_image).setOnClickListener(this);
        rootView.findViewById(R.id.tv_back_icon).setOnClickListener(this);
        rootView.findViewById(R.id.tv_back).setOnClickListener(this);
        mPanelToolbar = rootView.findViewById(R.id.constraintLayout);
        mIV_toolbarSetionImage = rootView.findViewById(R.id.item_list_schedule_program_img_image);
        initToolbar();


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.toolbar_ic_hamburger) {
            mPageController.moveSlidingMenu();
        } else if (id == mIV_avatar.getId() || id == R.id.fragment_edit_profile_tv_pick_image) {
            pickImage();
        } else if (id == R.id.tv_back || id == R.id.tv_back_icon) {
            mPageController.goBack();
        }
    }

    private void pickImage() {
        Pix.start(this, IMAGE_REQUEST_CODE);
    }


    @Override
    void initToolbar() {
        mPanelToolbar.setBackgroundColor(mSecetionColor);
        mIV_toolbarSetionImage.setImageResource(mSecetionImageId);
        switch (mSecetion) {
            case Constant.Section.TV:
                break;

            case Constant.Section.NEWS:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.FM:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_akhbar_icon_width);
                break;

            case Constant.Section.AISH:
                mIV_toolbarSetionImage.getLayoutParams().width = getResources().getDimensionPixelSize(R.dimen.toolbar_aish_icon_width);
                mTV_humbergerIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mTV_backIcon.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));
                mTV_backLabel.setTextColor(ContextCompat.getColor(mActivity, R.color.gray_text_dark));

                break;
        }
    }
}
