package com.edoramedia.alaan.infrastructure;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * aish live listing item decoration
 */
public class SpacesItemDecorationLiveVideo extends RecyclerView.ItemDecoration {
    private int space;

    /**
     * Instantiates a new Spaces item decoration aish live.
     *
     * @param space the space
     */
    public SpacesItemDecorationLiveVideo(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        if (parent.getChildLayoutPosition(view) != 0 && parent.getChildLayoutPosition(view) != 1) {
            outRect.top = space / 2;
        }
        outRect.bottom = space / 2;

        if (parent.getChildLayoutPosition(view) % 2 == 0) {
            outRect.left = space;
            outRect.right = space / 2;

        } else {
            outRect.right = space;
            outRect.left = space / 2;

        }
    }
}