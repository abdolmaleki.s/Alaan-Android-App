package com.edoramedia.alaan.infrastructure;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.shagi.materialdatepicker.date.DatePickerFragmentDialog;

import java.util.Calendar;

/**
 * some public function for date
 */
public class DateHelper {

    /**
     * Show date picker.
     *
     * @param context           the context
     * @param onDateSetListener the on date set listener
     * @param view              the view
     * @return the int
     */
    public static int showDatePicker(Context context, DatePickerFragmentDialog.OnDateSetListener onDateSetListener, View view) {
        try {

            Calendar now = Calendar.getInstance();
            DatePickerFragmentDialog datePickerFragmentDialog = DatePickerFragmentDialog.newInstance(onDateSetListener, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
            datePickerFragmentDialog.show(((AppCompatActivity) context).getSupportFragmentManager(), "tag");
            return view.getId();

        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * convert int day to string format.
     *
     * @param day the int day
     * @return the day of week string
     */
    public static String getDayOFWeekString(int day) {
        String[] strDays = new String[]{"الاحد", "الأثنین", "الثلاثا", "الاربعا", "الخمیس", "الجمعه", "السبت"};
        return strDays[day - 1];
    }

}
