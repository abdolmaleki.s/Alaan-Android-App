package com.edoramedia.alaan.infrastructure;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * The list item decoration.
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    /**
     * Instantiates a new Spaces item decoration.
     *
     * @param space the space
     */
    public SpacesItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = space;
        outRect.top = space;

        if (parent.getChildLayoutPosition(view) % 2 == 0) {
            outRect.left = space;
            outRect.right = space/2;

        } else {
            outRect.right = space;
            outRect.left = space/2;

        }
    }
}