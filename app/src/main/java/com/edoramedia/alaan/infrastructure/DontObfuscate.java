package com.edoramedia.alaan.infrastructure;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * all class that implement this interface will be ignore in obfuscation
 */
@Retention(RetentionPolicy.CLASS)
public @interface DontObfuscate {
}