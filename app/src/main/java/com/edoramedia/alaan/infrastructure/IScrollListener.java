package com.edoramedia.alaan.infrastructure;

import android.widget.FrameLayout;

/**
 * listener for monitoring list
 * scrolls behavior.
 */
public interface IScrollListener {

    void onScrollChanged(FrameLayout scrollView, int x, int y, int oldx, int oldy);
}