package com.edoramedia.alaan.infrastructure;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;

import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Network & Internet status monitoring.
 */
public class NetworkHelper {

    /**
     * Is connecting to internet.
     *
     * @param ctx                       the ctx
     * @param checkNetworkStateListener the check network state listener
     */
    public static void isConnectingToInternet(final Context ctx, final CheckNetworkStateListener checkNetworkStateListener) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                Looper.prepare();

                if (isNetworkAvailable(ctx)) {
                    try {
                        HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com/").openConnection());
                        urlc.setRequestProperty("User-Agent", "Android");
                        urlc.setRequestProperty("Connection", "close");
                        urlc.setReadTimeout(5000);
                        urlc.setConnectTimeout(5000);
                        urlc.connect();
                        if ((urlc.getResponseCode() == 200)) {
                            checkNetworkStateListener.onNetworkChecked(true); // all things ok

                        } else {
                            checkNetworkStateListener.onNetworkChecked(false); // internet problem

                        }
                    } catch (Exception e) {
                        checkNetworkStateListener.onNetworkChecked(false); // code problem
                    }
                } else {
                    checkNetworkStateListener.onNetworkChecked(false); // net problem
                }
            }
        };

        new Thread(runnable).start();

    }

    private static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager
                    cm = (ConnectivityManager) context.getApplicationContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();


            return activeNetwork != null
                    && activeNetwork.isConnectedOrConnecting();

        } catch (Exception e) {
            return false;
        }
    }

    /**
     * The interface Check network state listener.
     */
    public interface CheckNetworkStateListener {
        /**
         * On network checked.
         *
         * @param isSuccess the is success
         */
        void onNetworkChecked(boolean isSuccess);
    }

}
