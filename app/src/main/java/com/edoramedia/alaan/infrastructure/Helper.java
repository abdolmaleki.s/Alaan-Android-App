package com.edoramedia.alaan.infrastructure;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.customview.MyProgressDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * some public function to use in everywhere
 */
public class Helper {

    private static MyProgressDialog progressDialog;

    /**
     * The type Progress bar.
     */
    public static class progressBar {

        /**
         * Show progress dialog.
         *
         * @param activity the activity
         */
        public static void showDialog(Context activity) {

            progressDialog = new MyProgressDialog(activity);
            progressDialog.show();
        }

        /**
         * Hide dialog.
         */
        public static void hideDialog() {

            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    public static Gson getGson() {
        return new GsonBuilder().create();

    }

    /**
     * Show Alert.
     *
     * @param view      the view
     * @param message   the message
     * @param alertType the alert type
     */
    public static void alert(View view, String message, int alertType) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        TextView textView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
        }

        if (alertType == Constant.AlertType.Success) {
            textView.setTextColor(Color.GREEN);
        } else if (alertType == Constant.AlertType.Error) {
            textView.setTextColor(Color.RED);
        } else if (alertType == Constant.AlertType.Information) {
            textView.setTextColor(Color.BLUE);
        } else {
            textView.setTextColor(Color.YELLOW);
        }
        snackbar.show();
    }


    /**
     * Sets html text.
     *
     * @param tv      the tv
     * @param htmlStr the html str
     */
    public static void setHtmlText(TextView tv, String htmlStr) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(htmlStr, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tv.setText(Html.fromHtml(htmlStr));
        }
    }

    /**
     * Open user twitter.
     *
     * @param context the context
     * @param user    the twitter username
     */
    public static void openTwitter(Context context, String user) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + user)));
        } catch (Exception e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + user)));
        }
    }

}
