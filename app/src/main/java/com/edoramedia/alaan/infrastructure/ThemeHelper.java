package com.edoramedia.alaan.infrastructure;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;

/**
 * helper for adjust them in every page
 */
public class ThemeHelper {
    /**
     * Sets status bar color base on section.
     *
     * @param context the context
     * @param section the site id
     */
    public static void setStatusBarColorBaseOnSection(Context context, int section) {
        if (section == Constant.Section.TV) {
            int color = ContextCompat.getColor(context, R.color.alan_tv_primary);
            setStatusBarColor(context, color);
            ((AppCompatActivity) context).getWindow().getDecorView().setSystemUiVisibility(0);
        } else if (section == Constant.Section.NEWS) {
            int color = ContextCompat.getColor(context, R.color.alaan_akhbar_primary);
            setStatusBarColor(context, color);
            ((AppCompatActivity) context).getWindow().getDecorView().setSystemUiVisibility(0);
        } else if (section == Constant.Section.FM) {
            int color = ContextCompat.getColor(context, R.color.alaan_FM_primary);
            setStatusBarColor(context, color);
            ((AppCompatActivity) context).getWindow().getDecorView().setSystemUiVisibility(0);

        } else if (section == Constant.Section.AISH) {
            int color;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                color = ContextCompat.getColor(context, R.color.alaan_aish_primary);
                ((AppCompatActivity) context).getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                color = ContextCompat.getColor(context, R.color.aish_orange);
            }
            setStatusBarColor(context, color);

        }
    }

    private static void setStatusBarColor(Context context, int color) {

        Window window = ((AppCompatActivity) context).getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(color);
    }
}
