package com.edoramedia.alaan.infrastructure;

import android.content.Context;

import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.setting.AppSetting;
import com.edoramedia.alaan.viewmodel.ArticleCategoryModel;
import com.edoramedia.alaan.viewmodel.SlideMenuModel;

import java.util.ArrayList;
import java.util.List;

/**
 * provide data for some functionality
 */
public class DataProvider {

    /**
     * Generate menu items for sliding menu.
     *
     * @param context the context
     * @param section site type
     * @return the items list
     */

    public static List<SlideMenuModel> generateMenuList(Context context, int section) {

        List<SlideMenuModel> menuList = new ArrayList<>();
        List<ArticleCategoryModel> categoryModels = AppSetting.getInstance().getArticleCategories();

        if (section == Constant.Section.TV) {

            List<String> showCategory = new ArrayList<>();
            showCategory.add("کمدی");
            showCategory.add("درام");

            List<String> articleCategory = new ArrayList<>();
            for (ArticleCategoryModel item : categoryModels) {
                articleCategory.add(item.title);
            }

            menuList.add(new SlideMenuModel(context.getString(R.string.faw_search), "بحث", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_tv), "تلفزيون مباشر", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_schedule), "دليل التلفزيون", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_layers), "برامج تلفزيونية", showCategory));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_layers), "مادة", articleCategory));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_info_circle), "معلومات عنا", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_terms), "الأحكام والشروط", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_policy), "السياسة والخصوصية", null));

        } else if (section == Constant.Section.NEWS) {

            List<String> articleCategory = new ArrayList<>();
            articleCategory.add("مادة اول");
            articleCategory.add("مادة دوم");

            menuList.add(new SlideMenuModel(context.getString(R.string.faw_search), "بحث", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_tv), "تلفزيون مباشر", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_layers), "مادة", articleCategory));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_info_circle), "معلومات عنا", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_terms), "الأحكام والشروط", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_policy), "السياسة والخصوصية", null));


        } else if (section == Constant.Section.FM) {

            List<String> showCategory = new ArrayList<>();
            showCategory.add("کمدی");
            showCategory.add("درام");

            List<String> articleCategory = new ArrayList<>();
            articleCategory.add("مادة اول");
            articleCategory.add("مادة دوم");

            menuList.add(new SlideMenuModel(context.getString(R.string.faw_search), "بحث", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_headphone), "الاستماع الراديو", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_schedule), "دليل الراديو", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_layers), "برامج الراديو", showCategory));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_layers), "مادة", articleCategory));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_info_circle), "معلومات عنا", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_terms), "الأحكام والشروط", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_policy), "السياسة والخصوصية", null));


        } else if (section == Constant.Section.AISH) {
            List<String> articleCategory = new ArrayList<>();
            articleCategory.add("مادة اول");
            articleCategory.add("مادة دوم");

            menuList.add(new SlideMenuModel(context.getString(R.string.faw_search), "بحث", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_tv), "برنامج مباشر", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_layers), "مادة", articleCategory));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_info_circle), "معلومات عنا", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_terms), "الأحكام والشروط", null));
            menuList.add(new SlideMenuModel(context.getString(R.string.faw_policy), "السياسة والخصوصية", null));
        }

        return menuList;
    }
}
