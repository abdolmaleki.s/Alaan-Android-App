package com.edoramedia.alaan.infrastructure;

import android.content.Context;
import android.content.res.Resources;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;


/**
 * The Type Convertor.
 */
public class Convertor {

    /**
     * Convert pixels to dp float.
     *
     * @param px      the pixel size
     * @param context the context
     * @return the dp size
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    /**
     * Convert pixels to sp float.
     *
     * @param px      the pixel size
     * @param context the context
     * @return the dp size
     */
    public static float convertPixelsToSP(float px, Context context) {
        float sp = px / context.getResources().getDisplayMetrics().scaledDensity;
        return sp;
    }

    /**
     * Convert dp to pixel float.
     *
     * @param dp      the dp
     * @param context the context
     * @return the float
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * convert html to spand for use in Textview.
     *
     * @param htmlString the html string
     * @return Spand text
     */
    public static Spanned fromHtml(String htmlString) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(htmlString, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(htmlString);
        }
        return result;
    }

    /**
     * Convert time to minutes int.
     *
     * @param time String time
     * @return minutes
     */
    public static int convertTimeToMinutes(String time) {
        String[] timeParams = time.split(":");
        String hour = timeParams[0];
        String minuts = timeParams[1];
        int minutes = Integer.valueOf(hour) * 60 + Integer.valueOf(minuts);
        return minutes;
    }
}
