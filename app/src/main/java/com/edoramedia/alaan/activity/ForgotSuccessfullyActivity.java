package com.edoramedia.alaan.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;

import com.edoramedia.alaan.R;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Page for show Successful message of forgot password operation
 */
public class ForgotSuccessfullyActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_success);
        initView();
    }

    private void initView() {
        hideActionBar();
        setStatusBarColor();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void hideActionBar() {
        getSupportActionBar().hide();
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.alan_tv_primary));
    }
}
