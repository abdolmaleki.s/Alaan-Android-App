package com.edoramedia.alaan.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.edoramedia.alaan.R;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


/**
 * The page for login.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTV_forgotPassword;
    private TextView mTV_register;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        hideActionBar();
        setStatusBarColor();
        mTV_forgotPassword = findViewById(R.id.activity_login_tv_forgot_password);
        mTV_register = findViewById(R.id.activity_login_tv_register);

        mTV_forgotPassword.setOnClickListener(this);
        mTV_register.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void hideActionBar() {
        getSupportActionBar().hide();
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.alan_tv_primary));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == mTV_forgotPassword.getId()) {
            gotoForgotPasswordPage();
        } else if (id == mTV_register.getId()) {
            goToRegisterPage();
        }
    }

    private void goToRegisterPage() {
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
    }

    private void gotoForgotPasswordPage() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
        finish();
    }
}
