package com.edoramedia.alaan.activity;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.edoramedia.alaan.Interface.IPageController;
import com.edoramedia.alaan.adapter.SlideMenuAdapter;
import com.edoramedia.alaan.fragment.AccountDashboardFragment;
import com.edoramedia.alaan.fragment.AccountFragment;
import com.edoramedia.alaan.fragment.ArticleFragment;
import com.edoramedia.alaan.fragment.DashboardFragment;
import com.edoramedia.alaan.fragment.CommingSoonFragment;
import com.edoramedia.alaan.fragment.LivePublishDetailsFragment;
import com.edoramedia.alaan.fragment.LiveRadioFragment;
import com.edoramedia.alaan.fragment.LiveTVFragment;
import com.edoramedia.alaan.fragment.LiveVideoListingFragment;
import com.edoramedia.alaan.fragment.NavigateFragment;
import com.edoramedia.alaan.fragment.ScheduleFragment;
import com.edoramedia.alaan.fragment.SearchFragment;
import com.edoramedia.alaan.fragment.ShowFragment;
import com.edoramedia.alaan.fragment.StaticPageFragment;
import com.edoramedia.alaan.infrastructure.DataProvider;
import com.edoramedia.alaan.infrastructure.ThemeHelper;
import com.edoramedia.alaan.R;
import com.edoramedia.alaan.application.Constant;
import com.edoramedia.alaan.setting.AppSetting;
import com.edoramedia.alaan.viewmodel.ArticleCategoryModel;
import com.edoramedia.alaan.viewmodel.SlideMenuModel;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * the page that is container for all pages (Except register and login pages) & sliding menu & bottom navigation.
 * this activity is responsible for adjust them and both menus (slide and bottom) base on section(site)
 */
public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener, IPageController, View.OnClickListener {

    AppSetting mAppSetting;
    private DrawerLayout mDrawer;
    private BottomNavigationViewEx mBottomNavigation;
    private int mSection;
    private Fragment mAttachedFragment;
    private ExpandableListView mSlidinMenuList;
    private ImageView mIV_avatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        configFirstLunch(savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.activity_main_tv_setting_label || id == R.id.activity_main_tv_setting_icon) {
            loadFragment(AccountFragment.newInstance());
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int selectedPosition = mBottomNavigation.getMenuItemPosition(menuItem);
        setBottomMenuBaseOnSection(selectedPosition);
        setBottomMenuActionBaseOnSection(selectedPosition);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void loadFragment(final Fragment fragment) {
        try {
            String backStateName = null;
            backStateName = fragment.getClass().getName();
            String fragmentTag = backStateName;
            FragmentManager manager = getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

            if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment, fragmentTag);
                fragmentTransaction.addToBackStack(backStateName);
                fragmentTransaction.commit();
            }
        } catch (Exception e) {

        }

        mDrawer.closeDrawers();

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                
            }
        });
    }

    @Override
    public void setSection(int section) {
        mSection = section;
        AppSetting.getInstance().setAppSection(section);
        ThemeHelper.setStatusBarColorBaseOnSection(this, section);
        setBottomMenuBaseOnSection(2);
        setSlidingMenuBaseOnSection();
    }

    @Override
    public void goBack() {
        onBackPressed();
    }

    @Override
    public void needBottomNavigation(boolean needBottomNavigation) {
        if (needBottomNavigation) {
            mBottomNavigation.setVisibility(View.VISIBLE);
        } else {
            mBottomNavigation.setVisibility(View.GONE);
        }
    }

    @Override
    public void moveSlidingMenu() {
        if (mDrawer.isDrawerOpen(Gravity.END)) {
            mDrawer.closeDrawer(Gravity.END);
        } else {
            mDrawer.openDrawer(Gravity.END);
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        mAttachedFragment = fragment;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAttachedFragment = null;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    private void configFirstLunch(Bundle savedInstanceState) {
        if (savedInstanceState == null) {

        }
        setSection(Constant.Section.TV);
        onNavigationItemSelected(mBottomNavigation.getMenu().getItem(0));
    }

    private void initView() {
        mAppSetting = AppSetting.getInstance();
        hideActionBar();
        bindViews();
        configSlidingMenu();
        initBottomNavigationView();

    }

    private void bindViews() {
        mDrawer = findViewById(R.id.activity_main_drawer);
        mBottomNavigation = findViewById(R.id.activity_main_bottom_menu);
        mSlidinMenuList = findViewById(R.id.activity_main_list_slide_menu);
        mIV_avatar = findViewById(R.id.activity_main_iv_avatar);
        findViewById(R.id.activity_main_tv_setting_icon).setOnClickListener(this);
        findViewById(R.id.activity_main_tv_setting_label).setOnClickListener(this);

    }

    private void hideActionBar() {
        getSupportActionBar().hide();
    }

    private void configSlidingMenu() {
        configDrawerTransition();
        setSlidingMenuActionBaseOnSection();
    }

    private void configDrawerTransition() {
        final ConstraintLayout contentPanel = findViewById(R.id.activity_main_panel_content);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float slideX = drawerView.getWidth() * slideOffset;
                contentPanel.setTranslationX(-slideX);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawer.addDrawerListener(actionBarDrawerToggle);
    }

    private void initBottomNavigationView() {

        mBottomNavigation.enableAnimation(false);
        mBottomNavigation.setTextVisibility(false);
        mBottomNavigation.enableShiftingMode(false);
        mBottomNavigation.setOnNavigationItemSelectedListener(this);
    }

    private void setBottomMenuBaseOnSection(int selectedPosition) {
        int color = 0;
        int selectedColor = 0;

        Drawable drawable1 = null;
        Drawable drawable2 = null;
        Drawable drawable3 = null;
        Drawable drawable4 = null;
        Drawable drawable5 = null;

        MenuItem item1 = mBottomNavigation.getMenu().getItem(0);
        MenuItem item2 = mBottomNavigation.getMenu().getItem(1);
        MenuItem item3 = mBottomNavigation.getMenu().getItem(2);
        MenuItem item4 = mBottomNavigation.getMenu().getItem(3);
        MenuItem item5 = mBottomNavigation.getMenu().getItem(4);

        if (mSection == Constant.Section.TV) {

            color = ContextCompat.getColor(this, R.color.gray_icon_drawer);
            selectedColor = ContextCompat.getColor(this, R.color.alan_tv_primary);
            drawable1 = (selectedPosition == 0) ? getDrawable(R.drawable.ic_dashboard_solid) : getDrawable(R.drawable.ic_dashboard);
            drawable2 = (selectedPosition == 1) ? getDrawable(R.drawable.ic_cup_solid) : getDrawable(R.drawable.ic_cup);
            drawable3 = (selectedPosition == 2) ? getDrawable(R.drawable.ic_home_solid) : getDrawable(R.drawable.ic_home);
            drawable4 = (selectedPosition == 3) ? getDrawable(R.drawable.ic_calendar_solid) : getDrawable(R.drawable.ic_calendar);
            drawable5 = (selectedPosition == 4) ? getDrawable(R.drawable.ic_user_solid) : getDrawable(R.drawable.ic_user);

        } else if (mSection == Constant.Section.NEWS) {

            color = ContextCompat.getColor(this, R.color.gray_icon_drawer);
            selectedColor = ContextCompat.getColor(this, R.color.alaan_akhbar_primary);
            drawable1 = (selectedPosition == 0) ? getDrawable(R.drawable.ic_dashboard_solid) : getDrawable(R.drawable.ic_dashboard);
            drawable2 = (selectedPosition == 1) ? getDrawable(R.drawable.ic_video_solid) : getDrawable(R.drawable.ic_video);
            drawable3 = (selectedPosition == 2) ? getDrawable(R.drawable.ic_home_solid) : getDrawable(R.drawable.ic_home);
            drawable4 = (selectedPosition == 3) ? getDrawable(R.drawable.ic_stars_solid) : getDrawable(R.drawable.ic_stars);
            drawable5 = (selectedPosition == 4) ? getDrawable(R.drawable.ic_user_solid) : getDrawable(R.drawable.ic_user);

        } else if (mSection == Constant.Section.FM) {

            color = ContextCompat.getColor(this, R.color.gray_icon_drawer);
            selectedColor = ContextCompat.getColor(this, R.color.alan_tv_primary);
            drawable1 = (selectedPosition == 0) ? getDrawable(R.drawable.ic_dashboard_solid) : getDrawable(R.drawable.ic_dashboard);
            drawable2 = (selectedPosition == 1) ? getDrawable(R.drawable.ic_play_solid) : getDrawable(R.drawable.ic_play);
            drawable3 = (selectedPosition == 2) ? getDrawable(R.drawable.ic_home_solid) : getDrawable(R.drawable.ic_home);
            drawable4 = (selectedPosition == 3) ? getDrawable(R.drawable.ic_calendar_solid) : getDrawable(R.drawable.ic_calendar);
            drawable5 = (selectedPosition == 4) ? getDrawable(R.drawable.ic_user_solid) : getDrawable(R.drawable.ic_user);

        } else if (mSection == Constant.Section.AISH) {

            drawable1 = (selectedPosition == 0) ? getDrawable(R.drawable.ic_dashboard_solid) : getDrawable(R.drawable.ic_dashboard);
            drawable2 = (selectedPosition == 1) ? getDrawable(R.drawable.ic_medal_solid) : getDrawable(R.drawable.ic_medal);
            drawable3 = (selectedPosition == 2) ? getDrawable(R.drawable.ic_home_solid) : getDrawable(R.drawable.ic_home);
            drawable4 = (selectedPosition == 3) ? getDrawable(R.drawable.ic_dot_circle_solid) : getDrawable(R.drawable.ic_dot_circle);
            drawable5 = (selectedPosition == 4) ? getDrawable(R.drawable.ic_user_solid) : getDrawable(R.drawable.ic_user);

        }

        item1.setIcon(drawable1);
        item2.setIcon(drawable2);
        item3.setIcon(drawable3);
        item4.setIcon(drawable4);
        item5.setIcon(drawable5);

        if (mSection == Constant.Section.AISH) {
            mBottomNavigation.setIconTintList(0, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.aish_bottom_menu_1)));
            mBottomNavigation.setIconTintList(1, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.aish_bottom_menu_2)));
            mBottomNavigation.setIconTintList(2, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.aish_bottom_menu_3)));
            mBottomNavigation.setIconTintList(3, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.aish_bottom_menu_4)));
            mBottomNavigation.setIconTintList(4, ColorStateList.valueOf(ContextCompat.getColor(this, R.color.aish_bottom_menu_5)));
        } else {
            mBottomNavigation.setIconTintList(0, ColorStateList.valueOf((selectedPosition == 0) ? selectedColor : color));
            mBottomNavigation.setIconTintList(1, ColorStateList.valueOf((selectedPosition == 1) ? selectedColor : color));
            mBottomNavigation.setIconTintList(2, ColorStateList.valueOf((selectedPosition == 2) ? selectedColor : color));
            mBottomNavigation.setIconTintList(3, ColorStateList.valueOf((selectedPosition == 3) ? selectedColor : color));
            mBottomNavigation.setIconTintList(4, ColorStateList.valueOf((selectedPosition == 4) ? selectedColor : color));
        }

    }

    private void setBottomMenuActionBaseOnSection(int selectedPosition) {
        if (mSection == Constant.Section.TV) {
            switch (selectedPosition) {
                case 0:
                    loadFragment(NavigateFragment.newInstance());
                    break;
                case 1:
                    loadFragment(CommingSoonFragment.newInstance());
                    break;
                case 2:
                    loadFragment(DashboardFragment.newInstance());
                    break;
                case 3:
                    loadFragment(ScheduleFragment.newInstance());
                    break;
                case 4:
                    loadFragment(AccountDashboardFragment.newInstance());
                    break;
            }
        } else if (mSection == Constant.Section.NEWS) {
            switch (selectedPosition) {
                case 0:
                    loadFragment(NavigateFragment.newInstance());
                    break;
                case 1:
                    loadFragment(CommingSoonFragment.newInstance());
                    break;
                case 2:
                    loadFragment(DashboardFragment.newInstance());
                    break;
                case 3:
                    loadFragment(CommingSoonFragment.newInstance());
                    break;
                case 4:
                    loadFragment(AccountDashboardFragment.newInstance());
                    break;
            }

        } else if (mSection == Constant.Section.FM) {
            switch (selectedPosition) {
                case 0:
                    loadFragment(NavigateFragment.newInstance());
                    break;
                case 1:
                    loadFragment(LiveRadioFragment.newInstance());
                    break;
                case 2:
                    loadFragment(DashboardFragment.newInstance());
                    break;
                case 3:
                    loadFragment(ScheduleFragment.newInstance());
                    break;
                case 4:
                    loadFragment(AccountDashboardFragment.newInstance());
                    break;
            }

        } else if (mSection == Constant.Section.AISH) {
            switch (selectedPosition) {
                case 0:
                    loadFragment(NavigateFragment.newInstance());
                    break;
                case 1:
                    loadFragment(CommingSoonFragment.newInstance());
                    break;
                case 2:
                    loadFragment(DashboardFragment.newInstance());
                    break;
                case 3:
                    loadFragment(LivePublishDetailsFragment.newInstance());
                    break;
                case 4:
                    loadFragment(AccountDashboardFragment.newInstance());
                    break;
            }
        }
    }

    private void setSlidingMenuBaseOnSection() {

        List<ArticleCategoryModel> categoryModels = generateFakeArticleCategories();
        AppSetting.getInstance().setArticleCategoryList(categoryModels);
        SlideMenuAdapter menuAdapter = null;
        List<SlideMenuModel> menuModels;
        menuModels = DataProvider.generateMenuList(this, mSection);
        menuAdapter = new SlideMenuAdapter(this, menuModels);
        mSlidinMenuList.setAdapter(menuAdapter);
        Glide.with(this).load(Constant.FAKE_AUTHOR_IMAGE_URL).apply(new RequestOptions().circleCrop()).into(mIV_avatar);

    }

    private void setSlidingMenuActionBaseOnSection() {

        mSlidinMenuList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {
                if (mSection == Constant.Section.TV) {
                    switch (groupPosition) {
                        case 0:
                            loadFragment(SearchFragment.newInstance());
                            break;
                        case 1:
                            loadFragment(LiveTVFragment.newInstance());
                            break;
                        case 2:
                            loadFragment(ScheduleFragment.newInstance());
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        case 5:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_ABOUT));
                            break;
                        case 6:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_CONDITIONS));
                            break;
                        case 7:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_POLICY));
                            break;

                    }
                } else if (mSection == Constant.Section.NEWS) {
                    switch (groupPosition) {
                        case 0:
                            loadFragment(SearchFragment.newInstance());
                            break;
                        case 1:
                            loadFragment(LiveTVFragment.newInstance());
                            break;
                        case 2:
                            break;
                        case 3:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_ABOUT));
                            break;
                        case 4:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_CONDITIONS));
                            break;
                        case 5:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_POLICY));
                            break;

                    }

                } else if (mSection == Constant.Section.FM) {

                    switch (groupPosition) {
                        case 0:
                            loadFragment(SearchFragment.newInstance());
                            break;
                        case 1:
                            loadFragment(LiveRadioFragment.newInstance());
                            break;
                        case 2:
                            loadFragment(ScheduleFragment.newInstance());
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        case 5:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_ABOUT));
                            break;
                        case 6:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_CONDITIONS));
                            break;
                        case 7:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_POLICY));
                            break;

                    }

                } else if (mSection == Constant.Section.AISH) {
                    switch (groupPosition) {
                        case 0:
                            loadFragment(SearchFragment.newInstance());
                            break;
                        case 1:
                            loadFragment(LiveVideoListingFragment.newInstance());
                            break;
                        case 2:
                            break;
                        case 3:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_ABOUT));
                            break;
                        case 4:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_CONDITIONS));
                            break;
                        case 5:
                            loadFragment(StaticPageFragment.newInstance(StaticPageFragment.TYPE_POLICY));
                            break;

                    }
                }
                return false;
            }
        });

        mSlidinMenuList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {
                if (mSection == Constant.Section.TV) {
                    switch (groupPosition) {

                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            loadFragment(ShowFragment.newInstance());
                            break;
                        case 4:
                            loadFragment(ArticleFragment.newInstance(childPosition));
                            break;
                    }
                } else if (mSection == Constant.Section.NEWS) {

                    switch (groupPosition) {

                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            loadFragment(ArticleFragment.newInstance(childPosition));
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                    }

                } else if (mSection == Constant.Section.FM) {
                    switch (groupPosition) {

                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            loadFragment(ShowFragment.newInstance());
                            break;
                        case 4:
                            loadFragment(ArticleFragment.newInstance(childPosition));
                            break;
                    }

                } else if (mSection == Constant.Section.AISH) {
                    switch (groupPosition) {

                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            loadFragment(ArticleFragment.newInstance(childPosition));
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                    }
                }
                return false;
            }
        });
    }

    private List<ArticleCategoryModel> generateFakeArticleCategories() {
        List<ArticleCategoryModel> categoryModels = new ArrayList<>();
        ArticleCategoryModel model1 = new ArticleCategoryModel();
        ArticleCategoryModel model2 = new ArticleCategoryModel();

        model1.title = "ریاضیات";
        model2.title = "مشاهیر";

        categoryModels.add(model1);
        categoryModels.add(model2);

        return categoryModels;
    }


}
