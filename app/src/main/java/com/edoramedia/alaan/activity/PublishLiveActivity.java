package com.edoramedia.alaan.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.edoramedia.alaan.R;
import com.takusemba.rtmppublisher.Publisher;
import com.takusemba.rtmppublisher.PublisherListener;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * page for stream live in aish section (camera view).
 */
public class PublishLiveActivity extends BaseActivity implements PublisherListener, View.OnClickListener {

    private GLSurfaceView mSurfaceView;
    private Publisher mPublisher;
    private ImageView mIV_switchCamera;
    private TextView mTV_exit;
    private TextView mTV_cancel;
    private TextView mTV_stop;
    private int mAudioRate;
    private Button mBTN_startPublish;
    private ConstraintLayout mPanelStart;
    private ConstraintLayout mPanelWaiting;
    private ConstraintLayout mPanelPublishing;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreen();
        setContentView(R.layout.activity_publish_live);
        initView();
        initPublisher();
    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void initView() {
        mSurfaceView = findViewById(R.id.activity_publish_live_surface);
        mIV_switchCamera = findViewById(R.id.activity_publish_live_iv_change_camera);
        mBTN_startPublish = findViewById(R.id.activity_publish_live_btn_start_publish);
        mPanelStart = findViewById(R.id.activity_publish_panel_start);
        mPanelWaiting = findViewById(R.id.activity_publish_panel_waiting);
        mPanelPublishing = findViewById(R.id.activity_publish_live_panel_publishing);
        mTV_exit = findViewById(R.id.activity_publish_live_tv_exit);
        mTV_cancel = findViewById(R.id.activity_publish_live_tv_cancel);
        mTV_stop = findViewById(R.id.activity_publish_live_tv_stop);
        mIV_switchCamera.setOnClickListener(this);
        mBTN_startPublish.setOnClickListener(this);
        mTV_exit.setOnClickListener(this);
        mTV_cancel.setOnClickListener(this);
        mTV_stop.setOnClickListener(this);
    }

    private void initPublisher() {

        mPublisher = new Publisher.Builder(this)
                .setGlView(mSurfaceView)
                .setUrl("rtmp://alishafiee.ir:1935/live/alaan2")
                .setSize(Publisher.Builder.DEFAULT_WIDTH, Publisher.Builder.DEFAULT_HEIGHT)
                .setAudioBitrate(Publisher.Builder.DEFAULT_AUDIO_BITRATE)
                .setVideoBitrate(Publisher.Builder.DEFAULT_VIDEO_BITRATE)
                .setCameraMode(Publisher.Builder.DEFAULT_MODE)
                .setListener(this)
                .build();
    }

    private void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

    }

    @Override
    public void onStarted() {

    }

    @Override
    public void onStopped() {

    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onFailedToConnect() {

    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        if (id == mIV_switchCamera.getId()) {
            mPublisher.switchCamera();
        } else if (id == mBTN_startPublish.getId()) {
            goWaitingForAccept();
        } else if (id == mTV_exit.getId()) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        } else if (id == mTV_stop.getId()) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else if (id == mTV_cancel.getId()) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
    }

    private void goWaitingForAccept() {
        mPanelStart.setVisibility(View.GONE);
        mPanelWaiting.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mPanelStart.setVisibility(View.GONE);
                mPanelWaiting.setVisibility(View.GONE);
                mPanelPublishing.setVisibility(View.VISIBLE);
            }
        }, 5000);
    }
}
