package com.edoramedia.alaan.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edoramedia.alaan.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import retrofit2.Call;

/**
 * Page for register user by info entry or social media
 */
public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private EditText mET_firstName;
    private EditText mET_lastName;
    private EditText mET_email;
    private EditText mET_pass;
    private EditText mET_pass_confirm;
    private TextView mTV_skip;
    private TextView mTV_login;
    private ImageView mIV_facebook;
    private ImageView mIV_google;
    private ImageView mIV_twitter;
    private TextView mTV_passInvisible;
    private TextView mTV_passConfirmInvisible;

    //////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////// Facebook Register ////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    private CallbackManager mCallbackManager;
    private LoginButton mBTN_FacebookLogin;

    //////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////// Google Register ////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    private GoogleSignInClient mGoogleSignInClient;
    private SignInButton mBTN_googlLogin;
    private final static int GOOGLE_LOGIN_REQUEST_CODE = 2560;

    //////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////// Twitter Register ////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    private TwitterLoginButton mBTN_twitterLoginButton;
    private final static int TWITTER_LOGIN_REQUEST_CODE = 140;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == mTV_login.getId()) {
            gotoLoginPage();
        } else if (id == mTV_skip.getId()) {
            goToMainPage();
        } else if (id == mIV_facebook.getId()) {
            configFacebookRegister();
            mBTN_FacebookLogin.performClick();
        } else if (id == mIV_google.getId()) {
            configGoogleRegister();
            mBTN_googlLogin.performClick();
        } else if (id == mIV_twitter.getId()) {
            configTwitterRegister();
            mBTN_twitterLoginButton.performClick();
        } else if (id == mTV_passInvisible.getId()) {
            ShowHideText(mTV_passInvisible, mET_pass);
        } else if (id == mTV_passConfirmInvisible.getId()) {
            ShowHideText(mTV_passConfirmInvisible, mET_pass_confirm);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void initView() {
        hideActionBar();
        setStatusBarColor();
        bindView();
    }

    private void bindView() {
        mET_firstName = findViewById(R.id.activity_register_et_first_name);
        mET_lastName = findViewById(R.id.activity_register_et_last_name);
        mET_email = findViewById(R.id.activity_register_et_email);
        mET_pass = findViewById(R.id.activity_register_et_password);
        mET_pass_confirm = findViewById(R.id.activity_register_et_password_confirm);
        mTV_skip = findViewById(R.id.activity_register_tv_skip);
        mTV_login = findViewById(R.id.activity_register_tv_login);
        mIV_facebook = findViewById(R.id.activity_register_img_facebook);
        mIV_google = findViewById(R.id.activity_register_img_google);
        mIV_twitter = findViewById(R.id.activity_register_img_twitter);
        mBTN_FacebookLogin = findViewById(R.id.activity_register_btn_facebook_login);
        mBTN_googlLogin = findViewById(R.id.activity_register_btn_google_login);
        mBTN_twitterLoginButton = findViewById(R.id.activity_register_btn_twitter_login);
        mTV_passInvisible = findViewById(R.id.activity_register_tv_pass_invisible);
        mTV_passConfirmInvisible = findViewById(R.id.activity_register_tv_pass_confirm_invisible);

        mTV_login.setOnClickListener(this);
        mTV_skip.setOnClickListener(this);
        mIV_facebook.setOnClickListener(this);
        mIV_google.setOnClickListener(this);
        mIV_twitter.setOnClickListener(this);
        mBTN_googlLogin.setOnClickListener(this);
        mTV_passInvisible.setOnClickListener(this);
        mTV_passConfirmInvisible.setOnClickListener(this);
    }

    private void hideActionBar() {
        getSupportActionBar().hide();
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.alan_tv_primary));
    }

    private void goToMainPage() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void gotoLoginPage() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void ShowHideText(TextView label, EditText editText) {
        if (editText.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
            editText.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            label.setText("تبین");
            editText.setTypeface(Typeface.SERIF);

        } else {
            editText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            label.setText("إخفا");
        }
    }


    //
    //   _____              _                 _      _                _
    //  |  ___|_ _  ___ ___| |__   ___   ___ | | __ | |    ___   __ _(_)_ __
    //  | |_ / _` |/ __/ _ \ '_ \ / _ \ / _ \| |/ / | |   / _ \ / _` | | '_ \
    //  |  _| (_| | (_|  __/ |_) | (_) | (_) |   <  | |__| (_) | (_| | | | | |
    //  |_|  \__,_|\___\___|_.__/ \___/ \___/|_|\_\ |_____\___/ \__, |_|_| |_|
    //                                                          |___/
    private void configFacebookRegister() {

        mCallbackManager = CallbackManager.Factory.create();

        List<String> permissionNeeds = Arrays.asList("user_photos", "email",
                "user_birthday", "public_profile");
        mBTN_FacebookLogin.setReadPermissions(permissionNeeds);
        mBTN_FacebookLogin.registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        System.out.println("onSuccess");

                        String accessToken = loginResult.getAccessToken()
                                .getToken();

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object,
                                                            GraphResponse response) {


                                        try {
                                            String id = object.getString("id");
                                            try {
                                                URL profile_pic = new URL(
                                                        "http://graph.facebook.com/" + id + "/picture?type=large");

                                            } catch (MalformedURLException e) {
                                                e.printStackTrace();
                                            }
                                            String name = object.getString("name");
                                            String email = object.getString("email");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields",
                                "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        //Toast.makeText(RegisterActivity.this, exception.getCause().toString(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent data) {
        super.onActivityResult(requestCode, responseCode, data);

        if (requestCode == GOOGLE_LOGIN_REQUEST_CODE) {
            Task<GoogleSignInAccount> completedTask = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = completedTask.getResult(ApiException.class); // SuccessFull Login
                getGoogleAccountInformation(account);
            } catch (ApiException e) {
                e.printStackTrace(); //Failed Login
            }
        } else if (requestCode == TWITTER_LOGIN_REQUEST_CODE) {
            mBTN_twitterLoginButton.onActivityResult(requestCode, responseCode, data);

        } else {
            mCallbackManager.onActivityResult(requestCode, responseCode, data);
        }


    }


    //
    //    ____                   _        _                _
    //   / ___| ___   ___   __ _| | ___  | |    ___   __ _(_)_ __
    //  | |  _ / _ \ / _ \ / _` | |/ _ \ | |   / _ \ / _` | | '_ \
    //  | |_| | (_) | (_) | (_| | |  __/ | |__| (_) | (_| | | | | |
    //   \____|\___/ \___/ \__, |_|\___| |_____\___/ \__, |_|_| |_|
    //                     |___/                     |___/


    private void configGoogleRegister() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, GOOGLE_LOGIN_REQUEST_CODE);

    }

    private void getGoogleAccountInformation(GoogleSignInAccount account) {
        if (account != null) {
            String personName = account.getDisplayName();
            String personGivenName = account.getGivenName();
            String personFamilyName = account.getFamilyName();
            String personEmail = account.getEmail();
            String personId = account.getId();
            Uri personPhoto = account.getPhotoUrl();
        }
    }

    //
    //   _____          _ _   _               _                _
    //  |_   _|_      _(_) |_| |_ ___ _ __   | |    ___   __ _(_)_ __
    //    | | \ \ /\ / / | __| __/ _ \ '__|  | |   / _ \ / _` | | '_ \
    //    | |  \ V  V /| | |_| ||  __/ |     | |__| (_) | (_| | | | | |
    //    |_|   \_/\_/ |_|\__|\__\___|_|     |_____\___/ \__, |_|_| |_|
    //                                                   |___/

    private void configTwitterRegister() {
        mBTN_twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession twitterSession = result.data;
                getTwitterUserInformation(twitterSession);
                // Do something with result, which provides a TwitterSession for making API calls
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });
    }

    private void getTwitterUserInformation(final TwitterSession session) {
        TwitterAuthClient authClient = new TwitterAuthClient();
        authClient.requestEmail(session, new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                //userDetailsLabel.setText("User Id : " + session.getUserId() + "\nScreen Name : " + session.getUserName() + "\nEmail Id : " + result.data);
                fetchTwitterImage();
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });
    }

    /**
     * Fetch twitter user image.
     */
    public void fetchTwitterImage() {
        //check if user is already authenticated or not
        if (getTwitterSession() != null) {

            //initialize twitter api client
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();

            //Link for Help : https://developer.twitter.com/en/docs/accounts-and-users/manage-account-settings/api-reference/get-account-verify_credentials

            //pass includeEmail : true if you want to fetch Email as well
            Call<User> call = twitterApiClient.getAccountService().verifyCredentials(true, false, true);
            call.enqueue(new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    User user = result.data;
                    //userDetailsLabel.setText("User Id : " + user.id + "\nUser Name : " + user.name + "\nEmail Id : " + user.email + "\nScreen Name : " + user.screenName);

                    String imageProfileUrl = user.profileImageUrl;

                    //Link : https://developer.twitter.com/en/docs/accounts-and-users/user-profile-images-and-banners
                    //so if you want to get bigger size image then do the following:
                    imageProfileUrl = imageProfileUrl.replace("_normal", "");

                }

                @Override
                public void failure(TwitterException exception) {
                    Toast.makeText(RegisterActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            //if user is not authenticated first ask user to do authentication
            Toast.makeText(this, "First to Twitter auth to Verify Credentials.", Toast.LENGTH_SHORT).show();
        }

    }

    private TwitterSession getTwitterSession() {
        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();

        //NOTE : if you want to get token and secret too use uncomment the below code
        /*TwitterAuthToken authToken = session.getAuthToken();
        String token = authToken.token;
        String secret = authToken.secret;*/

        return session;
    }

}
