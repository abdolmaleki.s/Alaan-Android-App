package com.edoramedia.alaan.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.edoramedia.alaan.R;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Page for gorgot password
 */
public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTV_back;
    private TextView mTV_backIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        initView();
    }

    private void initView() {
        hideActionBar();
        setStatusBarColor();
        mTV_back = findViewById(R.id.activity_forgot_tv_back);
        mTV_backIcon = findViewById(R.id.activity_forgot_tv_back_icon);

        mTV_back.setOnClickListener(this);
        mTV_backIcon.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void hideActionBar() {
        getSupportActionBar().hide();
    }

    private void setStatusBarColor() {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.alan_tv_primary));
    }

    public void goToSuccess(View view) {
        startActivity(new Intent(this, ForgotSuccessfullyActivity.class));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == mTV_back.getId() || id == mTV_backIcon.getId()) {
            gotoLoginPage();
        }
    }

    private void gotoLoginPage() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

}
