package com.edoramedia.alaan.Interface;

import android.support.v4.app.Fragment;

/**
 * The interface for control and navigate to pages.
 */
public interface IPageController {

    /**
     * Load custom page.
     *
     * @param fragment the fragment
     */
    void loadFragment(Fragment fragment);

    /**
     * Sets site(section).
     *
     * @param section the section
     */
    void setSection(int section);

    /**
     * go to previuos page.
     */
    void goBack();

    /**
     * indicates that bottom menu must show in the page or not.
     *
     * @param needBottomNavigation the need bottom navigation
     */
    void needBottomNavigation(boolean needBottomNavigation);

    /**
     * open/close sliding menu.
     */
    void moveSlidingMenu();


}
