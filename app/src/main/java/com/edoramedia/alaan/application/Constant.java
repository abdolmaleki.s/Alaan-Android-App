package com.edoramedia.alaan.application;

import android.graphics.drawable.ColorDrawable;

/**
 * Constant values that may use in everywhere in app
 */
public class Constant {

    public static final String API_BASE_URL = "http://arshanikan.dev.edoramedia.com/webservice/";
    public static final String FAKE_AUTHOR_IMAGE_URL = "https://tcaabudhabi.ae/DataFolder/Images/board-images/tca/1.jpg";
    public static final String FAKE_Article_IMAGE_URL = "https://cdn-04.independent.ie/incoming/article37731016.ece/e22bc/AUTOCROP/w620/62leinster.jpg";
    public static final String FAKE_Article_CUSTOM_IMAGE_URL = "https://usskiandsnowboard.org/sites/default/files/images/static-pages/StaticPageHeader_1600x1200_Alpine_Mikeala_Shiffrin_GS_0.jpg";
    public static final String DEBUG_LOG_KEY = "Alaan";
    public static final String YOUTUBE_PLAYER_API_KEY = "AIzaSyDN4zfTkWF7z2Zw86oIxjBT78oof0LN9XU";
    public static final String KEY_APP_SECTION = "KEY_APP_SECTION";

    //Keys
    public static final String DATEPICKER = "DatePickerDialog";

    public static class Setting {

        public static final String IS_FIRST_LUNCH = "is.first.lunch";
        public static final String IS_AUTHENTICATED = "is.authenticated";
        public static final String USER_ID = "user.id";
        public static final String APP_LANGUAGE = "app.lang";
        public static final String USER_FULL_NAME = "user.full.name";
        public static final String ACTIVE_PACKAGE_TYPE = "active.package.type";
        public static final String USER_ID_PLAINTEXT = "user.id.plaintext";
        public static final String USER_IMAGE = "user.image";
        public static final String CATEGORY_LIST = "CATEGORY_LIST";

    }

    public static class Keys {
        public static final String ARTICEL_CATEGORY_POSITION = "ARTICEL_CATEGORY_POSITION";
        public static final String ARTICLE_TYPE = "ARTICLE_TYPE";
    }

    public static class ApiTypes {

        public static final int LOGIN = 1;
    }

    public static class Session {
        public static final String FirstName = "FirstName";
    }

    public static class Permisson {
        public static final int REQUEST_READ_PHONE_STATE = 400;
    }

    public static class VideoTypes {

        public static final int AMAZON = 1;
        public static final int YOUTUBE = 2;
        public static final int DAILYMOTION = 3;
    }

    public static class MediaTypes {

        public static final int Image = 1;
        public static final int Gallery = 2;
        public static final int Video = 3;
    }

    public static class AlertType {
        public static int Information = 0;
        public static int Warning = 1;
        public static int Error = 2;
        public static int Success = 3;
    }

    public static ColorDrawable placeHolderColor;


    public static class Section {
        public static final int TV = 1;
        public static final int NEWS = 2;
        public static final int FM = 3;
        public static final int AISH = 4;
    }

    public static class VideoQuality {
        public static final int P720 = 720;
        public static final int P480 = 480;
        public static final int P360 = 360;
    }

}
